/*
 * Author: Andrew Somerville
 * Date:   2013-04-18
 * License: Private
 */

#include "utils.hpp"
#include "eigen_utils.hpp"

#include <re2uta_atlasCommander/WalkPlan.h>
#include <re2uta/AtlasCommander.hpp>
#include <re2uta/AtlasCommander/BdiStepCommander.hpp>
#include <re2uta/AtlasCommander/PreviewWalkCommander.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/interactive_marker_utils.hpp>
#include <re2uta_atlasCommander/FrameCommandStamped.h>
#include <re2/eigen/eigen_util.h>
#include <osrf_msgs/JointCommands.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/Vector3Stamped.h>
//#include <sensor_msgs/JointState.h>
#include <atlas_msgs/AtlasState.h>
#include <std_msgs/String.h>
#include <std_msgs/Int8.h>

#include <eigen_conversions/eigen_msg.h>
#include <ros/ros.h>

#include <urdf_model/model.h>
#include <Eigen/Core>
#include <boost/format.hpp>
#include <boost/thread.hpp>

using namespace re2uta;
using namespace re2uta::atlascommander;

class AtlasCommanderNode
{
    public:
        enum HandId
        {
            LEFT_HAND  = 0,
            RIGHT_HAND = 1
        };

    public:
        AtlasCommanderNode()
            : m_interactiveMarkerServer( "commander_node" )
        {
            m_shutdown = false;
            std::string urdfString;
            m_node.getParam( "/robot_description",              urdfString );
            m_node.getParam( "/robot_description_fantom_hands", urdfString ); //fantom hands allow for simplified model
            m_urdfModel.initString( urdfString );

            m_atlasLookup.reset(    new AtlasLookup(    m_urdfModel ) );
            m_atlasCommander.reset( new AtlasCommander( m_urdfModel, m_atlasLookup ) );

            using boost::bind;

            m_walktoSub              = m_node.subscribe( "/commander/walkto", 1,                   &AtlasCommanderNode::handleWalkto,                 this );
            m_walktoDestSub          = m_node.subscribe( "/commander/walkto_dest", 1,              &AtlasCommanderNode::handleWalktoDest,             this );
            m_commandFramePoseSub    = m_node.subscribe( "/commander/command_frame_pose", 1,       &AtlasCommanderNode::handleCommandFramePose,       this );
            m_walkCommanderSelectSub = m_node.subscribe( "/commander/walk_commander_selection", 1, &AtlasCommanderNode::handleWalkCommanderSelection, this );
            m_stepGeneratorSelectSub = m_node.subscribe( "/commander/step_generator_selection", 1, &AtlasCommanderNode::handleStepGeneratorSelection, this );
//            m_jointControllerSelectSub = m_node.subscribe( "/commander/joint_generator_selection", 1, &AtlasCommanderNode::handleCommandFramePose, this );

            m_lHandJointCmdSub       = m_node.subscribe<osrf_msgs::JointCommands>( "/commander/l_hand/joint_commands", 1, bind( &AtlasCommanderNode::handleHandCommand, this, _1, LEFT_HAND ) );
            m_rHandJointCmdSub       = m_node.subscribe<osrf_msgs::JointCommands>( "/commander/r_hand/joint_commands", 1, bind( &AtlasCommanderNode::handleHandCommand, this, _1, RIGHT_HAND ) );

            m_lHandJointCmdPub       = m_node.advertise<osrf_msgs::JointCommands>( "/sandia_hands/l_hand/joint_commands", 1 );
            m_rHandJointCmdPub       = m_node.advertise<osrf_msgs::JointCommands>( "/sandia_hands/r_hand/joint_commands", 1 );

            visualization_msgs::InteractiveMarker::Ptr   marker;

            //marker = re2uta::create6dBoxControl( "head", "head_control", "head" );
            //m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            marker = re2uta::create6dBoxControl( m_atlasLookup->lookupString( "l_hand" ), "l_hand_control", m_atlasLookup->lookupString( "l_hand" ) );
            m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            marker = re2uta::create6dBoxControl( m_atlasLookup->lookupString( "r_hand" ), "r_hand_control", m_atlasLookup->lookupString( "r_hand" ) );
            m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            marker = re2uta::create6dBoxControl( m_atlasLookup->lookupString( "r_foot" ), "r_foot_control", m_atlasLookup->lookupString( "r_foot" ) );
            m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            marker = re2uta::create6dBoxControl( m_atlasLookup->lookupString( "l_foot" ), "l_foot_control", m_atlasLookup->lookupString( "l_foot" ) );
            m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            marker = re2uta::create6dBoxControl( "com", "COM_hand_control", "COM_control" );
            m_interactiveMarkerServer.insert(*marker, boost::bind( &AtlasCommanderNode::handleMarkerFeedback, this, _1 ) );

            m_interactiveMarkerServer.applyChanges();
        }



        void handleWalkto( const re2uta_atlasCommander::WalkPlan::ConstPtr & walktoMsg )
        {
            if( walktoMsg->goal_poses.size() > 1 )
            {
                ROS_ERROR( "FIXME: Might not be handling multi goals correctly, this walk plan of size: %lu", walktoMsg->goal_poses.size() );
                WalkGoalVectorPtr walkGoalVector( new WalkGoalVector );
                BOOST_FOREACH( const geometry_msgs::Pose & pose, walktoMsg->goal_poses )
                {
                    WalkGoal::Ptr walkGoal( new WalkGoal );
                    walkGoal->goalFrameId = walktoMsg->header.frame_id;
                    walkGoal->swingLegSuggestion = (Foot) walktoMsg->start_swing_foot;
                    tf::poseMsgToEigen( walktoMsg->goal_poses.back(), walkGoal->goalPose );

                    walkGoalVector->push_back( walkGoal );
                    m_atlasCommander->walkTo( walkGoalVector );
                }
            }
            else
            {
                WalkGoal::Ptr walkGoal( new WalkGoal );
                walkGoal->goalFrameId = walktoMsg->header.frame_id;
                walkGoal->swingLegSuggestion = (Foot) walktoMsg->start_swing_foot;

                tf::poseMsgToEigen( walktoMsg->goal_poses.back(), walkGoal->goalPose );
                m_atlasCommander->walkTo( walkGoal );

                ROS_INFO_STREAM( "Frame id: " << walktoMsg->header.frame_id           );
                ROS_INFO_STREAM( "Vector:   " << walkGoal->goalPose.translation().transpose() );
                ROS_INFO_STREAM( "Rot:      " << walkGoal->goalPose.rotation()                );
            }
        }

        void handleWalktoDest( const geometry_msgs::PoseStamped::ConstPtr & walktoMsg )
        {
//            WalkGoal::Ptr walkGoal( new WalkGoal );
//
//            walkGoal->goalFrameId = walktoMsg->header.frame_id;
//            tf::poseMsgToEigen( walktoMsg->pose, walkGoal->goalPose );
//
//            m_atlasCommander->walkTo( walkGoal );

            WalkGoalVectorPtr walkGoalVector( new WalkGoalVector );

            WalkGoal::Ptr walkGoal( new WalkGoal );
            walkGoal->goalFrameId = walktoMsg->header.frame_id;
            tf::poseMsgToEigen( walktoMsg->pose, walkGoal->goalPose );

            walkGoalVector->push_back( walkGoal );
            m_atlasCommander->walkTo( walkGoalVector );


            ROS_INFO_STREAM( "Frame id: " << walktoMsg->header.frame_id           );
            ROS_INFO_STREAM( "Vector:   " << walkGoal->goalPose.translation().transpose() );
            ROS_INFO_STREAM( "Rot:      " << walkGoal->goalPose.rotation()                );
        }


        void handleHandCommand( const osrf_msgs::JointCommands::ConstPtr & msg, HandId hand )
        {
            switch( hand )
            {
                case LEFT_HAND:
                    m_lHandJointCmdPub.publish( msg );
                    break;
                case RIGHT_HAND:
                    m_rHandJointCmdPub.publish( msg );
                    break;
            }
        }

        void handleCommandFramePose( const re2uta_atlasCommander::FrameCommandStamped::ConstPtr & msg )
        {
//            ROS_INFO_STREAM_THROTTLE( 1, "Thread id: %i" << boost::this_thread::get_id() );

            Eigen::Affine3d pose;

            tf::poseMsgToEigen( msg->goalPose.pose, pose );
            Eigen::Vector6d weights;
            weights.head(3) = Eigen::Vector3dConstMap( &msg->weights.linear.x  );
            weights.tail(3) = Eigen::Vector3dConstMap( &msg->weights.angular.x );


            FrameGoal::Ptr frameGoal( new FrameGoal );

            frameGoal->baseFrameId = msg->header.frame_id;
            frameGoal->goalFrameId = msg->goalPose.header.frame_id;
            frameGoal->tipFrameId  = msg->target_frame_id;
            frameGoal->goalPose    = pose;
            frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3dConstMap( &msg->weights.linear.x  );
            frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3dConstMap( &msg->weights.angular.x );

            m_atlasCommander->commandFramePose( frameGoal );
        }

        void handleWalkCommanderSelection( const std_msgs::String::ConstPtr & msg )
        {
            ROS_WARN_STREAM( "Got walk commander switch selection: " << msg->data );
            if( msg->data == "Bdi" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::BDI_WALK_COMMANDER     );
            else
            if( msg->data == "Re2Uta" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::RE2UTA_WALK_COMMMANDER );
            else
            if( msg->data == "Balance" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::BALANCE_WALK_COMMMANDER );
            else
            if( msg->data == "Crawl" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::CRAWL_WALK_COMMMANDER );
            else
            if( msg->data == "CapturePoint" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::CAPTUREPOINT_WALK_COMMMANDER );
            else
            if( msg->data == "off" )
                m_atlasCommander->setWalkCommanderType( AtlasCommander::OFF );
            else
                ROS_ERROR_STREAM( "Unknown walk commander : " << msg->data );
        }

        void handleStepGeneratorSelection( const std_msgs::String::ConstPtr & msg )
        {
            ROS_WARN_STREAM( "Got step generator selection: " << msg->data );
            if( msg->data == "Nao" )
                m_atlasCommander->setStepPlannerType( atlascommander::WalkCommanderInterface::NAO_PLANNER );
            if( msg->data == "Circle" )
                m_atlasCommander->setStepPlannerType( atlascommander::WalkCommanderInterface::CIRCLE_PLANNER );
            if( msg->data == "Manual" )
                m_atlasCommander->setStepPlannerType( atlascommander::WalkCommanderInterface::MANUAL_PLANNER );
        }

        void handleMarkerFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr & msg )
        {

            if( msg->event_type == visualization_msgs::InteractiveMarkerFeedback::MOUSE_UP )
            {
                ROS_INFO_STREAM( "Got mouse up, sending" );
                ROS_INFO_STREAM( "Feedback" );
                ROS_INFO_STREAM( "Msg : " << *msg );
                ROS_INFO_STREAM( "\n" );


                FrameGoal::Ptr frameGoal( new FrameGoal );

                frameGoal->baseFrameId = ""; //msg->header.frame_id;
                frameGoal->goalFrameId = msg->header.frame_id;
                frameGoal->tipFrameId  = msg->header.frame_id;
                frameGoal->goalPose    = toEigen( msg->pose );

                m_interactiveMarkerServer.setPose( msg->marker_name, re2uta::toMsg( Eigen::Affine3d::Identity() ) );
                m_interactiveMarkerServer.applyChanges();


                if( msg->marker_name == "COM_control" )
                {
                    frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3d( 1, 1, 1 );
                }
                else
                if( msg->marker_name == "head_control" )
                {
                    frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3d( 0, 0, 0 );
                    frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3d( 0.5,0.5,0.5 );
                }
                else
                {
//                    frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3d( 1, 1, 0 );
//                    Eigen::Quaterniond quat;
//                    quat.setFromTwoVectors( Eigen::XAxis3d, Eigen::Vector3d(1,0,-1) );


                    frameGoal->weightMatrix.block(0,0,3,3).diagonal() = Eigen::Vector3d(0.2,0.2,0.2);

//                    frameGoal->weightMatrix.block(0,0,3,3) = quat.toRotationMatrix() * frameGoal->weightMatrix.block(0,0,3,3);


                    frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3d( 0.04,0.04,0.04);

                }
    //            frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3dConstMap( &msg->weights.linear.x  );
    //            frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3dConstMap( &msg->weights.angular.x );

                m_atlasCommander->commandFramePose( frameGoal );
            }
        }


        void go()
        {
            ROS_INFO( "GO!" );
            ros::spin();
        }



    private:
        ros::NodeHandle  m_node;

        urdf::Model      m_urdfModel;
        AtlasLookup::Ptr m_atlasLookup;

        ros::Subscriber  m_walktoSub;
        ros::Subscriber  m_walktoDestSub;
        ros::Subscriber  m_commandFramePoseSub;
        ros::Subscriber  m_lHandJointCmdSub;
        ros::Subscriber  m_rHandJointCmdSub;
        ros::Subscriber  m_walkCommanderSelectSub;
        ros::Subscriber  m_stepGeneratorSelectSub;

        ros::Publisher   m_lHandJointCmdPub;
        ros::Publisher   m_rHandJointCmdPub;

        interactive_markers::InteractiveMarkerServer m_interactiveMarkerServer;


        bool             m_shutdown;

        AtlasCommander::Ptr m_atlasCommander;
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "atlas_commander_node" );

    AtlasCommanderNode atlasCommanderNode;

    atlasCommanderNode.go();

    return 0;
}
