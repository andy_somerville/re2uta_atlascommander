/*
 * AtlasCommander.mcpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

//#include <re2uta/AtlasCommander/CompliantModelJointController.hpp> // Make sure this is on top so the Eigen/Core is not included before the RBDL stuff
#include <re2uta/AtlasCommander/NeuralNetworkJointController.hpp>
#include <re2uta/AtlasCommander.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/AtlasCommander/NullWalkCommander.hpp>
#include <re2uta/AtlasCommander/types.hpp>
#include <re2uta/AtlasCommander/BdiStepCommander.hpp>
#include <re2uta/AtlasCommander/DefaultJointController.hpp>
#include <re2uta/AtlasCommander/NullJointController.hpp>
#include <re2uta/AtlasCommander/PreviewWalkCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyController.hpp>
#include <re2uta/AtlasCommander/CapturePointCommander.hpp>
#include <re2uta/AtlasCommander/CrawlCommander.hpp>
#include <re2uta/AtlasLookup.hpp>

#include <re2/eigen/eigen_util.h>
#include <re2/visutils/VisualDebugPublisher.h>
#include <re2/matrix_conversions.h>

#include <std_msgs/Empty.h>
#include <atlas_msgs/AtlasState.h>
#include <ros/ros.h>

#include <boost/functional/hash.hpp>
#include <boost/format.hpp>
#include <boost/interprocess/detail/atomic.hpp>


namespace re2uta
{

using namespace atlascommander;

AtlasCommander::
AtlasCommander( const urdf::Model & model, const AtlasLookup::Ptr & atlasLookup )
    : m_urdfModel( model )
    , m_atlasLookup( atlasLookup )
{
    m_shutdown = false;
    ros::Duration period( 0.01 );

    std::string controllerTypeName( "DefaultJointController" );
    ros::NodeHandle("~").getParam( "joint_controller", controllerTypeName );

    std::string walkCommanderName( "PreviewWalkCommander" );
    ros::NodeHandle("~").getParam( "walk_commander", walkCommanderName );

    std::string stepPlannerTypeName( "NaoPlanner" );
    ros::NodeHandle("~").getParam( "path_planner", stepPlannerTypeName );

    std::string fullBodyControllerName( "FullBodyController" );
    ros::NodeHandle("~").getParam( "fullbody_controller", fullBodyControllerName );


//    m_jointController.reset( new NullJointController( m_stateMsgReadyCondition ) );
    if( controllerTypeName == "NeuralNetworkJointController" )
    {
        ROS_INFO_STREAM( "Using NeuralNetworkJointController" );
        m_jointController.reset( new NeuralNetworkJointController( ros::Duration( 0.002 ),  m_stateMsgReadyCondition, m_atlasLookup ) );
    }
//    else
//    if( controllerTypeName == "CompliantModelJointController" )
//    {
//        ROS_INFO_STREAM( "Using CompliantModelJointController" );
//        m_jointController.reset( new CompliantModelJointController( ros::Duration( 0.001 ),  m_stateMsgReadyCondition ) );
//    }
    else
    if( controllerTypeName == "DefaultJointController" )
    {
        ROS_INFO_STREAM( "Using DefaultJointController" );
        m_jointController.reset( new DefaultJointController(       ros::Duration( 0.001 ),  m_stateMsgReadyCondition, m_atlasLookup ) );
    }
    else
    if( controllerTypeName == "NullJointController" )
    {
        ROS_INFO_STREAM( "Using NullJointController" );
        m_jointController.reset( new NullJointController( m_stateMsgReadyCondition ) );
    }
    else
    {
        ROS_ERROR_STREAM( "Unrecognized joint controller type name [" << controllerTypeName << "] using DefaultJointController instead" );
        m_jointController.reset( new DefaultJointController(       ros::Duration( 0.001 ),  m_stateMsgReadyCondition, m_atlasLookup ) );
    }


    if( fullBodyControllerName == "FullBodyController" )
    {
        ROS_INFO_STREAM( "Using FullBodyController" );
        m_fullBodyController.reset( new FullBodyController( model, m_atlasLookup, m_stateMsgReadyCondition, m_jointController, ros::Duration(0.005) ) );
    }

    m_stepPlannerType = WalkCommanderInterface::NAO_PLANNER;
    if( stepPlannerTypeName == "NaoPlanner" )
        m_stepPlannerType = WalkCommanderInterface::NAO_PLANNER;
    if( stepPlannerTypeName == "CirclePlanner" )
        m_stepPlannerType = WalkCommanderInterface::CIRCLE_PLANNER;
    if( stepPlannerTypeName == "ManualPlanner" )
        m_stepPlannerType = WalkCommanderInterface::MANUAL_PLANNER;


    if( walkCommanderName == "BdiWalkCommander" )
    {
        m_walkCommanderType = BDI_WALK_COMMANDER;
        ROS_INFO_STREAM( "Using BdiWalkCommander" );
        m_walkCommander.reset( new BdiStepCommander( m_fullBodyController, m_urdfModel, m_atlasLookup, m_stepPlannerType ) );// m_fullBodyController, model, ros::Duration( 0.01 ) ) );
    }
    else
    if( walkCommanderName == "BalanceCommander" )
    {
        m_walkCommanderType = BALANCE_WALK_COMMMANDER;
        ROS_INFO_STREAM( "Using BalanceCommander" );
        m_walkCommander.reset( new BalanceCommander( m_fullBodyController, m_urdfModel, m_atlasLookup, ros::Duration( 0.01 ) ) );
    }
    else
    if( walkCommanderName == "CapturePointCommander" )
    {
        m_walkCommanderType = CAPTUREPOINT_WALK_COMMMANDER;
        ROS_INFO_STREAM( "Using CapturePointCommander" );
        m_walkCommander.reset( new CapturePointCommander( m_fullBodyController, m_urdfModel, m_atlasLookup, m_stepPlannerType, ros::Duration( 0.01 ) ) );
    }
    else
    if( walkCommanderName == "CrawlCommander" )
    {
        m_walkCommanderType = CRAWL_WALK_COMMMANDER;
        ROS_INFO_STREAM( "Using CrawlCommander" );
        m_walkCommander.reset( new CrawlCommander( m_fullBodyController, m_urdfModel, m_atlasLookup, ros::Duration( 0.01 ), m_stepPlannerType) );
    }
    else
//    if( walkCommanderName == "PreviewWalkCommander" || !m_walkCommander )
    {
        m_walkCommanderType = RE2UTA_WALK_COMMMANDER;
        ROS_INFO_STREAM( "Using PreviewWalkCommander" );
        m_walkCommander.reset( new PreviewWalkCommander( m_fullBodyController, m_urdfModel, m_atlasLookup, ros::Duration( 0.005 ), m_stepPlannerType ) );
    }



    ROS_INFO_STREAM( "Using : " << stepPlannerTypeName );



    ros::SubscribeOptions atlasStateOptions;
    atlasStateOptions = ros::SubscribeOptions::create<atlas_msgs::AtlasState>( "/atlas/atlas_state", 1,
                                                       boost::bind( &AtlasCommander::handleAtlasStates, this, _1 ),
                                                       ros::VoidPtr(),
                                                       &m_stateUpdateThreadCallbackQueue );

    atlasStateOptions.transport_hints = ros::TransportHints().tcpNoDelay( true );

    m_atlasStateSub     = m_node.subscribe( atlasStateOptions );

    m_vizDebugPublisher.reset( new re2::VisualDebugPublisher( "atlas_commander_viz_debug" ) );

    m_stateUpdateThread = boost::thread( &AtlasCommander::stateUpdateCallbackLoop, this );

    m_stateUpdateLoopTestPub  = m_node.advertise<std_msgs::Empty>( "/stateUpdateLoopTest",              1 );
    m_walkCommanderChangedPub = m_node.advertise<std_msgs::Empty>( "/commander/walk_commander_changed", 1 );
}


AtlasCommander::
~AtlasCommander()
{
    m_shutdown = true;
    m_stateMsgReadyCondition.notify_all();
    m_stateUpdateThread.join();
}

void
AtlasCommander::
stateUpdateCallbackLoop()
{
    while( ros::ok() && !m_shutdown )
    {
        // timeout set to 0.1 so that it will check ros::ok() often enough to exit on ctrl+c
        m_stateUpdateThreadCallbackQueue.callAvailable( ros::WallDuration(0.1) );
    }
}

// this function is called inside of m_stateUpdateThread/stateUpdateCallbackLoop()
void
AtlasCommander::
handleAtlasStates( const atlas_msgs::AtlasState::ConstPtr & msg )
{
    boost::atomic_store( &m_lastStateMsg, msg );
//    ROS_INFO_STREAM( "Commander got: " << *m_lastStateMsg );

    // Silliness
//    if( msg->header.seq%2 == 0 )
//    {
//        m_jointController->setLatestStateMsg(    m_lastStateMsg );
//        std::cout << std::endl << msg->header.seq << " | " << fmod(2, msg->header.seq) << std::endl;
//    }

    m_jointController->setLatestStateMsg(    m_lastStateMsg );
    m_fullBodyController->setLatestStateMsg( m_lastStateMsg );
    m_walkCommander->setLatestStateMsg(      m_lastStateMsg );

    ROS_ASSERT_MSG( !std::isnan( msg->position[0] ) && !std::isinf( msg->position[0] ) && msg->position[0] < 10, "Numberproblem" );

    // this effectively tells the controllers to continue in their work loops
//    ROS_INFO_STREAM_THROTTLE( 1,  "status loop notify delay: " << ros::Time::now() - m_lastNotifyTime );
    m_stateMsgReadyCondition.notify_all();
    m_lastNotifyTime = ros::Time::now();

    // DEBUG
    std_msgs::Empty null;
    m_stateUpdateLoopTestPub.publish( null );

}


atlascommander::Result
AtlasCommander::
walkTo( const atlascommander::WalkGoal::Ptr & walkGoal )
{
//    ROS_ASSERT_MSG( false, "walkTo incomplete" );
    ROS_INFO( "Got single goal walkto" );
    m_walkCommander->walkTo( walkGoal );

    return atlascommander::Success_Result;
}

atlascommander::Result
AtlasCommander::
walkTo( const atlascommander::WalkGoalVectorPtr & walkGoals )
{
    ROS_INFO( "Got multi goals walkto" );
    m_walkCommander->walkTo( walkGoals );

    return atlascommander::Success_Result;
}


//FIXME this is in two places which it shouldn't be;
Eigen::Affine3d
AtlasCommander::
lookupTransform( const std::string & fromFrame, const std::string & toFrame )
{
    tf::StampedTransform tfTransform;
    m_tfListener.waitForTransform( toFrame, fromFrame, ros::Time(0), ros::Duration(2) );
    m_tfListener.lookupTransform( toFrame, fromFrame, ros::Time(0), tfTransform );

    Eigen::Matrix4d homoTransform = re2::convertTfToEigen4x4( tfTransform );

    return Eigen::Affine3d( homoTransform );
}


atlascommander::Result
AtlasCommander::
commandFramePose( const atlascommander::FrameGoal::Ptr frameGoal )
{

    if( frameGoal->goalFrameId == "com" )
    {
        //FIXME base foot can't be hard coded
        std::string     baseFrame =  m_fullBodyController->getBaseFrameId() ;
        Eigen::Affine3d comToBase = lookupTransform( "com", baseFrame );

        m_fullBodyController->setCenterOfMassGoal( comToBase * frameGoal->goalPose.translation(), frameGoal->weightMatrix.diagonal().head(3) );
    }
    else
    {
        m_fullBodyController->setFrameGoal( frameGoal );

        Eigen::Vector4d color( 0,1,0,1 );
        m_vizDebugPublisher->publishPoint3( frameGoal->goalPose.translation(),
                                            color,
                                            boost::hash<std::string>()(frameGoal->goalFrameId) + 1234,
                                            frameGoal->goalFrameId );
    }

    return atlascommander::Success_Result;
}

atlascommander::Result
AtlasCommander::
setWalkCommanderType( WalkCommanderType  commanderType )
{
    if( m_walkCommanderType == commanderType )
    {
        ROS_WARN( "Already using this type, not switching" );
    }
    else
    {
        switch( commanderType )
        {
            case BDI_WALK_COMMANDER:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new BdiStepCommander( m_fullBodyController,
                                                             m_urdfModel,
                                                             m_atlasLookup,
                                                             m_stepPlannerType ) );
                m_walkCommanderType = commanderType;
                break;
            case RE2UTA_WALK_COMMMANDER:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new PreviewWalkCommander( m_fullBodyController,
                                                                 m_urdfModel,
                                                                 m_atlasLookup,
                                                                 ros::Duration( 0.01 ), //FIXME this should be parametrized
                                                                 m_stepPlannerType) );
                m_walkCommanderType = commanderType;
                break;
            case BALANCE_WALK_COMMMANDER:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new BalanceCommander( m_fullBodyController,
                                                             m_urdfModel,
                                                             m_atlasLookup,
                                                             ros::Duration( 0.01 ) ) );
                m_walkCommanderType = commanderType;
                break;
            case CRAWL_WALK_COMMMANDER:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new CrawlCommander( m_fullBodyController,
                                                           m_urdfModel,
                                                           m_atlasLookup,
                                                           ros::Duration( 0.01 ),
                                                           m_stepPlannerType      ) );
                m_walkCommanderType = commanderType;
                break;
            case CAPTUREPOINT_WALK_COMMMANDER:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new CapturePointCommander( m_fullBodyController,
                                                                 m_urdfModel,
                                                                 m_atlasLookup,
                                                                 m_stepPlannerType,
                                                                 ros::Duration( 0.01 ) ) );
                m_walkCommanderType = commanderType;
                break;
            case OFF:
                m_walkCommander->shutdown();
                m_walkCommander.reset( new NullWalkCommander() );
                break;

            default:
                ROS_ERROR_STREAM( "Typeid failed for " << typeid( commanderType ).name() );
                break;
        }
    }

    std_msgs::Empty done;
    m_walkCommanderChangedPub.publish(done);

    if( m_walkCommander )
        return atlascommander::Success_Result;
    else
        return atlascommander::Failure_Result;
}


atlascommander::Result
AtlasCommander::
setStepPlannerType( atlascommander::WalkCommanderInterface::StepPlannerType stepPlannerType )
{
    m_walkCommander->setStepPlannerType( stepPlannerType );
    m_stepPlannerType = stepPlannerType;
    return atlascommander::Success_Result;
}


void
AtlasCommander::
stimulate( const ros::Time & sampleTime )
{
    ROS_ASSERT_MSG( false, "incomplete" );
}




}


































