/*
 * StepGenerator.cpp
 *
 *  Created on: May 31, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/visutils/marker_util.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/math/special_functions/sign.hpp>
#include <math.h>


namespace re2uta
{



std::string footToFrameId( Foot foot, AtlasLookup::Ptr const & atlasLookup )
{
    switch( foot )
    {
        case LEFT_FOOT:
            return atlasLookup->lookupString( "l_foot" );
        case RIGHT_FOOT:
            return atlasLookup->lookupString( "r_foot" );
        default:
            throw std::runtime_error( "Unknown foot to convert" );
    }
}



Foot otherFoot( Foot foot )
{
    switch( foot )
    {
        case LEFT_FOOT:
            return RIGHT_FOOT;
        case RIGHT_FOOT:
            return LEFT_FOOT;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


std::string limbToFrameId( Limb limb, AtlasLookup::Ptr const & atlasLookup )
{
    switch( limb )
    {
        case LEFT_FOOT_LIMB:
            return atlasLookup->lookupString( "l_foot" );
        case LEFT_HAND_LIMB:
            return atlasLookup->lookupString( "l_hand" );
        default:
            throw std::runtime_error( "Unknown foot to convert" );
    }
}

Limb otherLimb( Limb limb )
{
    switch( limb )
    {
        case LEFT_FOOT_LIMB:
            return LEFT_HAND_LIMB;
        case LEFT_HAND_LIMB:
            return LEFT_FOOT_LIMB;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}



Eigen::Affine3d &
StepPose::
swingFootPose()
{
    switch( swingFoot )
    {
        case LEFT_FOOT:
            return lFootPose;
        case RIGHT_FOOT:
            return rFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

Eigen::Affine3d &
CrawlPose::
swingLimbPoseOne()
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

Eigen::Affine3d &
CrawlPose::
swingLimbPoseTwo()
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

Eigen::Affine3d &
CrawlPose::
swingLimbPoseThree()
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


Eigen::Affine3d &
StepPose::
baseFootPose()
{
    switch( swingFoot )
    {
        case LEFT_FOOT: // opposite
            return rFootPose;
        case RIGHT_FOOT:
            return lFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


Eigen::Affine3d &
CrawlPose::
baseLimbPose()
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB: // opposite
            return lHandPose;
        case LEFT_HAND_LIMB:
            return lFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


const Eigen::Affine3d &
StepPose::
swingFootPose() const
{
    switch( swingFoot )
    {
        case LEFT_FOOT:
            return lFootPose;
        case RIGHT_FOOT:
            return rFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


const Eigen::Affine3d &
CrawlPose::
swingLimbPoseOne() const
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

const Eigen::Affine3d &
CrawlPose::
swingLimbPoseTwo() const
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

const Eigen::Affine3d &
CrawlPose::
swingLimbPoseThree() const
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB:
            return lFootPose;
        case LEFT_HAND_LIMB:
            return lHandPose;
        case RIGHT_FOOT_LIMB:
            return rFootPose;
        case RIGHT_HAND_LIMB:
            return rHandPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}


const Eigen::Affine3d &
StepPose::
baseFootPose() const
{
    switch( swingFoot )
    {
        case LEFT_FOOT: // opposite
            return rFootPose;
        case RIGHT_FOOT:
            return lFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}

const Eigen::Affine3d &
CrawlPose::
baseLimbPose() const
{
    switch( swingLimb )
    {
        case LEFT_FOOT_LIMB: // opposite
            return lHandPose;
        case LEFT_HAND_LIMB:
            return lFootPose;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
}



//StepPlan::
//StepPlan( )
//{
//    assert(false); //FIXME what about swing foot!
//}


Foot
StepPlan::
swingFoot()
{
    switch( m_baseFoot )
    {
        case LEFT_FOOT:
            return RIGHT_FOOT;
        case RIGHT_FOOT:
            return LEFT_FOOT;
        default:
            throw std::runtime_error( "Unknown foot" );
    }
//    return m_swingFoot;
}

Foot &
StepPlan::
baseFoot()
{
    return m_baseFoot;
}

double
StepPlan::
completion( const ros::Duration & relativeTime )
{
    return relativeTime.toSec() / (m_stepPoses.size() * m_dt.toSec());
}



Eigen::Vector3d interpolate( const Eigen::Vector3d & vec0, const Eigen::Vector3d & vec1, double factor )
{
    Eigen::Vector3d interpVec;
    Eigen::Vector3d poseDiff = vec1 - vec0;
    interpVec = vec0 + factor*poseDiff;

    return interpVec;
}

Eigen::Affine3d interpolate( const Eigen::Affine3d & pose0, const Eigen::Affine3d & pose1, double factor )
{
    Eigen::Affine3d interpolatedPose = Eigen::Affine3d::Identity();

    interpolatedPose.translation() = interpolate( pose0.translation(), pose1.translation(), factor );

    Eigen::Quaterniond quat0( pose0.rotation() );
    Eigen::Quaterniond quat1( pose1.rotation() );

    Eigen::Quaterniond quatResult = quat0.slerp( factor, quat1 );
    interpolatedPose.linear() = quatResult.toRotationMatrix();

    return interpolatedPose;
}



StepPose::Ptr
StepPlan::
getAtTime( const ros::Duration & relativeTime )
{

    // FIXME maybe we should interpolate;
    double index = (relativeTime.toSec() / m_dt.toSec());

    double stepPoseSize = m_stepPoses.size();

    std::size_t prevIndex = std::min( (double)(m_stepPoses.size()-1), std::floor( index ) );
    std::size_t nextIndex = std::min( (double)(m_stepPoses.size()-1), std::ceil(  index ) );

    double dtPortion = index - prevIndex;///m_dt.toSec();


    StepPose::Ptr interpolatedPose(new StepPose(m_stepPoses[prevIndex]->baseFoot() ) );

    interpolatedPose->comPose         = interpolate( m_stepPoses[prevIndex]->comPose,         m_stepPoses[nextIndex]->comPose,         dtPortion );
    interpolatedPose->baseFootPose()  = interpolate( m_stepPoses[prevIndex]->baseFootPose() , m_stepPoses[nextIndex]->baseFootPose() , dtPortion );
    interpolatedPose->swingFootPose() = interpolate( m_stepPoses[prevIndex]->swingFootPose(), m_stepPoses[nextIndex]->swingFootPose(), dtPortion );

    return interpolatedPose;
}

StepPose::Ptr
StepPlan::
last()
{
    if( !m_stepPoses.empty() )
        return m_stepPoses.back();
    else
        return StepPose::Ptr();
}



StepGenerator::
~StepGenerator() {}


bool
StepGenerator::
hasArrived( const WalkPlan & walkPlan ) const
{
    if(    hasArrived( walkPlan.leftFootPoses->back()  )
        && hasArrived( walkPlan.rightFootPoses->back() )  )
        return true;
    else
        return false;
}


//Eigen::Affine3d
//StepGenerator::
//generateNextStep( const Eigen::Vector3d & currentTranslation, double zAngle, int stepIndex )
//{
//    Eigen::Affine3d current = Eigen::Translation3d(currentTranslation) * Eigen::AngleAxisd( zAngle, Eigen::ZAxis3d );
//    return generateNextStep( current, stepIndex );
//}



StepPlan::Ptr
StepGenerator::
generateAllSteps()
{
    StepPose::Ptr beginStepPose;
    beginStepPose = getBeginStepPose();

    StepPlan::Ptr stepPlan( new StepPlan( beginStepPose->baseFoot(), m_dt ) );
    geometry_msgs::PoseStamped poseStamped;

    StepPose::Ptr prevStepPose( new StepPose( *beginStepPose ) );
    prevStepPose->swingFoot = otherFoot( prevStepPose->swingFoot ); // invert so that our inversion inside the loop sets it back to the correct value


    ROS_INFO_STREAM( "Generating all steps" );

    for( int stepIndex = 0 ;
         (    !hasArrived(prevStepPose->rFootPose)
           || !hasArrived(prevStepPose->lFootPose) ) && ros::ok();  ++stepIndex )
    {
        StepPose::Ptr stepPose( new StepPose( prevStepPose->swingFoot ) );

        stepPose->baseFootPose()  = prevStepPose->swingFootPose();
        stepPose->swingFootPose() = prevStepPose->baseFootPose();
        stepPose->swingFootPose() = generateNextStep( stepPose->baseFootPose(),
                                                      stepPose->swingFootPose(),
                                                      stepIndex );

        stepPlan->stepPoses().push_back( stepPose );

        prevStepPose = stepPose;

        ROS_INFO_STREAM( "Step:           " << stepIndex );
        ROS_INFO_STREAM( "baseFootPose  : " << stepPose->baseFootPose().translation().transpose()  );
        ROS_INFO_STREAM( "swingFootPose : " << stepPose->swingFootPose().translation().transpose() );
//        ROS_INFO_STREAM( "swingFoot     : " << footToFrameId(stepPose->swingFoot, m_atlasLookup) );
        ROS_INFO_STREAM( "" );
    }

    ROS_INFO_STREAM( "Generated : "  << stepPlan->stepPoses().size() << "  steps " );

    return stepPlan;
}








StraightLineStepGenerator::
StraightLineStepGenerator( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    m_source     = source;
    m_dest       = dest;
    m_halfStepLength = stepLength;
}


bool
StraightLineStepGenerator::
hasArrived( const Eigen::Affine3d & current ) const
{
    return withinDistance( current, m_halfStepLength );
}

bool
StraightLineStepGenerator::
withinDistance( const Eigen::Affine3d & current, double dist ) const
{
    Eigen::Affine3d step( Eigen::Translation3d( dist, 0, 0 ) );
    Eigen::Affine3d next( Eigen::Translation3d( current.translation() + step.translation() ) );


    bool result = false;
    if(   std::abs( (next.translation()   - m_source.translation()).x() )
        > std::abs( (m_dest.translation() - m_source.translation()).x() ) )
    {
        result = true;
    }

//    std::cout << "  dest:    " << m_dest.translation().transpose()  << std::endl;
//    std::cout << "  current: " << current.translation().transpose() << std::endl;
//    std::cout << "  next:    " << next.translation().transpose()    << std::endl << std::endl;

    return result;
}


Eigen::Affine3d
StraightLineStepGenerator::
generateNextStep( const Eigen::Affine3d & last, const Eigen::Affine3d & current, int stepIndex )
{
    Eigen::Affine3d next;
    Eigen::Affine3d step( Eigen::Translation3d( m_halfStepLength*2, 0, 0 ) );

    if( withinDistance( current, m_halfStepLength*2 ) )
        step = Eigen::Translation3d( m_halfStepLength, 0, 0 );

    if( stepIndex == 0 )
        step = Eigen::Translation3d( m_halfStepLength, 0, 0 );

//    if( pastDest( current ) )
//        step = Eigen::Translation3d( 0, 0, 0 );


    std::cout << "Translation: " << step.translation().transpose() << "\n";
    next = Eigen::Translation3d( current.translation() + step.translation() );

    return next;
}

StepPose::Ptr
StraightLineStepGenerator::
getBeginStepPose()
{
    StepPose::Ptr beginStepPose( new StepPose( m_swingFoot ) );

    Eigen::Translation3d lFootPose( 0,  0.08, 0    );
    Eigen::Translation3d rFootPose( 0, -0.08, 0    );
    Eigen::Translation3d   comPose( 0,     0, 0.83 );

    beginStepPose->lFootPose = m_source * lFootPose;
    beginStepPose->rFootPose = m_source * rFootPose;
    beginStepPose->comPose   = comPose.translation();

    return beginStepPose;
}




}
