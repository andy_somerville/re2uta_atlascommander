/*
 * RosHalfStepTrajectoryGenerator.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/walking/RosHalfstepTrajectoryGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2uta/walking/NaoPathStepGenerator.hpp>
#include "../eigen_utils.hpp"

namespace re2uta
{



RosHalfstepTrajectoryGenerator::
RosHalfstepTrajectoryGenerator( const StepPose::ConstPtr & beginStepPose,
                                const Eigen::Affine3d & dest,
                                double halfStepSize,
                                double footSpreadOffset,
                                ros::Duration halfstepDt,
                                atlascommander::WalkCommanderInterface::StepPlannerType plannerType,
                                const AtlasLookup::Ptr & atlasLookup
                                )
    : m_atlasLookup( atlasLookup )
{
//    m_dt         = ros::Duration(0.005); // This is per the default of the half_step_generator's documentation. Does not come across in any
//                                         // other way
    m_dt               = halfstepDt;
    m_footSpreadOffset = footSpreadOffset;


    //FIXME wait for service
    m_halfStepClient = m_node.serviceClient<halfsteps_pattern_generator::GetPath>( "getPath" );

    m_pathTransaction.request.initial_left_foot_position      = toMsg( beginStepPose->lFootPose );
    m_pathTransaction.request.initial_right_foot_position     = toMsg( beginStepPose->rFootPose );
    m_pathTransaction.request.initial_center_of_mass_position = toMsg( beginStepPose->comPose   );

    Eigen::Affine3d beginPose = createOdomFrame( beginStepPose );
    Eigen::Affine3d poseDiff  = beginPose.inverse() * dest;

    m_pathTransaction.request.final_left_foot_position        = toMsg( poseDiff * beginStepPose->lFootPose );
    m_pathTransaction.request.final_right_foot_position       = toMsg( poseDiff * beginStepPose->rFootPose );
    m_pathTransaction.request.final_center_of_mass_position   = toMsg( poseDiff * beginStepPose->comPose   );

    if( plannerType == atlascommander::WalkCommanderInterface::CIRCLE_PLANNER )
        m_stepGenerator.reset( new CirclePathStepGenerator( beginPose, dest, halfStepSize, footSpreadOffset, beginStepPose->swingFoot ) );
    else
    if( plannerType == atlascommander::WalkCommanderInterface::NAO_PLANNER )
        m_stepGenerator.reset( new NaoPathStepGenerator( beginPose, dest, footSpreadOffset ) );


    m_pathTransaction.request.start_with_left_foot            = beginStepPose->swingFoot != RIGHT_FOOT; //flipped by Nao experiment

    m_pathTransaction.request.footprints                      = *generateAllStepsFootprints();

    ROS_INFO_STREAM( "Calling half step client..." );
    ROS_INFO_STREAM( "Telling half step that begin step swing foot is: " << footToFrameId( beginStepPose->swingFoot, m_atlasLookup ) );
    if( !m_halfStepClient.call( m_pathTransaction ) )
    {
        ROS_ERROR( "No path returned by RosHalfstepTrajectoryGenerator" );
    }
    ROS_INFO( "...Complete!" );
    ROS_INFO( "m_pathTransaction has %i points", m_pathTransaction.response.path.center_of_mass.points.size() );

    m_swingFoot  = beginStepPose->swingFoot;
    m_stepIndex  = 0;
    m_pointIndex = 0;

}





StepPlan::Ptr
RosHalfstepTrajectoryGenerator::
generateNextStep( const StepPose::ConstPtr & stepPoseIgnored )
{
    StepPlan::Ptr stepPlan( new StepPlan( otherFoot(m_swingFoot), m_dt ) );

/*    if( m_stepIndex >= m_pathTransaction.request.footprints.size() )
    {
        ROS_INFO( "No more steps, stepIndex: %ul greater than number of footprints: %ul", m_stepIndex,
                  m_pathTransaction.request.footprints.size() );
        return StepPlan::Ptr();
    }*/

    if( m_pointIndex >= m_pathTransaction.response.path.center_of_mass.points.size() )
    {
        ROS_INFO( "No more trajectory points, trajectory point index: %ul >= than number of trajectory points: %ul", m_pointIndex,
                   m_pathTransaction.response.path.center_of_mass.points.size() );
        return StepPlan::Ptr(); // return empty when we're done
    }

   /* walk_msgs::Footprint2d footPrint    ;
    Eigen::Affine3d        footPrintPose;
    footPrint                            = m_pathTransaction.request.footprints[m_stepIndex].footprint;
    footPrintPose                        = Eigen::Affine3d::Identity();
    footPrintPose.translation().head(2)  = Eigen::Vector2dMap( &footPrint.x );
    footPrintPose.linear()               = Eigen::AngleAxisd( footPrint.theta, Eigen::ZAxis3d ).toRotationMatrix();
*/
    for( ; m_pointIndex < m_pathTransaction.response.path.center_of_mass.points.size() && ros::ok() ; ++m_pointIndex )
    {
        StepPose::Ptr stepPose( new StepPose( otherFoot(m_swingFoot) ) );
        const geometry_msgs::PointStamped comPoint      = m_pathTransaction.response.path.center_of_mass.points[ m_pointIndex];
        const geometry_msgs::PoseStamped  leftFootPose  = m_pathTransaction.response.path.left_foot     .poses[  m_pointIndex];
        const geometry_msgs::PoseStamped  rightFootPose = m_pathTransaction.response.path.right_foot    .poses[  m_pointIndex];

        stepPose->lFootPose   = toEigen( leftFootPose );
        stepPose->rFootPose   = toEigen( rightFootPose );
        stepPose->comPose     = Eigen::Vector3dConstMap( &comPoint.point.x );

        stepPlan->stepPoses().push_back( stepPose );

        if( m_pointIndex < (m_pathTransaction.response.path.center_of_mass.points.size() - 1) )
        {
            StepPose::Ptr nextStepPose( new StepPose( otherFoot(m_swingFoot) ) );
            const geometry_msgs::PoseStamped  leftFootPose  = m_pathTransaction.response.path.left_foot     .poses[  m_pointIndex + 1];
            const geometry_msgs::PoseStamped  rightFootPose = m_pathTransaction.response.path.right_foot    .poses[  m_pointIndex + 1];
            nextStepPose->lFootPose   = toEigen( leftFootPose );
            nextStepPose->rFootPose   = toEigen( rightFootPose );

            double epsilon = 0.0002; // std::numeric_limits<double>::epsilon()
            if( ! stepPose->baseFootPose().isApprox( nextStepPose->baseFootPose(), epsilon ) )
            {
                ++m_stepIndex;
                m_swingFoot = otherFoot(m_swingFoot);
                break;
            }
        }
    }

//    {//DEBUG
//        ROS_INFO_STREAM( "fp Step:    " << m_stepIndex-1 );
//        ROS_INFO_STREAM( "fp Pose:    " << footPrintPose.translation().transpose() );
//        ROS_INFO_STREAM( "plan swing Foot: " << footToFrameId( stepPlan->swingFoot() ) );
//        ROS_INFO_STREAM( "last point swing Foot: " << footToFrameId( stepPlan->stepPoses().back()->swingFoot, m_atlasLookup ));
//        ROS_INFO_STREAM( "" );
//    }
    ROS_INFO( "Returning step plan with %i poses", stepPlan->stepPoses().size() );

    return stepPlan;
}



RosHalfstepTrajectoryGenerator::FootprintVectorPtr
RosHalfstepTrajectoryGenerator::
generateAllStepsFootprints()
{
    FootprintVectorPtr footprintVector( new FootprintVector );
    StepPlan::Ptr stepPlan;
    stepPlan = m_stepGenerator->generateAllSteps();

    ROS_INFO_STREAM( "Switching steps to footprints" );

    int index = 0;
    BOOST_FOREACH( StepPose::Ptr stepPose, stepPlan->stepPoses() )
    {
        halfsteps_pattern_generator::Footprint footPrint;
        Eigen::Affine3d swingFootPose = stepPose->swingFootPose();

        footPrint.footprint.beginTime.sec  =  0;
        footPrint.footprint.beginTime.nsec =  0;
        footPrint.footprint.duration.sec   =  0 ;
        footPrint.footprint.duration.nsec  =  1e9;
        footPrint.footprint.x              =  swingFootPose.translation().x();
        footPrint.footprint.y              =  swingFootPose.translation().y();
        footPrint.footprint.theta          =  swingFootPose.rotation().eulerAngles(0,1,2).z();
        footPrint.slideUp                  = -0.3;
        footPrint.slideDown                = -0.3;
        footPrint.horizontalDistance       =  m_footSpreadOffset * 2;
        footPrint.stepHeight               =  0.1;

        footprintVector->push_back( footPrint );

        ROS_INFO_STREAM( "Step:           " << index );
        ROS_INFO_STREAM( "swingFoot :     " << footToFrameId( stepPose->swingFoot, m_atlasLookup ) );
        ROS_INFO_STREAM( "swingFootPose : " << swingFootPose.translation().transpose() );
        ROS_INFO_STREAM( "" );
        ++index;
    }

    return footprintVector;
}


}
