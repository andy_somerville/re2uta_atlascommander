/*
 * WalkPatternGenerator.cpp
 *
 *  Created on: May 29, 2013
 *      Author: Ghassan Atmeh, andrew.somerville
 */

#include <halfsteps_pattern_generator/GetPath.h>
#include <halfsteps_pattern_generator/Footprint.h>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/splines/Spline.hpp>
#include <re2/eigen/eigen_util.h>
#include "../eigen_utils.hpp"
#include <Eigen/Core>
#include <boost/foreach.hpp>
#include <iostream>
#include <ros/ros.h>


namespace re2uta
{


Eigen::Affine3d
createOdomFrame( const StepPose::ConstPtr & currentStepPose )
{
    StepPose::Ptr newStepPose( new StepPose( *currentStepPose ) );

    Eigen::Affine3d odomInBaseFoot;
    odomInBaseFoot.translation() = (  currentStepPose->baseFootPose().translation()
                                    + currentStepPose->swingFootPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentStepPose->baseFootPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentStepPose->swingFootPose().rotation() );

    odomInBaseFoot.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    return odomInBaseFoot;
}







StepTrajectoryGenerator::
StepTrajectoryGenerator( const StepPose::ConstPtr & stepPose,
                         const Eigen::Affine3d & dest,
                         double halfStepSize,
                         double footSpreadOffset )
{
    Eigen::Affine3d source;
    source.translation() = (stepPose->lFootPose.translation() + stepPose->rFootPose.translation())/2;

    Eigen::Quaterniond lQuat( stepPose->lFootPose.rotation() );
    Eigen::Quaterniond rQuat( stepPose->rFootPose.rotation() );

    source.linear()      = lQuat.slerp( 0.5, rQuat ).toRotationMatrix();

    //FIXME adjusting dest by source to get into same frame, but unsure if this is the correct way to do it
    m_stepGenerator.reset( new CirclePathStepGenerator( source,
                                                        dest*source,
                                                        halfStepSize,
                                                        footSpreadOffset,
                                                        stepPose->swingFoot ) );

    m_stepIndex = 0;

    m_baseFoot          = otherFoot( stepPose->swingFoot );

    m_lastRightFootPose = stepPose->rFootPose;
    m_lastLeftFootPose  = stepPose->lFootPose;
    m_lastComPose       = stepPose->comPose;
    m_lastZmpPose        = stepPose->comPose;

    m_defaultStepLength    = 0.3; // 2*halfStepSize;       //     m_defaultStepLength    = 0.3;
    m_ankleHeight          = 0.081;                        //     m_ankleHeight          = 0.081;
    m_foreFootLen          = 0.178;                        //     m_foreFootLen          = 0.178;
    m_aftFootLen           = 0.082;                        //     m_aftFootLen           = 0.082;
    m_apexX                = m_defaultStepLength/2;        //     m_apexX                = m_defaultStepLength/2;
    m_liftoffAnklePitch    = 0; //3.1416/30;               //     m_liftoffAnklePitch    = 0; //3.1416/30;
    m_touchdownAnklePitch  = 0; //3.1416/30;               //     m_touchdownAnklePitch  = 0; //3.1416/30;
    m_startAnklePitch      = 0;                            //     m_startAnklePitch      = 0;
    m_endAnklePitch        = 0;                            //     m_endAnklePitch        = 0;
    m_maxZ                 = 0.07;                         //     m_maxZ                 = 0.07;
    m_defaultStepPeriod    = 5;                            //     m_stepPeriod           = 5;
    m_dt                   = 0.005;                        //     m_dt                   = 0.005;
    m_doubleSupportTime    = 1.5;                          //     m_leftFirst            = true;
                                                           //

    m_zmpRefPlan.reset( new Eigen::Vector3dList );
}



Eigen::Affine3d &
StepTrajectoryGenerator::
swingFootPose()
{
    switch( m_baseFoot )
    {
        case LEFT_FOOT:
            return m_lastRightFootPose;
        case RIGHT_FOOT:
            return m_lastLeftFootPose;
        default:
            throw std::runtime_error( "bad foot type" );
    }
}

Eigen::Affine3d &
StepTrajectoryGenerator::
baseFootPose()
{
    switch( m_baseFoot )
    {
        case LEFT_FOOT:
            return m_lastLeftFootPose;
        case RIGHT_FOOT:
            return m_lastRightFootPose;
        default:
            throw std::runtime_error( "bad foot type" );
    }
}


StepPlan::Ptr
StepTrajectoryGenerator::
generateNextStep( const StepPose::ConstPtr & initalPose )
{
    StepPlan::Ptr stepPlan( new StepPlan( m_baseFoot, ros::Duration( m_dt ) ) );

    Eigen::Affine3d newSwingFootPose;
    newSwingFootPose = m_stepGenerator->generateNextStep( baseFootPose(), swingFootPose(), m_stepIndex );
//    newSwingFootPose = m_stepGenerator->generateNextStep( initalPose->baseFootPose(), initalPose->swingFootPose(), m_stepIndex );

    if( (newSwingFootPose.translation() - swingFootPose().translation()).norm() < 0.001 )
        return StepPlan::Ptr();

    Eigen::Vector3dVector zmpRef;

    Eigen::Vector3d newItem;
    double zmpCorrector = 0.0;  // set to 0.1 to have a more conservative stability envelope


    if (m_stepIndex < 1)
    {
        newItem.head(2) = m_lastComPose.head(2); //m_lastZmpPose.head(2); //m_lastComPose.head(2); // Com should be at support foot at the beginning
        newItem(2)      = 0;
        zmpRef.push_back( newItem );

    }
    else
    {
        newItem.head(2) = baseFootPose().translation().head(2); //m_lastZmpPose.head(2); //m_lastComPose.head(2); // Com should be at support foot at the beginning
        newItem(2)      = 0;
        zmpRef.push_back( newItem );
    }


    Eigen::Affine3dListPtr swingTrajectory( new Eigen::Affine3dList );
    for( int timeStep = 0; timeStep < m_doubleSupportTime/m_dt; ++timeStep )
    {
        swingTrajectory->push_back( swingFootPose() );
    }

    newItem.head(2) = baseFootPose().translation().head(2) - zmpCorrector*baseFootPose().translation().head(2); // Com should be at support foot at the beginning
    newItem(2)      = (m_dt + 0.2*m_dt)  * (swingTrajectory->size()); // don't count the one at 0
    zmpRef.push_back( newItem );

    swingTrajectory = generateTrajectory( swingFootPose(), newSwingFootPose, swingTrajectory );

    if(    !swingTrajectory
        || swingTrajectory->size() == 0 )
    {
        ROS_ERROR( "Step Trajectory problem" );
        ROS_ERROR_STREAM( "old swing foot Pose " << swingFootPose().translation().transpose() );
        ROS_ERROR_STREAM( "new swing foot Pose " << newSwingFootPose.translation().transpose() );
//        StepPose::Ptr stepPose( new StepPose );
//        stepPose->swingFoot       = otherFoot( m_baseFoot );
//        stepPose->swingFootPose() = swingFootPose();
//        stepPose->baseFootPose()  = baseFootPose();
//        stepPose->comPose         = m_comPose;
//        stepPlan->stepPoses().push_back( stepPose );
//        return stepPlan;
        return re2uta::StepPlan::Ptr();
    }

    //After the motion, the swing foot is the new base foot: tell the COM to go there.
    newItem.head(2) = newSwingFootPose.translation().head(2) - zmpCorrector*newSwingFootPose.translation().head(2); // Com should be at support foot at the beginning
    newItem(2)      = (m_dt + 0.2*m_dt) * (swingTrajectory->size()); // don't count the one at 0
    zmpRef.push_back( newItem );

    for( int timeStep = 0; timeStep < m_doubleSupportTime/m_dt; ++timeStep )
    {
        swingTrajectory->push_back( swingTrajectory->back() );
    }

    newItem.head(2) = newSwingFootPose.translation().head(2) - zmpCorrector*newSwingFootPose.translation().head(2); // Com should be at support foot at the beginning
    newItem(2)      = (m_dt + 0.2*m_dt) * (swingTrajectory->size()); // don't count the one at 0
    zmpRef.push_back( newItem );

    int zmpIndex = 0;
    BOOST_FOREACH( const Eigen::Vector3d & zmpRefItem, zmpRef )
    {
        std::cout << "zmp ref: " << zmpIndex << " : " << zmpRefItem.transpose() << "\n";
        ++zmpIndex;
    }

    Eigen::Vector3dListPtr comPoseList;
    comPoseList = comTraj( m_dt, zmpRef, m_zmpRefPlan,  &m_lastComPose, &m_lastComVelocity, &m_lastComAccel , &m_lastZmpPose, m_doubleSupportTime);

    Eigen::Vector3dList::iterator comPoseItor = comPoseList->begin();
    StepPose::Ptr stepPose( new StepPose(m_baseFoot) );
    BOOST_FOREACH( const Eigen::Affine3d & swingFootPoseElement, *swingTrajectory )
    {
        if( comPoseItor == comPoseList->end() )
        {
            ROS_ERROR( "Not enough com poses vs swing foot poses: %lu vs %lu", comPoseList->size(), swingTrajectory->size()  );
            break;
        }

        stepPose.reset( new StepPose(m_baseFoot) );
        stepPose->swingFootPose() = swingFootPoseElement;
        stepPose->baseFootPose()  = baseFootPose();
        stepPose->comPose         = *comPoseItor;
        stepPlan->stepPoses().push_back( stepPose );

        ++comPoseItor;
    }


    while( comPoseItor != comPoseList->end() )
    {
        stepPose.reset( new StepPose(*stepPose) );
        stepPose->comPose         = *comPoseItor;
        stepPlan->stepPoses().push_back( stepPose );
        ++comPoseItor;
    }


    m_lastComPose = stepPlan->stepPoses().back()->comPose;

    swingFootPose() = newSwingFootPose;
    m_baseFoot = stepPlan->swingFoot();
    ++m_stepIndex;
    return stepPlan;
}



//StepPlan::Ptr
//StepTrajectoryGenerator::
//generateNextStep0()
//{
//    WalkPlan::Ptr walkPlan;
//    walkPlan = m_stepPatternGenerator.buildSingleStepTrajectory( *m_stepGenerator, m_baseFoot, m_baseFootPose, m_swingFootPose );
//
//    std::cout << "Walk Plan lfoot size: " << walkPlan->leftFootPoses->size() << "\n";
//    std::cout << "Walk Plan rfoot size: " << walkPlan->rightFootPoses->size() << "\n";
//    std::cout << "Walk Plan com   size: " << walkPlan->comPoints->size() << "\n";
//
//    StepPlan::Ptr stepPlan( new StepPlan( m_baseFoot ) );
//
//    Eigen::Affine3dList::iterator lFootPoseIterator = walkPlan->leftFootPoses->begin();
//    Eigen::Affine3dList::iterator rFootPoseIterator = walkPlan->rightFootPoses->begin();
//    Eigen::Vector3dList::iterator comPoseIterator   = walkPlan->comPoints->begin();
//
//    for( int i = 0;
//                     lFootPoseIterator != walkPlan->leftFootPoses->end()
//                  && rFootPoseIterator != walkPlan->rightFootPoses->end()
//                  && comPoseIterator   != walkPlan->comPoints->end();
//                                                                          ++ lFootPoseIterator,
//                                                                          ++ rFootPoseIterator,
//                                                                          ++ comPoseIterator  )
//    {
//        StepPose::Ptr stepPose( new StepPose );
//        stepPose->swingFoot = otherFoot(m_baseFoot);
//
//        stepPose->lFootPose = *lFootPoseIterator;
//        stepPose->rFootPose = *rFootPoseIterator;
//        stepPose->comPose   = *comPoseIterator  ;
//
//        std::cout << "Walk Plan lfoot: " << lFootPoseIterator->translation().transpose() << "\n";
//        std::cout << "Walk Plan rfoot: " << rFootPoseIterator->translation().transpose() << "\n";
//        std::cout << "Walk Plan com  : " << comPoseIterator ->transpose() << "\n\n";
//
//        stepPlan->stepPoses().push_back( stepPose );
//    }
//
//    m_baseFoot = stepPlan->swingFoot();
//    return stepPlan;
//}


Eigen::Affine3dListPtr
StepTrajectoryGenerator::
generateTrajectory( const Eigen::Affine3d & currentPose, const Eigen::Affine3d & nextPose, const Eigen::Affine3dListPtr & trajectoryArg )
{
    Eigen::Affine3dListPtr trajectory = trajectoryArg;
    if( ! trajectory )
        trajectory.reset( new Eigen::Affine3dList );

    Eigen::Affine3d    stepDiff = currentPose.inverse() * nextPose ;
    Eigen::Quaterniond diffQuat( stepDiff.rotation() );

    double stepPeriod = m_defaultStepPeriod * std::max(stepDiff.translation().norm(), m_maxZ)/m_defaultStepLength;

    Spline xSpline             = createXSpline(           stepDiff.translation().x(), stepPeriod );
    Spline ySpline             = createYSpline(           stepDiff.translation().y(), stepPeriod );
    Spline zSpline             = createZSpline(           stepDiff.translation().x(), stepPeriod );
    Spline anklePitchSpline    = createAnkleRotSpline(      stepPeriod );
    Spline groundContactSpline = createGroundContactSpline( stepPeriod );


    int numTimeSteps = std::ceil( stepPeriod / m_dt );
    for( int timeStep = 1; timeStep <= numTimeSteps && ros::ok(); ++timeStep ) // starts a 1 becaus we dont need a point at current location
    {
        double timeVal          = timeStep * m_dt;
        double completionFactor = (float)timeStep/numTimeSteps;
        Eigen::Affine3d trajectoryPose = Eigen::Affine3d::Identity();

        trajectoryPose.translation().x() = xSpline.eval( timeVal )(0);
        trajectoryPose.translation().y() = ySpline.eval( timeVal )(0);
//        trajectoryPose.translation().y() = stepDiff.translation().y() * completionFactor;
        trajectoryPose.translation().z() = zSpline.eval( timeVal )(0);
        trajectoryPose.linear()          = Eigen::AngleAxisd( anklePitchSpline.eval( timeVal )(0), Eigen::YAxis3d ).toRotationMatrix();
        trajectoryPose.linear()          = Eigen::Quaterniond::Identity().slerp( completionFactor, diffQuat ) * trajectoryPose.rotation() ;
        trajectoryPose                   = currentPose * trajectoryPose;

        trajectory->push_back( trajectoryPose );
    }

    if( trajectory->size() == 0 )
    {
        ROS_ERROR_STREAM( "Empty Trajectory" );
        ROS_ERROR_STREAM( "num timesteps was  " << numTimeSteps );
        ROS_ERROR_STREAM( "step period length " << stepPeriod   );
        ROS_ERROR_STREAM( "stepDiff " << stepDiff.translation().transpose()   );
        ROS_ERROR_STREAM( "currentPose " << currentPose.translation().transpose() );
        ROS_ERROR_STREAM( "nextPose " << nextPose.translation().transpose() );
    }

    return trajectory;
}



Spline
StepTrajectoryGenerator::
createXSpline( double stepLength, double stepPeriod )
{
    Eigen::VectorXd keySpaceValues(5);
    double startLoc = 0;
    double endLoc   = stepLength;

    keySpaceValues[0] = startLoc;
    keySpaceValues[1] = startLoc + (m_ankleHeight * sin(m_liftoffAnklePitch))   + (m_foreFootLen * ( 1 - cos(m_liftoffAnklePitch)   ));
    keySpaceValues[2] = startLoc + stepLength/2;
    keySpaceValues[3] = endLoc   - (m_ankleHeight * sin(m_touchdownAnklePitch)) - (m_aftFootLen  * ( 1 - cos(m_touchdownAnklePitch) ));
    keySpaceValues[4] = endLoc;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keySpaceValues, keyTimes, startCondition, endCondition );
    return spline;
}


Spline
StepTrajectoryGenerator::
createYSpline( double stepLength, double stepPeriod )
{
    Eigen::VectorXd keySpaceValues(5);
    double startLoc = 0;
    double endLoc   = stepLength;

    keySpaceValues[0] = startLoc;
    keySpaceValues[1] = startLoc;
    keySpaceValues[2] = (startLoc + endLoc)/2;
    keySpaceValues[3] = endLoc;
    keySpaceValues[4] = endLoc;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keySpaceValues, keyTimes, startCondition, endCondition );
    return spline;
}


Spline
StepTrajectoryGenerator::
createZSpline( double stepDist, double stepPeriod )
{
    double zStart = 0;
    double zEnd   = 0;

    Eigen::VectorXd keySpaceValues(5);

    keySpaceValues(0) = zStart;
    keySpaceValues(1) = zStart + (m_foreFootLen * sin(m_liftoffAnklePitch))   + (m_ankleHeight * cos(m_liftoffAnklePitch) ) - m_ankleHeight;
    keySpaceValues(2) = m_maxZ;// - m_ankleHeight; 0.2*(stepDist/m_defaultStepLength);//
    keySpaceValues(3) = zEnd   + (m_aftFootLen * sin(m_touchdownAnklePitch)) + (m_ankleHeight * cos(m_touchdownAnklePitch) ) - m_ankleHeight;
    keySpaceValues(4) = zEnd;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );


    Spline zSpline( keySpaceValues, keyTimes, startCondition, endCondition );
    return zSpline;
}

Spline
StepTrajectoryGenerator::
createAnkleRotSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  m_startAnklePitch;
    keyAngleValues(1) =  m_liftoffAnklePitch;
    keyAngleValues(2) = -m_touchdownAnklePitch;
    keyAngleValues(3) = -m_endAnklePitch;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline anklePitchSpline( keyAngleValues, keyTimes, startCondition, endCondition );
    return anklePitchSpline;
}

Spline
StepTrajectoryGenerator::
createGroundContactSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  0;
    keyAngleValues(1) =  0;
    keyAngleValues(2) =  1;
    keyAngleValues(3) =  1;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keyAngleValues, keyTimes, startCondition, endCondition );

    return spline;
}

Eigen::VectorXd
StepTrajectoryGenerator::
calcTranslationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes( 5 );

    double startTime           = 0;
    double endTime             = stepPeriod;
    double doubleSupportPeriod = 0.1 * stepPeriod;
    double midpointTime        = stepPeriod/2;

    transTimes[0] = startTime;
    transTimes[1] = startTime + doubleSupportPeriod;
    transTimes[2] = startTime + midpointTime;
    transTimes[3] = endTime   - doubleSupportPeriod;
    transTimes[4] = endTime;

    return transTimes;
}

Eigen::VectorXd
StepTrajectoryGenerator::
calcRotationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes = calcTranslationTimeKeypoints( stepPeriod );
    Eigen::VectorXd rotationTimes( transTimes.size() - 1 );

    assert( rotationTimes.size() == 4 );

    rotationTimes(0) = transTimes(0) ;
    rotationTimes(1) = transTimes(1) ;
    rotationTimes(2) = transTimes(3) ;
    rotationTimes(3) = transTimes(4) ;

    return rotationTimes;
}



//Eigen::Vector4dListPtr
//comTraj( // double & Tc,
//         // double & Td,
//         double & Dt,
////         double & Ds,
//         const std::vector<double> & refT,
//         const std::vector<double> & refX,
//         const std::vector<double> & refY  )
Eigen::Vector3dListPtr
comTraj( const double & Dt,
         const Eigen::Vector3dVector & zmpRef,
         Eigen::Vector3dListPtr & zmpRefPlan,
         Eigen::Vector3d * lastComPose,
         Eigen::Vector3d * lastComVel,
         Eigen::Vector3d * lastComAccel,
         Eigen::Vector3d * lastZmpPose,
         double  doubleSupportTime )
{
    const int previewSteps = 320;

    // allocate memory for all mats
    Eigen::MatrixXd Amat = Eigen::MatrixXd::Zero(3, 3);   // Plant/System/State matrix   A * stateK0 + B * InputK0 = stateK1
    Eigen::MatrixXd Bmat = Eigen::MatrixXd::Zero(3, 1);    //              Input matrix
    Eigen::MatrixXd Cmat = Eigen::MatrixXd::Zero(1, 3);    //             Output matrix   C * stateK0 = outputK0    (zmp)

    //controller matrices
    // Controller generates inputs
    // -gX * stateK0 - gI * errorVector - gD * previewInput
    //
    Eigen::MatrixXd Qx      = Eigen::MatrixXd::Zero(3, 3);    //  State Weights
//    Eigen::MatrixXd  Qx     = Eigen::MatrixXd::Identity(3,3);//  State Weights
    Eigen::MatrixXd R       = Eigen::MatrixXd::Zero(1, 1);    //  Input Weights
    Eigen::MatrixXd Qe      = Eigen::MatrixXd::Zero(1, 1);    //  OuputError Weights

    Eigen::MatrixXd B_tilde = Eigen::MatrixXd::Zero(4, 1);    //
    Eigen::MatrixXd A_tilde = Eigen::MatrixXd::Zero(4, 4);    //
    Eigen::MatrixXd I_tilde = Eigen::MatrixXd::Zero(4, 1);    //
    Eigen::MatrixXd Ip      = Eigen::MatrixXd::Zero(1, 1);    //  for I_tilde
    Eigen::MatrixXd F_tilde = Eigen::MatrixXd::Zero(4, 3);    //
    Eigen::MatrixXd Q_tilde = Eigen::MatrixXd::Zero(4, 4);    //
    Eigen::MatrixXd K_tilde = Eigen::MatrixXd::Zero(4, 4);    //  solution to Ricatti equation
    Eigen::MatrixXd X_tilde = Eigen::MatrixXd::Zero(4, previewSteps);   // for G_d
    Eigen::MatrixXd Ac_tilde;                                 // for G_d
    Eigen::MatrixXd G_I;                                      // integral gain
    Eigen::MatrixXd G_X;                                      // proportional gain
    Eigen::MatrixXd G_d = Eigen::MatrixXd::Zero(1, previewSteps);   // preview gain

    // dynamic system
    double Ts = Dt;     // timestep
    double Zc = 0.814;  // height of CoM
    double g  = 9.81;    // gravity

    Amat << 1, Ts, Ts * Ts / 2,
            0, 1, Ts,
            0, 0, 1;
    Bmat << Ts * Ts * Ts / 6,
            Ts * Ts / 2,
            Ts;
    Cmat << 1,
            0,
            -Zc / g;


    // weights
    R  << 1e-6;
    Qe << 10;

    // augmented system
    B_tilde.block(0, 0, 1, 1) = Cmat * Bmat;
    B_tilde.block(1, 0, 3, 1) = Bmat;

    F_tilde.block(0, 0, 1, 3) = Cmat * Amat;
    F_tilde.block(1, 0, 3, 3) = Amat;

    Ip      << 1;
    I_tilde << Ip, 0, 0, 0;

    Q_tilde.block(0, 0, 1, 1) = Qe;

    A_tilde.block(0, 0, 4, 1) = I_tilde;
    A_tilde.block(0, 1, 4, 3) = F_tilde;



    // Discrete-Time Riccati equation solution
    // TODO:: need to get a DARE solver that gives correct results, for now we have a solution for the system with T=0.005 from MATLAB
    //    K_tilde = re2uta::dare(A_tilde,B_tilde,R,Q_tilde);

    //for Zc = 0.814
    K_tilde <<      1166.7,        67481,        19608,        49.006,
                    67481,         3.9517e+06,   1.1483e+06,   2885.4,
                    19608,         1.1483e+06,   3.3369e+05,   838.58,
                    49.006,        2885.4,       838.58,       2.1401;
//----------------------------------------------------------------------
    //for Zc = 0.7
/*    K_tilde <<  1083.8,        58191,        15691,       39.269,
                 58191,   3.1686e+06,   8.5447e+05,       2153.8,
                 15691,   8.5447e+05,   2.3042e+05,       580.92,
                39.269,       2153.8,       580.92,       1.4962;*/
    //----------------------------------------------------------------------
        //for Zc = 0.9
/*   K_tilde <<  1225.6,        74492,        22750,        56.82,
                 74492,   4.5797e+06,   1.3987e+06,       3508.9,
                 22750,   1.3987e+06,   4.2719e+05,       1071.8,
                 56.82,       3508.9,       1071.8,       2.7224;*/

    Eigen::MatrixXd temp3;
    // calculate controller gains
    temp3 = (R + B_tilde.transpose() * K_tilde * B_tilde).inverse() * B_tilde.transpose();

    G_I = temp3 * K_tilde * I_tilde; //integral gain
    G_X = temp3 * K_tilde * F_tilde; // proportional gain

    ///////// preview gain
    G_d.block(0, 0, 1, 1) = -G_I;

    Ac_tilde = A_tilde - B_tilde * temp3 * K_tilde * A_tilde;
    X_tilde.block(0, 0, 4, 1) = -Ac_tilde.transpose() * K_tilde * I_tilde;

    int Icount;
    for (Icount = 1; Icount < previewSteps; Icount++)
    {
        X_tilde.block(0, Icount, 4, 1) = Ac_tilde.transpose() * X_tilde.col(Icount - 1);
        G_d.block(0, Icount, 1, 1) = temp3 * X_tilde.col(Icount - 1);// preview gain
    }
    ///////


    // generating reference trajectory for ZMP
    //double timeLength = (FootStepNum.data + 1)*Tc + Td;// + Ts;
    double timeLength   = zmpRef.back()(2);
    size_t numTimeSteps = std::ceil( timeLength / Dt );

    Eigen::MatrixXd refx = Eigen::MatrixXd::Zero(1, numTimeSteps + previewSteps);
    Eigen::MatrixXd refy = Eigen::MatrixXd::Zero(1, numTimeSteps + previewSteps);

    size_t timeIndex;
    size_t refIndex = 0;


    for( timeIndex = 0; timeIndex < numTimeSteps + previewSteps && ros::ok(); ++timeIndex )
    {
        double currentTime = timeIndex * Dt;

        while(    refIndex + 1 <  zmpRef.size()           // once we've passed the next time ref, keep stepping forward
               && currentTime  >= zmpRef[refIndex+1](2) ) // until we find a time ref that is beyond our current time, and advance
        {                                                // to the ref just before it
            ++refIndex;
        }


       if (   ( currentTime >= ( zmpRef[refIndex+1](2) - doubleSupportTime) )
            && ( currentTime <=   zmpRef[refIndex+1](2) ) )
        {
            // to have a staight line with 45 degree slope for ZMP transition in double support
            // x = (x2 - x1)(T - T1)/(T2 - T1) + x1
            // T: current time      T1 = ZMP time index 1      T2 = ZMP time index 2
            // x: current ZMP location      x1 = ZMP location index 1      x2 = ZMP location index 2

                 if ( currentTime <= zmpRef[1](2) )
                   {
                           refx(0, timeIndex) = (zmpRef[1].x() - zmpRef[0].x())*(currentTime - (zmpRef[1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[0].x();
                        refy(0, timeIndex) = (zmpRef[1].y() - zmpRef[0].y())*(currentTime - (zmpRef[1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[0].y();
                   }
                 else
                 {

                    refx(0, timeIndex) = ( zmpRef[refIndex+1].x() - zmpRef[refIndex].x() ) * ( currentTime - ( zmpRef[refIndex+1](2) - doubleSupportTime ) )
                                         / doubleSupportTime

                                         + zmpRef[refIndex].x();

                    refy(0, timeIndex) = ( zmpRef[refIndex+1].y() - zmpRef[refIndex].y() ) * ( currentTime - ( zmpRef[refIndex+1](2) - doubleSupportTime ) )
                                         / doubleSupportTime

                                         + zmpRef[refIndex].y();
                 }



//            if ( currentTime <= zmpRef[1](2) )
//            {
//                refx(0, timeIndex) = (zmpRef[refIndex+1].x() - zmpRef[refIndex].x())*(currentTime - (zmpRef[refIndex+1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[refIndex].x();
//                refy(0, timeIndex) = (zmpRef[refIndex+1].y() - zmpRef[refIndex].y())*(currentTime - (zmpRef[refIndex+1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[refIndex].y();
//
//            }
//            else
//            {
//                refx(0, timeIndex) = (zmpRef[refIndex+1].x() - zmpRef[refIndex].x())*(currentTime - (zmpRef[refIndex+1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[refIndex].x();
//                refy(0, timeIndex) = (zmpRef[refIndex+1].y() - zmpRef[refIndex].y())*(currentTime - (zmpRef[refIndex+1](2) - doubleSupportTime))/doubleSupportTime + zmpRef[refIndex].y();
//            }

        }
        else
        {

            refx(0, timeIndex) = zmpRef[refIndex].x();
            refy(0, timeIndex) = zmpRef[refIndex].y();


        }
        Eigen::Vector3d zmpTrajPoint;
        zmpTrajPoint.x() = zmpRef[refIndex].x();
        zmpTrajPoint.y() = zmpRef[refIndex].y();
        zmpRefPlan->push_back(zmpTrajPoint);
    }


    // simulate to get COM trajectory
    Eigen::MatrixXd eVecX  = Eigen::MatrixXd::Zero(numTimeSteps, 1);               //  error     output - desired
    Eigen::MatrixXd eVecY  = Eigen::MatrixXd::Zero(numTimeSteps, 1);               //  error     output - desired
    Eigen::MatrixXd uPrevx = Eigen::MatrixXd::Zero(1, 1);                          //  preview input
    Eigen::MatrixXd uPrevy = Eigen::MatrixXd::Zero(1, 1);                          //  preview input
    Eigen::MatrixXd utempX = Eigen::MatrixXd::Zero(1, 1);                          //
    Eigen::MatrixXd utempY = Eigen::MatrixXd::Zero(1, 1);                          //
    Eigen::MatrixXd uX     = Eigen::MatrixXd::Zero(1, 1);                          //  total input :  preview + integral + porportional   (in jerk)
    Eigen::MatrixXd uY     = Eigen::MatrixXd::Zero(1, 1);                          //  total input :  preview + integral + porportional   (in jerk)
    Eigen::MatrixXd xVec   = Eigen::VectorXd::Zero(3);    //states in x-direction  //  position velocity acceleration (initial value important)
    Eigen::MatrixXd yVec   = Eigen::VectorXd::Zero(3);    //states in y-direction  //  position velocity acceleration (initial value important)
    Eigen::MatrixXd yX     = Eigen::MatrixXd::Zero(1, 1); //output in x-direction  //  output (zmp)    ( should have initial value of current zmp )
    Eigen::MatrixXd yY     = Eigen::MatrixXd::Zero(1, 1); //output in y-direction  //  output (zmp)    ( should have initial value of current zmp )


    xVec(0) = lastComPose ->x();
    yVec(0) = lastComPose ->y();

    xVec(1) = lastComVel  ->x();
    yVec(1) = lastComVel  ->y();

    xVec(2) = lastComAccel->x();
    yVec(2) = lastComAccel->y();

    Eigen::Vector3dListPtr comPlan( new Eigen::Vector3dList );

    for( size_t simStep = 0; simStep < numTimeSteps && ros::ok(); ++simStep )
    {
        // X direction simulation
        uPrevx = Eigen::MatrixXd::Zero(1, 1); // forget old preview
        uPrevy = Eigen::MatrixXd::Zero(1, 1);

        for( int previewStep = 0; previewStep < previewSteps; ++previewStep )
        {
            utempX.block(0, 0, 1, 1) = G_d.col(previewStep) * refx.col(previewStep + simStep);
            uPrevx.block(0, 0, 1, 1) = (uPrevx + utempX);

            utempY.block(0, 0, 1, 1) = G_d.col(previewStep) * refy.col(previewStep + simStep);
            uPrevy.block(0, 0, 1, 1) = (uPrevy + utempY);
        }

        eVecX(simStep, 0) = yX(0, 0) - refx(0, simStep);
        eVecY(simStep, 0) = yY(0, 0) - refy(0, simStep);

        uX   = (-G_I * eVecX.sum() - G_X * xVec - uPrevx);
        uY   = (-G_I * eVecY.sum() - G_X * yVec - uPrevy);

        xVec = Amat * xVec + Bmat * uX;
        yVec = Amat * yVec + Bmat * uY;

        yX   = Cmat * xVec;
        yY   = Cmat * yVec;


        Eigen::Vector3d trajPoint;

        trajPoint.x()  = xVec(0);
        trajPoint.y()  = yVec(0);
        trajPoint.z()  = Zc; // viz debug + simCount  * Dt * 0.1;

        comPlan->push_back( trajPoint );
    }


    lastZmpPose ->x() = yX(0,0);
    lastZmpPose ->y() = yY(0,0);

    std::cout << "zmp sim  (x,y): " << yX(0,0) << " , " << yY(0,0) << "\n";



    lastComPose ->x() = xVec(0) ;
    lastComPose ->y() = yVec(0) ;

    std::cout << "COM sim  (x,y): " << xVec(0) << " , " << yVec(0) << "\n";

    lastComVel  ->x() = xVec(1) ;
    lastComVel  ->y() = yVec(1) ;

    lastComAccel->x() = xVec(2) ;
    lastComAccel->y() = yVec(2) ;


    return comPlan;
}



}



