/*
 * WalkPatternGenerator.cpp
 *
 *  Created on: May 29, 2013
 *      Author: Ghassan Atmeh, andrew.somerville
 */

#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2uta/splines/Spline.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Core>
#include <boost/foreach.hpp>
#include <iostream>
#include <ros/ros.h>



namespace re2uta
{


//Eigen::Vector3dListPtr
//comTraj( const double & Dt,
//         const Eigen::Vector3dVector & comRef  );


WalkPatternGenerator::
WalkPatternGenerator()
{
    m_defaultStepLength    = 0.3;
    m_ankleHeight          = 0.081;
    m_foreFootLen          = 0.178;
    m_aftFootLen           = 0.082;
    m_apexX                = m_defaultStepLength/2;
    m_liftoffAnklePitch    = 0; //3.1416/30;
    m_touchdownAnklePitch  = 0; //3.1416/30;
    m_startAnklePitch      = 0;
    m_endAnklePitch        = 0;
    m_maxZ                 = 0.07;
    m_stepPeriod           = 5;
    m_dt                   = 0.005;
    m_leftFirst            = true;
    m_zmpRefPlan.reset( new Eigen::Vector3dList );
}



//WalkPlan
//WalkPatternGenerator::
//buildStraightLineWalkPlan( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength )
//{
//    StraightLineStepGenerator stepGenerator( source, dest, stepLength );
//    return buildTrajectories( stepGenerator );
//}


WalkPlan
WalkPatternGenerator::
buildCircleBasedWalkPlan( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength )
{
    CirclePathStepGenerator stepGenerator( source, dest, stepLength, 0.15, LEFT_FOOT );
    return buildTrajectories( stepGenerator );
}


WalkPlan
WalkPatternGenerator::
buildTrajectories( StepGenerator & stepGenerator )
{
//    int stepParts = numSteps+1;
    Eigen::Vector3dVector comRef; //( stepParts+1, Eigen::Vector3d(0,0,0) );

    re2::VisualDebugPublisher vizdebug( "foo" );

    WalkPlan walkPlan;
    Eigen::Affine3dListPtr swingFootPoses;
    Eigen::Affine3dListPtr baseFootPoses;

    Eigen::Affine3d currentPose = Eigen::Affine3d::Identity();
    Eigen::Affine3d nextPose    = Eigen::Affine3d::Identity();

    // Initial left and right foot poses

    double time = 0; //FIXME!!! why does setting this to 0 mess things up?
                     // Andy, this is because this is the time taken to move the COM from (0, 0, 0) or stepGenerator.source() to the first support leg location
    double timeForDoubleSupport = 2;

    for(  ; time < timeForDoubleSupport; time += m_dt )
    {
        walkPlan.leftFootPoses->push_back(  stepGenerator.source()*Eigen::Translation3d( 0, 0.15, 0 ) ); //m_ankleHeight ) );
        walkPlan.rightFootPoses->push_back( stepGenerator.source()*Eigen::Translation3d( 0,-0.15, 0 ) ); //m_ankleHeight ) );
    }

    Eigen::Vector3d comRefPoint;
//    if( m_leftFirst )
//        comRefPoint.head(2) = walkPlan.rightFootPoses->back().translation().head(2);
//    else
//        comRefPoint.head(2) = walkPlan.leftFootPoses->back().translation().head(2);

    comRefPoint.head(2) = stepGenerator.source().translation().head(2) - 0.1*stepGenerator.source().translation().head(2);

    comRefPoint(2)      = 0;
    comRef.push_back( comRefPoint );


    int stepIndex = 0;
    while( !stepGenerator.hasArrived( walkPlan ) && ros::ok() )
    {
        bool left = ((stepIndex + (int)!m_leftFirst) % 2 == 0); // even number for left foot unless m_leftFoot is false

        Eigen::Affine3d initialSwingFootPose;
        Eigen::Affine3d initialBaseFootPose ;

        if( left )
        {
            swingFootPoses = walkPlan.leftFootPoses;
            baseFootPoses  = walkPlan.rightFootPoses;
        }
        else
        {
            swingFootPoses = walkPlan.rightFootPoses;
            baseFootPoses  = walkPlan.leftFootPoses;
        }

        int previousTrajectoryPoints = swingFootPoses->size();

        // ##########################
        // PLAN SINGLE STEP
        planSingleStep( stepGenerator, swingFootPoses, baseFootPoses, comRef, stepIndex, m_defaultStepLength, m_stepPeriod, m_dt, time );
        // ##########################

        int newTrajectoryPoints = swingFootPoses->size() - previousTrajectoryPoints;
        time += newTrajectoryPoints * m_dt;

        //FIXME add all time from last chunk
        ++stepIndex;
    }

    comRefPoint.head(2) = stepGenerator.dest().translation().head(2);
    comRefPoint(2)      = time;
    comRef.push_back( comRefPoint );

    comRefPoint.head(2) = (swingFootPoses->back().translation().head(2) + baseFootPoses->back().translation().head(2)) / 2;
    comRefPoint(2)      = time + m_stepPeriod/2;
    comRef.push_back( comRefPoint );

    Eigen::Vector3d dummy(0,0,0);
    double dummyDouble = 0.5;


    // ##########################
    // Calc COM Points
    walkPlan.comPoints = comTraj( m_dt, comRef, m_zmpRefPlan, &dummy, &dummy, &dummy , &dummy , timeForDoubleSupport );
    // ##########################

    std::cout << "Plan:\n" ;
    std::cout << " lpoints:" << walkPlan.leftFootPoses->size()  << "\n" ;
    std::cout << " rpoints:" << walkPlan.rightFootPoses->size() << "\n" ;
    std::cout << " compts:"  << walkPlan.comPoints->size()      << "\n" ;

    return walkPlan;
}


WalkPlan::Ptr
WalkPatternGenerator::
buildSingleStepTrajectory( StepGenerator & stepGenerator, Foot baseFoot, const Eigen::Affine3d & baseFootPose, const Eigen::Affine3d & swingFootPose )
{
    Eigen::Vector3dVector comRef; //( stepParts+1, Eigen::Vector3d(0,0,0) );

    WalkPlan::Ptr walkPlan( new WalkPlan );
    Eigen::Affine3dListPtr swingFootPoses;
    Eigen::Affine3dListPtr baseFootPoses;

    double time      = 1; //FIXME!!! why does setting this to 0 mess things up?
    int    stepIndex = 0;

    Eigen::Vector3d comRefPoint;
    if( baseFoot == LEFT_FOOT )
    {
        swingFootPoses = walkPlan->rightFootPoses;
        baseFootPoses  = walkPlan->leftFootPoses;
    }
    else
    {
        swingFootPoses = walkPlan->leftFootPoses;
        baseFootPoses  = walkPlan->rightFootPoses;
    }

    swingFootPoses->push_back( swingFootPose );
    baseFootPoses->push_back(  baseFootPose  );
    comRefPoint.head(2) = baseFootPose.translation().head(2);

    comRefPoint(2)      = 0; // time 0
    comRef.push_back( comRefPoint );

    int previousTrajectoryPoints = swingFootPoses->size();

    planSingleStep( stepGenerator, swingFootPoses, baseFootPoses, comRef, stepIndex, m_defaultStepLength, m_stepPeriod, m_dt, time );

    int newTrajectoryPoints = swingFootPoses->size() - previousTrajectoryPoints;
    time += newTrajectoryPoints * m_dt;

    ++stepIndex;

    comRefPoint.head(2) = swingFootPoses->back().translation().head(2);
    comRefPoint(2)      = time;
    comRef.push_back( comRefPoint );

//    comRefPoint.head(2) = (swingFootPoses->back().translation().head(2) + baseFootPoses->back().translation().head(2)) / 2;
//    comRefPoint(2)      = time + m_stepPeriod/2;
//    comRef.push_back( comRefPoint );

    Eigen::Vector3d dummy(0,0,0);
    double dummyDouble = 0.5;
    Eigen::Vector3dListPtr zmpRefPlanDummy;
    walkPlan->comPoints = comTraj( m_dt,comRef, m_zmpRefPlan, &dummy, &dummy, &dummy, &dummy, dummyDouble );

    return walkPlan;
}



void
WalkPatternGenerator::
planSingleStep( StepGenerator &          stepGenerator,
                Eigen::Affine3dListPtr & swingFootPoses,
                Eigen::Affine3dListPtr & baseFootPoses,
                Eigen::Vector3dVector &  comRef,
                int                      stepIndex,
                double                   defaultStepLength,
                double                   defaultStepPeriod,
                double                   dt,
                double                   time )
{
    Eigen::Vector3d comRefPoint;
    comRefPoint.head(2) = baseFootPoses->back().translation().head(2);
    comRefPoint(2)      = time;
    comRef.push_back( comRefPoint );

    Eigen::Affine3d currentPose = swingFootPoses->back();
    Eigen::Affine3d nextPose    = stepGenerator.generateNextStep( baseFootPoses->back(), currentPose, stepIndex );

    Eigen::Affine3d    stepDiff = currentPose.inverse() * nextPose ;
    Eigen::Quaterniond diffQuat( stepDiff.rotation() );

    double stepPeriod = defaultStepPeriod * stepDiff.translation().x()/defaultStepLength;

    Spline xSpline             = createTranslationSpline( stepDiff.translation().x(), stepPeriod );
    Spline ySpline             = createTranslationSpline( stepDiff.translation().y(), stepPeriod );
    Spline zSpline             = createZSpline(           stepDiff.translation().x(), stepPeriod );
    Spline anklePitchSpline    = createAnkleRotSpline(                                stepPeriod );
    Spline groundContactSpline = createGroundContactSpline(                           stepPeriod );


    int numTimeSteps = stepPeriod / dt;
    for( int timeStep = 1; timeStep <= numTimeSteps && ros::ok(); ++timeStep )
    {
        double timeVal          = timeStep * dt;
        double completionFactor = (float)timeStep/numTimeSteps;
        Eigen::Affine3d trajectoryPose = Eigen::Affine3d::Identity();

        trajectoryPose.translation().x() = xSpline.eval( timeVal )(0);
//            trajectoryPose.translation().y() = ySpline.eval( timeVal )(0);
        trajectoryPose.translation().y() = stepDiff.translation().y() * completionFactor;
        trajectoryPose.translation().z() = zSpline.eval( timeVal )(0);
        trajectoryPose.linear()          = Eigen::AngleAxisd( anklePitchSpline.eval( timeVal )(0), Eigen::YAxis3d ).toRotationMatrix();
        trajectoryPose.linear()          = trajectoryPose.rotation() *  Eigen::Quaterniond().slerp( completionFactor, diffQuat );
        trajectoryPose                   = currentPose * trajectoryPose;

        swingFootPoses->push_back( trajectoryPose );

        // Opposite foot should stay at the last location until we switch back to it
        baseFootPoses->push_back( baseFootPoses->back() );
    }

}







Spline
WalkPatternGenerator::
createTranslationSpline( double stepLength, double stepPeriod )
{
    Eigen::VectorXd keySpaceValues(5);
    double startLoc = 0;
    double endLoc   = stepLength;

    keySpaceValues[0] = startLoc;
    keySpaceValues[1] = startLoc + (m_ankleHeight * sin(m_liftoffAnklePitch))   + (m_foreFootLen * ( 1 - cos(m_liftoffAnklePitch)   ));
    keySpaceValues[2] = startLoc + stepLength/2;
    keySpaceValues[3] = endLoc   - (m_ankleHeight * sin(m_touchdownAnklePitch)) - (m_aftFootLen  * ( 1 - cos(m_touchdownAnklePitch) ));
    keySpaceValues[4] = endLoc;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keySpaceValues, keyTimes, startCondition, endCondition );

//    std::cout << "X Spline key values " << keySpaceValues.transpose() << std::endl;
//    std::cout << "X Spline key times " <<  keyTimes.transpose() << std::endl;
//    std::cout << "X Spline key deriv " << m_keySpaceValuesDerivT.transpose() << std::endl;

    return spline;
}

//Spline
//WalkPatternGenerator::
//createYSpline()
//{
//    Eigen::VectorXd keySpaceValues(5);
//
//    keySpaceValues[0] = 0;
//    keySpaceValues[1] = 0;
//    keySpaceValues[2] = 0;
//    keySpaceValues[3] = 0;
//    keySpaceValues[4] = 0;
//
//    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( m_stepPeriod );
//
//    typedef Spline::EndPointCondition EndPointCondition;
//
//    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
//    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );
//
//    Spline ySpline( keySpaceValues, keyTimes, startCondition, endCondition );
//    return ySpline;
//}

Spline
WalkPatternGenerator::
createZSpline( double stepDist, double stepPeriod )
{
    double zStart = 0;
    double zEnd   = 0;

    Eigen::VectorXd keySpaceValues(5);

    keySpaceValues(0) = zStart;
    keySpaceValues(1) = zStart + (m_foreFootLen * sin(m_liftoffAnklePitch))   + (m_ankleHeight * cos(m_liftoffAnklePitch) ) - m_ankleHeight;
    keySpaceValues(2) = m_maxZ;// - m_ankleHeight; 0.2*(stepDist/m_defaultStepLength);//
    keySpaceValues(3) = zEnd   + (m_aftFootLen * sin(m_touchdownAnklePitch)) + (m_ankleHeight * cos(m_touchdownAnklePitch) ) - m_ankleHeight;
    keySpaceValues(4) = zEnd;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );


    Spline zSpline( keySpaceValues, keyTimes, startCondition, endCondition );


    return zSpline;
}

Spline
WalkPatternGenerator::
createAnkleRotSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  m_startAnklePitch;
    keyAngleValues(1) =  m_liftoffAnklePitch;
    keyAngleValues(2) = -m_touchdownAnklePitch;
    keyAngleValues(3) = -m_endAnklePitch;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline anklePitchSpline( keyAngleValues, keyTimes, startCondition, endCondition );

    return anklePitchSpline;
}

Spline
WalkPatternGenerator::
createGroundContactSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  0;
    keyAngleValues(1) =  0;
    keyAngleValues(2) =  1;
    keyAngleValues(3) =  1;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keyAngleValues, keyTimes, startCondition, endCondition );

    return spline;
}

Eigen::VectorXd
WalkPatternGenerator::
calcTranslationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes( 5 );

    double startTime           = 0;
    double endTime             = stepPeriod;
    double doubleSupportPeriod = 0.1 * stepPeriod;
    double midpointTime        = stepPeriod/2;

    transTimes[0] = startTime;
    transTimes[1] = startTime + doubleSupportPeriod;
    transTimes[2] = startTime + midpointTime;
    transTimes[3] = endTime   - doubleSupportPeriod;
    transTimes[4] = endTime;

    return transTimes;
}

Eigen::VectorXd
WalkPatternGenerator::
calcRotationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes = calcTranslationTimeKeypoints( stepPeriod );
    Eigen::VectorXd rotationTimes( transTimes.size() - 1 );

    assert( rotationTimes.size() == 4 );

    rotationTimes(0) = transTimes(0) ;
    rotationTimes(1) = transTimes(1) ;
    rotationTimes(2) = transTimes(3) ;
    rotationTimes(3) = transTimes(4) ;

    return rotationTimes;
}











}



