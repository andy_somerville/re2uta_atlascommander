/*
 * SynchTest.cpp
 *
 *  Created on: May 10, 2013
 *      Author: andrew.somerville
 */

#include <ros/ros.h>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread.hpp>
#include <boost/thread/locks.hpp>
#include <string>

class SynchTest
{
    public:
        boost::condition_variable        m_cond;
        boost::mutex                     m_mutex;

        void producterLoop()
        {
            std::string foo;
            while( ros::ok() )
            {
                std::cin >> foo;
                ROS_INFO_STREAM( "Just got input, notifying " << foo );
                m_cond.notify_all();
            }
        }

        void consumerLoop()
        {
            boost::unique_lock<boost::mutex> lock( m_mutex );

            while( ros::ok() )
            {
                if( m_cond.timed_wait( lock, boost::posix_time::millisec( 2000 ) ) )
                {
                    ROS_INFO_STREAM( "Consumer Lock released, but could be \"spurious\" " << boost::this_thread::get_id() );
                }
                else
                {
                    ROS_INFO( "Timed out waiting. Waiting again." );
                }
            }

        }

        void go()
        {
            boost::thread producerThread(  &SynchTest::producterLoop, this );
            boost::thread consumerThread0( &SynchTest::consumerLoop,  this );
            boost::thread consumerThread1( &SynchTest::consumerLoop,  this );
            boost::thread consumerThread2( &SynchTest::consumerLoop,  this );

            ros::spin();

            producerThread.join();
            consumerThread0.join();
            consumerThread1.join();
            consumerThread2.join();
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "synch_test" );
    ROS_INFO( "Starting synch_test" );

    SynchTest synchTest;

    synchTest.go();
    ROS_INFO( "go function exited" );

    return 0;
}
