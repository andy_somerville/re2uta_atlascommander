/*
 * FullBodyControllerTest.cpp
 *
 *  Created on: May 14, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/FullBodyController.hpp>
#include <re2/eigen/eigen_util.h>
#include <boost/random.hpp>
#include <ros/ros.h>
#include <boost/foreach.hpp>

using namespace re2uta::atlascommander;

class TestJointController : public JointController
{
    public:
        TestJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar ) : JointController( syncVar ) {}
        virtual ~TestJointController() {}

        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime )
        {
            ROS_INFO_STREAM( "Joint command set: " << *msg );
        }
};



class FullBodyControllerTest
{
    public:
        ros::NodeHandle m_node;

    public:
        void go()
        {
            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            boost::condition_variable foo;

            TestJointController::Ptr testJointController( new TestJointController( ros::Duration(0.1), foo ) );
            FullBodyController fullBodyController( urdfModel, foo, testJointController );

            atlas_msgs::AtlasState::Ptr atlasState( new atlas_msgs::AtlasState );

            atlasState->position.resize( 28 );
            atlasState->velocity.resize( 28 );

            BOOST_FOREACH( float & position, atlasState->position )
                position = 0.1;

            BOOST_FOREACH( float & velocity, atlasState->velocity )
                velocity = 0.1;

//            fullBodyController.setHandPositionConstraint();

            boost::mt19937 rng;
            boost::uniform_real<double> u(0, 0.5);
            boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > gen(rng, u);

            Eigen::Affine3d originalHandPose = Eigen::Affine3d::Identity();
            originalHandPose.translation() = Eigen::Vector3d( 0.1, 0.2, 1.0 );

            Eigen::Vector6d handWeights;
            //originalHandPose = fullBodyController.getHandPose(); //FIXME this is a problem
            handWeights << 0.1, 1, 1, 0, 0, 0;

            double radius = 0.05;

            while( ros::ok() )
            {
                ros::spinOnce();

                double angleRad = std::fmod( ros::Time::now().toSec(), 4 ) / 4  * 2 * M_PI;
                Eigen::Affine3d handPose = originalHandPose;
                handPose.translation().y() += std::cos( angleRad ) * radius;
                handPose.translation().z() += std::sin( angleRad ) * radius;

                FrameGoal::Ptr frameGoal( new FrameGoal );

                frameGoal->baseFrameId = "l_foot";
                frameGoal->goalFrameId = "l_hand";
                frameGoal->tipFrameId  = "l_hand";
                frameGoal->goalPose    = handPose;
                frameGoal->weightMatrix= handWeights.asDiagonal();

                fullBodyController.setFrameGoal( frameGoal );

                ros::Duration( gen() ).sleep();
                ROS_INFO_STREAM( "sleep done" );
                ROS_INFO_STREAM( "angle: " << angleRad << "  y: " << handPose.translation().y() << "  z: " << handPose.translation().z() );

                atlasState->header.stamp = ros::Time::now();
                fullBodyController.setLatestStateMsg( atlasState );
                foo.notify_all();
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "full_body_controller_test" );
    ROS_INFO( "Starting full_body_controller_test" );

    FullBodyControllerTest fullBodyControllerTest;

    fullBodyControllerTest.go();

    return 0;
}
