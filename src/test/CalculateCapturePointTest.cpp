/**
 *
 * calculateCapturepoint.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Apr 24, 2013
 * @modified Apr 24, 2013
 *
 */

#include <ros/ros.h>

#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>

#include <std_msgs/Int32.h>

#include <std_msgs/Empty.h>
#include <tf/transform_listener.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <re2uta/CalculateCapturePoint.hpp>

#include <re2uta/WalkPatternGenerator.hpp>
//#include <re2uta/StepGenerator.hpp>
#include <re2uta/splines/Spline.hpp>

using namespace re2uta;


class AtlasCaptureNodeTest
{
    private:

        ros::NodeHandle       m_node;

        AtlasCaptureNode::Ptr m_atlasCaptureNode;

        ros::Subscriber       m_atlasStateSubscriber;

        ros::Publisher        m_instantaneousCapturePointPublisher;
        ros::Publisher        m_propagatedCapturePointPublisher;
        ros::Publisher        m_stopTrajectoryPublisher;
        ros::Publisher        m_supportPolygonPublisher;

        Eigen::Vector3d       m_instantaneousCapturePoint;


    public:

        /*
         * This is the constructor for the AtlasCaptureNode
         */
        AtlasCaptureNodeTest()
        {

            std::string urdfString;

            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            m_atlasCaptureNode.reset( new AtlasCaptureNode( urdfModel ) );

            m_atlasStateSubscriber = m_node.subscribe( "/atlas/atlas_state" , 1, &AtlasCaptureNodeTest::handleAtlasState , this );

            m_instantaneousCapturePointPublisher  = m_node.advertise<visualization_msgs::Marker>("/atlas/instantaneousCapturePoint", 1);
            m_propagatedCapturePointPublisher     = m_node.advertise<visualization_msgs::Marker>("/atlas/propagatedCapturePoint", 1);

            m_stopTrajectoryPublisher = m_node.advertise<std_msgs::Int32>("joint_trajectory_startStop", 1);
            m_supportPolygonPublisher = m_node.advertise<geometry_msgs::PolygonStamped>("/atlas/support_polygon", 1);

        }


        /*
         * Here we set the KDL tree joint states using the Atlas state message
         * This is where the main processing is done                 *
         */
        void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
        {

            // Preempt if capture point outside support polygon
            if( !m_atlasCaptureNode->isStateCaptured(  atlasStateMsg, m_instantaneousCapturePoint ) )
            {
                std_msgs::Int32 trajStartStop;
                trajStartStop.data = StopPublishing;

                m_stopTrajectoryPublisher.publish( trajStartStop );
            }

            /*
             * This function will calculate the propagated Capture Point (CP)
             */
            m_atlasCaptureNode->updateInstantaneousCapturePoint( atlasStateMsg );
            m_atlasCaptureNode->updatePropagatedCapturePoint( atlasStateMsg, 1 );

            // These will publish the capture points and support polygon
            m_instantaneousCapturePointPublisher.publish( m_atlasCaptureNode->getInstantaneousCapturePointMarker() );
            m_propagatedCapturePointPublisher.publish( m_atlasCaptureNode->getPropagatedCapturePointMarker() );
            m_supportPolygonPublisher.publish( m_atlasCaptureNode->getSupportPolygonMarker() );

        }

        void go()
        {
            ROS_INFO( "GO!" );
            ros::spin();
        }

};

int main (int argc, char *argv[])
{

    ros::init( argc, argv, "AtlasCaptureNodeTest" );

    AtlasCaptureNodeTest atlasCapturePointNodeTest;

    atlasCapturePointNodeTest.go();

    return 0;

}
