/*
 * Author: Andrew Somerville
 * date:   2013-04-18
 */

#include <re2uta/AtlasCommander/BdiStepCommander.hpp>
#include <re2/eigen/eigen_util.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <Eigen/Geometry>


using namespace re2uta;
using namespace re2uta::atlascommander;

class BdiStepCommanderTest
{
    public:
        ros::NodeHandle  m_node;
        BdiStepCommander m_stepCommander;
        ros::Subscriber  m_goalSubscriber;

        BdiStepCommanderTest()
        {
            m_goalSubscriber = m_node.subscribe( "/move_base_simple/goal", 1, &BdiStepCommanderTest::handleGoal, this );
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & msg )
        {
            WalkGoal::Ptr walkGoal( new WalkGoal );

            tf::poseMsgToEigen( msg->pose, walkGoal->goalPose );
            walkGoal->goalFrameId = msg->header.frame_id;

            ROS_INFO_STREAM( "Got goal loc   : " << walkGoal->goalPose.translation().transpose() );
            ROS_INFO_STREAM( "Got goal orient: " << walkGoal->goalPose.rotation().eulerAngles(0,1,2).transpose() );

            m_stepCommander.walkTo( walkGoal );
        }

        void go()
        {

            ros::Duration(2).sleep();
            ros::spinOnce();

            Eigen::Affine3d dest = Eigen::Affine3d::Identity();
            dest.translation() = Eigen::Vector3d( 3, 0, 0 );
//            m_stepCommander.walkTo( dest );

            while( ros::ok() )
            {
                ros::spinOnce();
                ros::Duration(0.2).sleep();
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "bdi_step_commander_test" );
    ROS_INFO( "Starting bdi_step_commander_test" );

    BdiStepCommanderTest bdiStepCommanderTest;

    bdiStepCommanderTest.go();

    return 0;
}
