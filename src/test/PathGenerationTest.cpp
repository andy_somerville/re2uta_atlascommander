/*
 * PathGenerationTest.cpp
 *
 *  Created on: Jun 3, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/StepGenerator.hpp>
#include <re2uta/WalkPatternGenerator.hpp>
#include <re2/visutils/marker_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>




Eigen::Affine3d toAffine( const re2uta::Ray & ray0 )
{
    Eigen::Affine3d affine;

    affine.translation() = ray0.origin();
    affine.linear()      = Eigen::Quaterniond().setFromTwoVectors( Eigen::XAxis3d, ray0.direction() ).toRotationMatrix();

    return affine;
}




class PathGenerationTest
{
    public:
        ros::NodeHandle m_node;
        re2uta::WalkPatternGenerator m_walkPatternGenerator;
        Eigen::Affine3d m_dest;

        ros::Publisher lFootTrajPub;
        ros::Publisher rFootTrajPub;
        ros::Publisher lFootPosePub;
        ros::Publisher rFootPosePub;
        ros::Publisher comTrajPub  ;
        ros::Publisher simMarkerPub;

        PathGenerationTest()
        {
            lFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/l_foot", 5, true);
            rFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/r_foot", 5, true);
            lFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/l_foot_pose", 5, true);
            rFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/r_foot_pose", 5, true);
            comTrajPub   = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/com",    5, true);
            simMarkerPub = m_node.advertise<visualization_msgs::Marker>("/walkTrajectory/sim",    5, true);
        }


        void go()
        {
            ros::Subscriber goalSub;
            goalSub = m_node.subscribe( "/move_base_simple/goal", 1, &PathGenerationTest::handleGoal, this );
            while( ros::ok() )
            {
                ros::spinOnce();
            }
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & poseMsg )
        {
            tf::poseMsgToEigen( poseMsg->pose, m_dest );

            runTest();
        }

        void runTest()
        {
            double destDist = 2;
            int index = 0;

            m_node.getParam( "dist" , destDist );



            nav_msgs::Path lFootTraj;
            nav_msgs::Path rFootTraj;
            nav_msgs::Path comTraj;

            lFootTraj.header.frame_id = "/world";
            rFootTraj.header.frame_id = "/world";
            comTraj.header.frame_id   = "/world";

            geometry_msgs::PoseStamped poseStamped;
            poseStamped.header.frame_id = "/world";





            Eigen::Affine3d lCurrentPose( Eigen::Translation3d( 0, -0.15, 0 ) );
            Eigen::Affine3d rCurrentPose( Eigen::Translation3d( 0,  0.15, 0 ) );

            tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
            lFootTraj.poses.push_back( poseStamped );

            tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
            rFootTraj.poses.push_back( poseStamped );

            double halfStepSize    = 0.2;
            double halfStanceWidth = 0.15;
            re2uta::CirclePathStepGenerator generator( toAffine( re2uta::Ray( Eigen::Vector3d( 0,0,0), Eigen::Vector3d( 1, 0,0) ) ),
                                                        m_dest, halfStepSize, halfStanceWidth );
            for( int i = 0 ; (!generator.hasArrived(rCurrentPose) || !generator.hasArrived(lCurrentPose)) && ros::ok(); ++i )
            {
                if( i % 2 )
                {
                    lCurrentPose = generator.generateNextStep( rCurrentPose, lCurrentPose, i );
                    tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
                    lFootTraj.poses.push_back( poseStamped );

                }
                else
                {
                    rCurrentPose = generator.generateNextStep( lCurrentPose, rCurrentPose, i );
                    tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
                    rFootTraj.poses.push_back( poseStamped );

                }
            }


            lFootTrajPub.publish( lFootTraj );
            rFootTrajPub.publish( rFootTraj );


            using re2::visualization_msgs_util::createMarkerMsg;
            visualization_msgs::Marker::Ptr simMarker;
            Eigen::Vector4d color( 1,1,0,1 );
            simMarker = createMarkerMsg( 1,
                                         visualization_msgs::Marker::LINE_STRIP,
                                         color,
                                         "test",
                                         "/world" );



        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "PathGenerationTest" );
    ROS_INFO( "Starting PathGenerationTest" );

    PathGenerationTest pathGeneratorTest;

    pathGeneratorTest.go();

    return 0;
}
