/*
 * AtlastStateRateTest.cpp
 *
 *  Created on: May 26, 2013
 *      Author: andrew.somerville
 */


#include <sensor_msgs/JointState.h>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ros/ros.h>

class JointStateRateTest
{
    public:
        ros::NodeHandle m_node;
        boost::posix_time::ptime m_lastStateMsgPTime;
        ros::Time       m_lastStateMsgRosTime;
        ros::Time       m_lastStateMsgStampRosTime;


    public:

        void handleJointStates( const sensor_msgs::JointState::ConstPtr & msg )
        {
            ros::Time now = ros::Time::now();
            boost::posix_time::ptime pnow = boost::posix_time::microsec_clock::local_time();
            ROS_INFO_STREAM( "Stamp diff us:    " << (msg->header.stamp - m_lastStateMsgStampRosTime).toNSec()/1000 );
            ROS_INFO_STREAM( "callback diff us: " << (now               - m_lastStateMsgRosTime).toNSec()/1000      );
            ROS_INFO_STREAM( "callback pdiff us: " << (pnow               - m_lastStateMsgPTime).total_microseconds()      );

            m_lastStateMsgStampRosTime = msg->header.stamp;
            m_lastStateMsgRosTime      = now;
            m_lastStateMsgPTime        = pnow;
        }

        void go()
        {
            ros::Subscriber stateSub = m_node.subscribe( "/atlas/joint_states", 1, &JointStateRateTest::handleJointStates, this );
            ros::Time lastWakeupTime = ros::Time::now();
            ros::Time lastSleepTime  = ros::Time::now();
            boost::posix_time::ptime lastWakeupPtime = boost::posix_time::microsec_clock::local_time();
            boost::posix_time::ptime lastSleepPtime  = boost::posix_time::microsec_clock::local_time();
            boost::posix_time::ptime pnow            = boost::posix_time::microsec_clock::local_time();
            ros::Time now;

            while( ros::ok() )
            {
                ros::spinOnce();
                now  = ros::Time::now();
                pnow = boost::posix_time::microsec_clock::local_time();
                ROS_INFO_STREAM( "Spin Delay us: " << (now - lastWakeupTime).toNSec()/1000 );
                ROS_INFO_STREAM( "Spin pdelay us: " << (pnow - lastWakeupPtime).total_microseconds() );

                lastSleepTime  = ros::Time::now();
                lastSleepPtime = boost::posix_time::microsec_clock::local_time();
                usleep( 100 );
                pnow = boost::posix_time::microsec_clock::local_time();
                now = ros::Time::now();
                ROS_INFO_STREAM( "Sleep delay us: " << (now - lastSleepTime).toNSec()/1000 );
                ROS_INFO_STREAM( "Sleep pdelay us: " << (pnow - lastSleepPtime).total_microseconds() );
                ROS_INFO_STREAM( "" );

                lastWakeupTime  = now;
                lastWakeupPtime = pnow;
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "jointstateRateTest" );
    ROS_INFO( "Starting jointstateRateTest" );

    JointStateRateTest stateRateTest;

    stateRateTest.go();

    return 0;
}



