/*
 * OdomPublisherNode.cpp
 *
 *  Created on: May 20, 2013
 *      Author: andrew.somerville
 */


#include <atlas_msgs/AtlasSimInterfaceState.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>
#include <ros/ros.h>


class OdomPublisherNode
{
    public:
        ros::NodeHandle          m_node;
        ros::Subscriber          m_bdiStateSub;
        tf::TransformBroadcaster m_tfBroadcaster;

    public:
        OdomPublisherNode()
        {
            m_bdiStateSub = m_node.subscribe( "/atlas/atlas_sim_interface_state", 1, &OdomPublisherNode::handleBdiState, this );
        }

        void handleBdiState( const atlas_msgs::AtlasSimInterfaceStateConstPtr & stateMsg )
        {
            enum { LEFT=0, RIGHT=1 };

            Eigen::Affine3d odomToFoot;

            tf::poseMsgToEigen( stateMsg->foot_pos_est.elems[LEFT], odomToFoot );

            tf::Transform footToOdomTf;
            tf::transformEigenToTF( odomToFoot.inverse(), footToOdomTf );
            m_tfBroadcaster.sendTransform( tf::StampedTransform( footToOdomTf,
                                                                 ros::Time::now(),
                                                                 std::string( "/l_foot" ),
                                                                 std::string( "/odom" ) ));
        }
};



int main( int argCount, char ** argVec )
{
    ros::init( argCount, argVec, "odom_publisher_node", 1 );

    OdomPublisherNode odomPublisherNode;

    ros::spin();

    return 0;
}
