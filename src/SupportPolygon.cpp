/**
 *
 * SupportPolygon.cpp: this class was heavily inspired by hrl_kinematics
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created May 28, 2013
 * @modified May 28, 2013
 *
 */

#include <urdf/model.h>
#include <Eigen/Geometry>
#include <re2uta/SupportPolygon.hpp>
#include <pcl/io/file_io.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <ros/package.h>

namespace re2uta
{


// this is a work around for qhull problems with pcl::ConvexHull
template< typename PointT >
class CvxHull : public pcl::ConvexHull<PointT>
{
    public:
        std::string & flags() { return this->qhull_flags; }
};


void AtlasSupportPolygon::
setSupportMode( FootSupport & supMode )
{
    m_support_mode = supMode;
}


void AtlasSupportPolygon::
setBaseLink( std::string & name )
{
    m_baseLinkName = name;
}


void AtlasSupportPolygon::
setR2Ltransform( Eigen::Affine3d & r2lTrans )
{
    tf::transformEigenToTF( r2lTrans, tf_right_to_left );
}


bool AtlasSupportPolygon::
isPoseStable( const Eigen::Vector3d & vector3 ) const
{
    tf::Point point;

    point[0] = vector3(0);
    point[1] = vector3(1);
    point[2] = vector3(2);

    return pointInConvexHull( point, m_support_polygon_ );
}


geometry_msgs::PolygonStamped AtlasSupportPolygon::
getSupportPolygon() const
{
    geometry_msgs::PolygonStamped footprint_poly;
    footprint_poly.header.frame_id = m_baseLinkName;
    footprint_poly.header.stamp    = ros::Time::now();
    for (unsigned i=0; i < m_support_polygon_.size(); ++i)
    {
        geometry_msgs::Point32 p;
        tf::Point tfP = tf_to_support_ * m_support_polygon_[i];
        p.x = tfP.x();
        p.y = tfP.y();
        p.z = tfP.z();
        footprint_poly.polygon.points.push_back(p);
    }

    return footprint_poly;
}


void AtlasSupportPolygon::
setSupportPolygon()
{
    // This is because we assume the base frame is the frame of foot support
    if (m_support_mode == SUPPORT_SINGLE_LEFT)
    {
        m_support_polygon_ = m_lFootSupPoly_;
        m_supportCentroid  = m_lCentroid;
    }
    else // RIGHT or DOUBLE
    {
        m_support_polygon_ = m_rFootSupPoly_;
        m_supportCentroid  = m_rCentroid;
    }

    // append left if double support:
    if (m_support_mode == SUPPORT_DOUBLE)
    {
        m_supportCentroid  = m_bothCentroid;
        m_support_polygon_ = m_bothSupPoly_;
    }

//    ROS_INFO_STREAM( "Support centroid at source: " << m_supportCentroid );
//    m_support_polygon_ = convexHull(m_support_polygon_);
}


std::vector<tf::Point> AtlasSupportPolygon::
convexHull(const std::vector<tf::Point>& points) const
{
    std::vector<tf::Point> hull;

    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_points(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ> chull_points;
    CvxHull<pcl::PointXYZ> chull;

    if ( points.empty() )
    {
        ROS_ERROR("convexHull on empty set of points!");
        return hull;
    }

    for (unsigned i = 0; i < points.size(); ++i)
    {
        pcl_points->points.push_back(pcl::PointXYZ(points[i].x(), points[i].y(), m_ankleOffset));
    }

#if ROS_VERSION_MINIMUM(1,8,0) // test for Fuerte (newer PCL)
    chull.setDimension(2);
#else

#endif

    chull.setInputCloud( pcl_points );
    std::vector<pcl::Vertices> polygons;

    chull.flags() = chull.flags() + "QJ";
    chull.reconstruct( chull_points, polygons );

    if ( polygons.size() == 0 )
    {
        ROS_ERROR("Convex hull polygons are empty");
        return hull;
    }
    else if ( polygons.size() > 1 )
    {
        ROS_WARN("Convex hull polygons are larger than 1");
    }

    for ( unsigned i = 0; i < polygons[0].vertices.size(); ++i )
    {
        int idx = polygons[0].vertices[i];
        tf::Point p(chull_points.points[idx].x,
                    chull_points.points[idx].y,
                    chull_points.points[idx].z);
        hull.push_back(p);
    }

    return hull;
}


std::string parseUri( std::string const & uri )
{
    std::string fullname = uri;
    if (fullname.find("package://") == 0)
    {
        fullname.erase(0, strlen("package://"));
        size_t pos = fullname.find("/");
        if (pos == std::string::npos)
        {
            ROS_FATAL("Could not parse package:// format for [%s]", uri.c_str());
        }

        std::string package = fullname.substr(0, pos);
        fullname.erase(0, pos);
        std::string package_path = ros::package::getPath(package);

        fullname = package_path + fullname;
    }
    else if (fullname.find("file://") == 0)
    {
        fullname.erase(0, strlen("file://"));
    }

    return fullname;
}



template< typename PointT >
typename pcl::PointCloud<PointT>::Ptr
projectOntoPlane( typename pcl::PointCloud<PointT>::Ptr const & inputCloud, Eigen::Vector3d const & planeNormal )
{
    pcl::ModelCoefficients::Ptr coefficients( new pcl::ModelCoefficients() );
    coefficients->values.resize( 4, 0 );
    Eigen::Map<Eigen::Vector3f>( &coefficients->values[0] ) = planeNormal.cast<float>();// only first 3 values intentionally

    // Create the filtering object
    pcl::ProjectInliers<pcl::PointXYZ> proj;
    proj.setModelType(         pcl::SACMODEL_PLANE  );
    proj.setInputCloud(        inputCloud   );
    proj.setModelCoefficients( coefficients );

    pcl::PointCloud<pcl::PointXYZ>::Ptr outputCloud;
    outputCloud.reset( new pcl::PointCloud<pcl::PointXYZ> );
    proj.filter(      *outputCloud  );

    return outputCloud;
}


void AtlasSupportPolygon::
initFootPolygon()
{
    ros::NodeHandle node;
    std::string modelUrdf;
    node.getParam( "/robot_description", modelUrdf);

    urdf::Model urdfModel;
    urdfModel.initString( modelUrdf );

    boost::shared_ptr<urdf::Link const> link = urdfModel.getLink( "right_foot" ); //atlasLookup->lookupString( "l_foot" ) );

    boost::shared_ptr<urdf::Mesh const> urdfMesh;
    if( link )
        urdfMesh = boost::shared_dynamic_cast<urdf::Mesh>( link->collision->geometry );

    pcl::PointCloud<pcl::PointXYZ>::Ptr footVertexProjection( new pcl::PointCloud<pcl::PointXYZ> );
    if( urdfMesh )
    {
        boost::shared_ptr<urdf::Mesh const> urdfMesh = boost::shared_dynamic_cast<urdf::Mesh>( link->collision->geometry );

        std::string     meshFilename  = parseUri( urdfMesh->filename );
        Eigen::Affine3d linkTransform;

        urdf::Pose linkPose = link->collision->origin;

        linkTransform.translation() = Eigen::Map<Eigen::Vector3d>( &linkPose.position.x ); // dangerous

        Eigen::Quaterniond linkRotation( linkPose.rotation.w, linkPose.rotation.x, linkPose.rotation.y, linkPose.rotation.z );
        linkTransform.linear() = linkRotation.matrix();
        linkTransform.scale( Eigen::Map<Eigen::Vector3d const>( &urdfMesh->scale.x ) );

        pcl::PolygonMesh footVertexMesh;
        pcl::io::loadPolygonFileSTL( meshFilename, footVertexMesh );


        pcl::PointCloud<pcl::PointXYZ>::Ptr preFootVertexCloud(   new pcl::PointCloud<pcl::PointXYZ> );
        pcl::PointCloud<pcl::PointXYZ>::Ptr footVertexCloud(      new pcl::PointCloud<pcl::PointXYZ> );

        pcl::fromROSMsg( footVertexMesh.cloud, *preFootVertexCloud );

        pcl::transformPointCloud( *preFootVertexCloud, *footVertexCloud, linkTransform.cast<float>() );

        footVertexProjection = projectOntoPlane<pcl::PointXYZ>( footVertexCloud, Eigen::Vector3d( 0, 0, 1 ) );
    }
    else
    {
        footVertexProjection->push_back( pcl::PointXYZ( 0.184f,  0.0624435f, m_ankleOffset) );
        footVertexProjection->push_back( pcl::PointXYZ( 0.184f, -0.0624435f, m_ankleOffset) );
        footVertexProjection->push_back( pcl::PointXYZ(-0.076f, -0.0624435f, m_ankleOffset) );
        footVertexProjection->push_back( pcl::PointXYZ(-0.076f,  0.0624435f, m_ankleOffset) );
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_points(new pcl::PointCloud<pcl::PointXYZ>);
    CvxHull<pcl::PointXYZ> chull;
    //chull.setDimension(2);

    chull.setInputCloud( footVertexProjection );
    std::vector<pcl::Vertices> polygons;

    pcl::PointCloud<pcl::PointXYZ>::Ptr convexHullFootVertices( new pcl::PointCloud<pcl::PointXYZ> );
    chull.flags() = chull.flags() + "QJ QbB";
    chull.reconstruct( *convexHullFootVertices, polygons );

    pcl::compute3DCentroid( *convexHullFootVertices, m_rCentroid );
    m_lCentroid     = m_rCentroid;
    m_lCentroid.y() = -m_lCentroid.y(); //FIXME warning this assumes that it should be flipped about 0
    ROS_INFO_STREAM( "Computed rCentroid to: " << m_rCentroid.transpose() );
    ROS_INFO_STREAM( "Computed lCentroid to: " << m_lCentroid.transpose() );

    m_rFootSupPoly_.resize( 0 );
    BOOST_FOREACH( pcl::PointXYZ const & point, convexHullFootVertices->points )
    {
        m_rFootSupPoly_.push_back( tf::Point( point.x, point.y, point.z ) );
    }
//    }
//    else
//    {
//        // These were set by hand by looking at the atlas URDF
//        m_rFootSupPoly_.push_back( tf::Point(  0.184f,  0.0624435f, m_ankleOffset ) );
//        m_rFootSupPoly_.push_back( tf::Point(  0.184f, -0.0624435f, m_ankleOffset ) );
//        m_rFootSupPoly_.push_back( tf::Point( -0.076f, -0.0624435f, m_ankleOffset ) );
//        m_rFootSupPoly_.push_back( tf::Point( -0.076f,  0.0624435f, m_ankleOffset ) );
//    }

    // mirror for left:
    m_lFootSupPoly_ = m_rFootSupPoly_;
    for (unsigned i=0; i < m_lFootSupPoly_.size(); ++i)
    {
        m_lFootSupPoly_[i] *= tf::Point(1.0, -1.0, 1.0); //FIXME warning this assumes that it should be flipped about 0
    }
    // restore order of polygon
//    m_lFootSupPoly_ = convexHull( m_lFootSupPoly_ );

    m_bothSupPoly_ = m_rFootSupPoly_; //FIXME this is a bad stop gap measure which assumes we're using right foot in double support
    m_bothCentroid = m_rCentroid;
//    for (unsigned i=0; i < m_lFootSupPoly_.size(); ++i)
//    {
//        m_bothSupPoly_.push_back( m_lFootSupPoly_[i] );
//    }
//    m_lFootSupPoly_ = convexHull( m_bothSupPoly_ );
}


bool AtlasSupportPolygon::
pointInConvexHull(const tf::Point& point, const std::vector<tf::Point>& polygon) const
{
    assert( polygon.size() >=3 );
    int positive_direction = 0;
    for ( unsigned i = 0; i < polygon.size(); ++i )
    {
        int i2 = (i+1)% (polygon.size());
        double dx = polygon[i2].getX() - polygon[i].getX();
        double dy = polygon[i2].getY() - polygon[i].getY();
        if (dx == 0.0 && dy == 0.0)
        {
//           ROS_DEBUG("Skipping polygon connection [%d-%d] (identical points)", i, i2);
            continue;
        }
        double line_test = (point.y() - polygon[i].getY())*dx - (point.x() - polygon[i].getX())*dy;
        if (i == 0)
            positive_direction = (line_test > 0.0);
//            ROS_DEBUG("Line test [%d-%d] from (%f,%f) to (%f,%f): %f", i, i2, polygon[i].getX(), polygon[i].getY(),
//                       polygon[i2].getX(), polygon[i2].getY(), line_test);

        if ( ( line_test > 0.0 ) != positive_direction )
            return false;
    }

    return true;
}


/*
 * This will output the center of the support polygon
 */
Eigen::Vector3d AtlasSupportPolygon::
getSupportPolygonCenter() const
{
    return m_supportCentroid.head<3>().cast<double>();
}


/*
 * The following publishes a visualization marker for the centroid of the support polygon
 */
visualization_msgs::Marker AtlasSupportPolygon::
getSupportPolygonCenterMarker() const
{
    visualization_msgs::Marker centroid_marker;

    Eigen::Vector3d supCenter = Eigen::Vector3d::Zero();
    supCenter = getSupportPolygonCenter();

    centroid_marker.header.stamp = ros::Time::now();
    centroid_marker.header.frame_id = m_baseLinkName;
    centroid_marker.type = visualization_msgs::Marker::SPHERE;
    centroid_marker.pose.position.x = supCenter( 0 );
    centroid_marker.pose.position.y = supCenter( 1 );
    centroid_marker.pose.position.z = supCenter( 2 );
    //centroid_marker.pose.orientation.
        //tf::quaternionTFToMsg(tf_to_support_.getRotation(), com_marker.pose.orientation);
    centroid_marker.scale.x = 0.03;
    centroid_marker.scale.y = 0.03;
    centroid_marker.scale.z = 0.005;
    centroid_marker.color.a = 1.0;
    centroid_marker.color.g = 1.0;
    centroid_marker.color.r = 0.0;
    centroid_marker.color.b = 0.0;

    return centroid_marker;
}

}

