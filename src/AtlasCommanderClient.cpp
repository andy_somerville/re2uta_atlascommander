/*
 * AtlasCommanderClient.cpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#include "ros_utils.hpp"

#include <re2uta/AtlasCommanderClient.hpp>
#include <re2uta_atlasCommander/FrameCommandStamped.h>
#include <re2/eigen/eigen_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <std_msgs/Int8.h>


namespace re2uta
{

AtlasCommanderClient::
AtlasCommanderClient()
{
    m_walktoPub           = m_node.advertise<geometry_msgs::Vector3Stamped>(              "walkto",            1 );
    m_selectControllerPub = m_node.advertise<std_msgs::Int8>(                             "select_controller", 1 );
    m_commandFramePub     = m_node.advertise<re2uta_atlasCommander::FrameCommandStamped>( "command_frame",     1 );
}

void
AtlasCommanderClient::
walkto( const Eigen::Vector3d & dest )
{
    m_walktoPub.publish( buildVec3Stamped("world",dest) ); //fixme this shouldn't be world
}

void
AtlasCommanderClient::
switchToController( atlascommander::ControllerType controllerType )
{
    m_selectControllerPub.publish( buildInt8( (int)controllerType ) );
}

void
AtlasCommanderClient::
commandFramePose( const std::string &     planFrameName,
                  const std::string &     tipFrameName,
                  const Eigen::Affine3d & dest,
                  const Eigen::Vector6d & weights )
{
    re2uta_atlasCommander::FrameCommandStamped msg;

    msg.header.frame_id = planFrameName;
    msg.child_frame_id  = tipFrameName;
    tf::poseEigenToMsg( dest, msg.pose );

    Eigen::Vector3dMap( &msg.weights.linear.x  ) = weights.head(3);
    Eigen::Vector3dMap( &msg.weights.angular.x ) = weights.tail(3);

    m_commandFramePub.publish( msg );
}

}


