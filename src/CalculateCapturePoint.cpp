/**
 *
 * CalculateCapturePoint.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Apr 24, 2013
 * @modified Apr 24, 2013
 *
 */

#include <re2uta/CalculateCapturePoint.hpp>

namespace re2uta
{


AtlasCaptureNode::AtlasCaptureNode( const urdf::Model & urdfModel )
{
    kdlRootInertiaWorkaround( urdfModel, m_tree );

    m_lastCOMPosition = Eigen::Vector3d::Zero();

    m_atlasLookup.reset( new AtlasLookup( urdfModel ) );
    m_treeJointPositions   = Eigen::VectorXd::Zero( m_tree.getNrOfJoints() );

    m_poseSolver.reset( new  FullBodyPoseSolver( urdfModel, m_atlasLookup->lookupString( "l_foot" ),
                                                        m_atlasLookup->lookupString( "r_foot" ),
                                                        m_atlasLookup->lookupString( "utorso" ) ) );

    m_supportPolygon.reset( new re2uta::AtlasSupportPolygon() );

    m_supportState = SUPPORT_DOUBLE;

    m_atlasR2LjointPos     = Eigen::VectorXd::Zero( 12 );
    m_atlasL2RjointPos     = Eigen::VectorXd::Zero( 12 );

    m_atlasJointPositions  = Eigen::VectorXd::Zero( m_tree.getNrOfJoints() );

    m_instantaneousCapturePoint = Eigen::Vector3d::Zero();
    m_propagatedCapturePoint = Eigen::Vector3d::Zero();

    m_lastTime = ros::Time::now();
}

/*
 * This function sets the internal class state that maintains the support state of Atlas
 * TODO add more sophisticated way to calculate this, maybe include torques etc
 */
void AtlasCaptureNode::setSupportState( double lForceZ, double rForceZ )
{
    if( rForceZ < 30 )
        m_supportState = SUPPORT_SINGLE_LEFT;
    else
    if( lForceZ < 30 )
        m_supportState = SUPPORT_SINGLE_RIGHT;
    else
    if( ( lForceZ > 30 ) && ( rForceZ > 30 ) )
        m_supportState = SUPPORT_DOUBLE;
    else
        m_supportState = SUPPORT_SINGLE_RIGHT;
}


/*
 * This function sets the internal class state that maintains the support state of Atlas
 * TODO add more sophisticated way to calculate this, maybe include torques etc
 */
void AtlasCaptureNode::setSupportStateNoDouble( double lForceZ, double rForceZ )
{
    if( rForceZ < 30 )
        m_supportState = SUPPORT_SINGLE_LEFT;
    else if( lForceZ < 30 )
        m_supportState = SUPPORT_SINGLE_RIGHT;
//    else if( ( lForceZ > 30 ) && ( rForceZ > 30 ) )
//        m_supportState = SUPPORT_DOUBLE;
    else
        m_supportState = SUPPORT_SINGLE_RIGHT;
}


/*
 * This will perform the forward kinematics from root to tip of a chain using KDL solvers
 * TODO change this to Tree FK
 */
KDL::Frame AtlasCaptureNode::ChainFK( KDL::Tree & in_tree, std::string root, std::string tip, Eigen::VectorXd & atlasJointPos )
{
    KDL::Chain leg_chain;
    in_tree.getChain( root, tip, leg_chain );

    unsigned int jointNum = leg_chain.getNrOfJoints();
    KDL::JntArray jointpositions = KDL::JntArray( jointNum );

    for(unsigned int i = 0; i < jointNum; i++)
    {
        jointpositions( i ) = atlasJointPos( i );
    }

    // Create solver based on kinematic chain
    KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive( leg_chain );

    // Create the frame that will contain the results
    KDL::Frame cartPos;

    // Calculate forward position kinematics
    bool kinematics_status;
    kinematics_status = fksolver.JntToCart( jointpositions, cartPos );

    return cartPos;
}


void AtlasCaptureNode::updateMemberDataInternal( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
{
    m_sampleTime = ros::Time::now();

    int msgJointIndex = 0;
    BOOST_FOREACH( const double & jointPos, atlasStateMsg->position )
    {
        int qnr = m_atlasLookup->jointIndexToQnr( msgJointIndex );
        if( qnr >= 0 && qnr < m_treeJointPositions.rows() )
            m_treeJointPositions( qnr ) = atlasStateMsg->position[msgJointIndex];
        m_atlasJointPositions( msgJointIndex ) = atlasStateMsg->position[msgJointIndex];
        ++ msgJointIndex;
    }

    if( m_supportState == SUPPORT_SINGLE_LEFT )
    {
        m_baseName            = m_atlasLookup->lookupString( "l_foot" );
        m_swingName           = m_atlasLookup->lookupString( "r_foot" );

    }
    else
    {
        m_baseName            = m_atlasLookup->lookupString( "r_foot" );
        m_swingName           = m_atlasLookup->lookupString( "l_foot" );
    }

    // Set Support Polygon stuff
    m_supportPolygon->setSupportMode( m_supportState );
    m_supportPolygon->setBaseLink( m_baseName );

    m_R2Ltransform = m_poseSolver->getTransformFromTo( m_atlasLookup->lookupString("l_foot"), m_atlasLookup->lookupString("r_foot"), m_treeJointPositions );

    m_supportPolygon->setR2Ltransform( m_R2Ltransform );

    m_supportPolygon->setSupportPolygon();

    ////////

    // We only care about the first 3 elements x, y, z given by calcPointMass
    updateCOM( m_baseName, m_treeJointPositions );


    // the time step to calculate COM velocity
    ros::Duration sampleTimeDiff  = m_sampleTime - m_lastTime;
    m_lastTime                    = m_sampleTime;

    // This will give the COM velocity
    // FIXME get COM velocity using COM Jacobian??
    m_comVelocity = ( m_comPosition - m_lastCOMPosition )/sampleTimeDiff.toSec();

    // Setting last COM position
    m_lastCOMPosition = m_comPosition;
}

void AtlasCaptureNode::updateMemberDataNoDouble( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
{
    // SET support state
    setSupportStateNoDouble( atlasStateMsg->l_foot.force.z, atlasStateMsg->r_foot.force.z );
    updateMemberDataInternal( atlasStateMsg );
}

void AtlasCaptureNode::updateMemberData( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
{
    // SET support state
    setSupportState( atlasStateMsg->l_foot.force.z, atlasStateMsg->r_foot.force.z );
    updateMemberDataInternal( atlasStateMsg );
}

Eigen::Vector3d AtlasCaptureNode::calculateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
{
    // This gives the capture point
    return re2uta::calcInstantaneousCapturePoint( m_comPosition, m_comVelocity, /*m_comPosition(2)*/ 0.9 );
}


void AtlasCaptureNode::updateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
{
    m_instantaneousCapturePoint = calculateInstantaneousCapturePoint( atlasStateMsg );
}


/*
 * This function will calculate the propagated Capture Point (CP)
 */
Eigen::Vector3d AtlasCaptureNode::calculatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT )
{
  // This will call the private member function to calculate the Instantaneous Capture Point (ICP)
  Eigen::Vector3d instantaneousCapturePoint = calculateInstantaneousCapturePoint( atlasStateMsg );

  // This will be the location of the center of the support
  Eigen::Vector3d rAnkle;

  // Here the propagated CP is generated
  // This is because the velocity of the capture point is equal to the distance between the ICP and the ankle
  if( m_supportState == SUPPORT_DOUBLE )
  {
    // In double support the world frame is the right ankle but the center of support is the midpoint of the line connecting the left and right ankles
    rAnkle(0) = m_R2Ltransform.translation().x()/2;
    rAnkle(1) = m_R2Ltransform.translation().y()/2;
    rAnkle(2) = m_R2Ltransform.translation().z();
  }
  else{
    // In our setup the ankle is the world frame so the ankle is at 0, 0, 0
    rAnkle = Eigen::Vector3d::Zero();
  }

  /*
   * This formula is from page 9 of
   * Capturability-Based Analysis and Control of Legged Locomotion. Part 1: Theory and Application to Three Simple Gait Models
   * by Twan Koolen, Tomas De Boer, John Rebula, Ambarish Goswami, and Jerry E. Pratt
   */
  return ( instantaneousCapturePoint - rAnkle )*exp( delT ) + rAnkle;
}


/*
 * This function will calculate the time taken to reach the desired capture point given current COM velocity
 * TODO test this
 */
double AtlasCaptureNode::calculateFutureCapturePointTime( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, Eigen::Vector3d futureCP )
{
  // This will call the private member function to calculate the Instantaneous Capture Point (ICP)
  Eigen::Vector3d instantaneousCapturePoint = calculateInstantaneousCapturePoint( atlasStateMsg );

  // This will be the location of the center of the support
  Eigen::Vector3d rAnkle;

  // Here the propagated CP is generated
  // This is because the velocity of the capture point is equal to the distance between the ICP and the ankle
  if( m_supportState == SUPPORT_DOUBLE )
  {
    // In double support the world frame is the right ankle but the center of support is the midpoint of the line connecting the left and right ankles
    rAnkle(0) = m_R2Ltransform.translation().x()/2;
    rAnkle(1) = m_R2Ltransform.translation().y()/2;
    rAnkle(2) = m_R2Ltransform.translation().z();
  }
  else
  {
    // In our setup the ankle is the world frame so the ankle is at 0, 0, 0
    rAnkle = Eigen::Vector3d::Zero();
  }

  /*
   * This formula is from page 9 of
   * Capturability-Based Analysis and Control of Legged Locomotion. Part 1: Theory and Application to Three Simple Gait Models
   * by Twan Koolen, Tomas De Boer, John Rebula, Ambarish Goswami, and Jerry E. Pratt
   */
  return log( ( futureCP - rAnkle ).norm() / ( instantaneousCapturePoint - rAnkle ).norm() );
}


void AtlasCaptureNode::updatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT )
{
    m_propagatedCapturePoint = calculatePropagatedCapturePoint( atlasStateMsg, delT );
}


bool AtlasCaptureNode::isStateCaptured( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, Eigen::Vector3d & instantaneousCapturePoint )
{
    // This will call the private member function to calculate the Instantaneous Capture Point (ICP)
    // and will save the capture point
    instantaneousCapturePoint = calculateInstantaneousCapturePoint( atlasStateMsg );


    instantaneousCapturePoint = m_instantaneousCapturePoint;

    // This will return the ICP member
    return m_supportPolygon->isPoseStable( instantaneousCapturePoint );
}


Eigen::Vector3d AtlasCaptureNode::getInstantaneousCapturePoint()
{
    return m_instantaneousCapturePoint;
}


Eigen::Vector3d AtlasCaptureNode::getPropagatedCapturePoint()
{
    return m_propagatedCapturePoint;
}

Eigen::Vector3d AtlasCaptureNode::getSwingFootPose()
{
  Eigen::Vector3d otherFoot;

  if ( m_baseName == m_atlasLookup->lookupString( "r_foot" ) )
  {
    otherFoot(0) = m_R2Ltransform.translation().x();
    otherFoot(1) = m_R2Ltransform.translation().y();
    otherFoot(2) = m_R2Ltransform.translation().z();
  }else
  {
    Eigen::Affine3d L2Rtransform = m_R2Ltransform.inverse();
    otherFoot(0) = L2Rtransform.translation().x();
    otherFoot(1) = L2Rtransform.translation().y();
    otherFoot(2) = L2Rtransform.translation().z();
  }

  return otherFoot;
}


visualization_msgs::Marker AtlasCaptureNode::getInstantaneousCapturePointMarker()
{
    // Build and publish capture point marker
    visualization_msgs::Marker capturePointMarker;
    capturePointMarker.header.stamp = ros::Time::now();
    capturePointMarker.header.frame_id = m_baseName;
    capturePointMarker.pose.position.x = m_instantaneousCapturePoint(0);
    capturePointMarker.pose.position.y = m_instantaneousCapturePoint(1);
    capturePointMarker.pose.position.z = -0.079342f; //// Should move publishing to something else so these kinds of hacks are not necessary
    capturePointMarker.action = visualization_msgs::Marker::ADD;
    capturePointMarker.type = visualization_msgs::Marker::SPHERE;

    capturePointMarker.scale.x = 0.03;
    capturePointMarker.scale.y = 0.03;
    capturePointMarker.scale.z = 0.005;

    capturePointMarker.color.a = 1.0;
    capturePointMarker.color.b = 0.0;

    // This will change the marker color
    // ORANGE   - outside support polygon
    // YELLOW   - inside support polygon
    if( m_supportPolygon->isPoseStable( m_instantaneousCapturePoint ) )
    {
        capturePointMarker.color.g = 1.0;
        capturePointMarker.color.r = 1.0;
    }
    else
    {
        capturePointMarker.color.g = 0.5;
        capturePointMarker.color.r = 1.0;
    }

    return capturePointMarker;
}

Eigen::Vector3d AtlasCaptureNode::getCom()
{
    return m_comPosition;
}


/*
 * In this gravityVector is the direction vector of gravity in the base frame
 * This function will give the vector of gravity in the frame of the given link name
 */
Eigen::Vector3d AtlasCaptureNode::getGravityVectorInLink( geometry_msgs::Quaternion orient, std::string linkName )
{
    double gravity = -9.81;
    Eigen::Vector3d gravityVector = gravity*getGravityNormalInLink( orient, linkName );
    return gravityVector;
}

/*
 * In this gravityNormal is the normal away from the plane that the gravity vector goes into
 * This function will give the vector of gravity in the frame of the given link name
 */
Eigen::Vector3d AtlasCaptureNode::getGravityNormalInLink( geometry_msgs::Quaternion orient, std::string linkName )
{
    Eigen::Quaterniond imuToInertial( orient.w, orient.x, orient.y, orient.z );
    Eigen::Quaterniond interialToImu = imuToInertial.inverse();

    // atlasLookup.cmdToTree( re2::toEigen( stateMsg->positions ).cast<double>()
    Eigen::Affine3d imuToLink = m_poseSolver->getTransformFromTo( "imu_link", linkName, m_treeJointPositions );

    Eigen::Vector3d gravityNormalInInertial( 0, 0, 1 );

    // Rotate the normal to current robot orientation in imu_link frame
    Eigen::Vector3d gravityNormalInImu = interialToImu*gravityNormalInInertial;

    // Rotate the gravity normal from imu_link to base foot
    Eigen::Vector3d gravityNormalInLink = imuToLink.linear() * gravityNormalInImu;

    return gravityNormalInLink;
}


/*
 * In this linkNormal is the normal away from the x, y plane of the link that the gravity vector goes into
 * This function will give the vector of gravity in the frame of the given link name
 */
Eigen::Vector3d AtlasCaptureNode::getLinkNormalInInertial( geometry_msgs::Quaternion orient, std::string linkName )
{
    Eigen::Quaterniond imuToInterial( orient.w, orient.x, orient.y, orient.z );

    // atlasLookup.cmdToTree( re2::toEigen( stateMsg->positions ).cast<double>()
    Eigen::Affine3d linkToImu = m_poseSolver->getTransformFromTo( linkName, "imu_link", m_treeJointPositions );

    Eigen::Vector3d linkNormalInLink( 0, 0, 1 );

    // Rotate the normal to current robot orientation in imu_link frame
    Eigen::Vector3d linkNormalInImu = linkToImu*linkNormalInLink;

    // Rotate the gravity normal from imu_link to base foot
    Eigen::Vector3d linkNormalInInertial = imuToInterial * linkNormalInImu;

    return linkNormalInInertial;
}

/*
 * In this gravityVector is the direction vector of gravity in the base frame
 */
Eigen::Vector3d AtlasCaptureNode::getComProjection( geometry_msgs::Quaternion orient, Eigen::Vector3d  & comInFootFrame )
{

    Eigen::Vector3d gravityNormalInBaseFoot = getGravityNormalInLink( orient, m_baseName );

    Eigen::Hyperplane3d gravityPlane( gravityNormalInBaseFoot, 0 );

//    return gravityPlane.projection( comInFootFrame );
    return comInFootFrame;
}

// FIXME should not use member data for COM
Eigen::Vector3d AtlasCaptureNode::getComProjection( geometry_msgs::Quaternion orient )
{
    return getComProjection( orient, m_comPosition );
}


/*
 * In this gravityVector is the direction vector of gravity in the base frame
 */
Eigen::Vector3d AtlasCaptureNode::getCorrectedSupportPolygonCenterInFootFrame( geometry_msgs::Quaternion orient, Eigen::Vector3d  & supPolyCenter )
{

    Eigen::Vector3d linkNormalInInertial = getLinkNormalInInertial( orient, m_baseName );
    Eigen::Hyperplane3d gravityPlane( linkNormalInInertial, 0 );

    return gravityPlane.projection( supPolyCenter );
}


/*
 * In this gravityVector is the direction vector of gravity in the base frame
 */
Eigen::Vector3d AtlasCaptureNode::getCorrectedSupportPolygonCenterInFootFrame( geometry_msgs::Quaternion orient )
{
    Eigen::Vector3d supCenterInFootFrame = getSupportPolygonCenter();

    // Need to get COM projected Z
    Eigen::Vector3d comProj;
    comProj = getComProjection( orient );

    supCenterInFootFrame( 2 ) = comProj( 2 );

    return getCorrectedSupportPolygonCenterInFootFrame( orient, supCenterInFootFrame );
}

/*
 * This function will calculate the CoP aka ZMP for each foot
 */
Eigen::Vector3d AtlasCaptureNode::calcIndividualCoP( geometry_msgs::Wrench wrench )
{
  return Eigen::Vector3d( -wrench.torque.y/wrench.force.z, wrench.torque.x/wrench.force.z, -0.079342 );
}

/*
 * This will be calculated in whatever frame that posL and posR are given
 * This function will calculate the CoP aka ZMP for each foot
 */
Eigen::Vector3d AtlasCaptureNode::calcCombinedCoP( geometry_msgs::Wrench wrenchL, geometry_msgs::Wrench wrenchR, Eigen::Vector3d posL, Eigen::Vector3d posR)
{
  Eigen::Vector3d CoPL( calcIndividualCoP( wrenchL ) );
  Eigen::Vector3d CoPR( calcIndividualCoP( wrenchR ) );

  double Xc = ( ( posL(0) + CoPL(0) )*wrenchL.force.z + ( posR(0) + CoPR(0) )*wrenchR.force.z ) / (wrenchL.force.z + wrenchR.force.z );
  double Yc = ( ( posL(1) + CoPL(1) )*wrenchL.force.z + ( posR(1) + CoPR(1) )*wrenchR.force.z ) / (wrenchL.force.z + wrenchR.force.z );

  return Eigen::Vector3d( Xc, Yc, 0 );
}



visualization_msgs::Marker AtlasCaptureNode::getPropagatedCapturePointMarker()
{
    // Build and publish capture point marker
    visualization_msgs::Marker capturePointMarker;
    capturePointMarker.header.stamp = ros::Time::now();
    capturePointMarker.header.frame_id = m_baseName;
    capturePointMarker.pose.position.x = m_propagatedCapturePoint(0);
    capturePointMarker.pose.position.y = m_propagatedCapturePoint(1);
    capturePointMarker.pose.position.z = -0.079342f; //// FIXME Should move publishing to something else so these kinds of hacks are not necessary
    capturePointMarker.action = visualization_msgs::Marker::ADD;
    capturePointMarker.type = visualization_msgs::Marker::SPHERE;

    capturePointMarker.scale.x = 0.03;
    capturePointMarker.scale.y = 0.03;
    capturePointMarker.scale.z = 0.005;

    capturePointMarker.color.a = 1.0;
    capturePointMarker.color.b = 1.0;
    capturePointMarker.color.g = 0.0;
    capturePointMarker.color.r = 0.5;

    return capturePointMarker;
}

visualization_msgs::Marker AtlasCaptureNode::getComMarker()
{
    // Build and publish COM marker
    visualization_msgs::Marker comMarker;
    comMarker.header.stamp = ros::Time::now();
    comMarker.header.frame_id = m_baseName;
    comMarker.pose.position.x = m_comPosition(0);
    comMarker.pose.position.y = m_comPosition(1);
    comMarker.pose.position.z = -0.079342;
    comMarker.action = visualization_msgs::Marker::ADD;
    comMarker.type = visualization_msgs::Marker::SPHERE;

    comMarker.scale.x = 0.03;
    comMarker.scale.y = 0.03;
    comMarker.scale.z = 0.005;

    comMarker.color.a = 1.0;
    comMarker.color.b = 0.0;
    comMarker.color.g = 1.0;
    comMarker.color.r = 0.0;

    return comMarker;
}


void AtlasCaptureNode::updateCoP( geometry_msgs::Wrench wrenchL, geometry_msgs::Wrench wrenchR, Eigen::Vector3d posL, Eigen::Vector3d posR )
{
    m_copPosition = calcCombinedCoP( wrenchL, wrenchR, posL, posR);
}

void AtlasCaptureNode::updateCOM( std::string baseName,  Eigen::VectorXd & treeJointPositions )
{
    m_comPosition = calcPointMass( m_tree, m_tree.getSegment( baseName )->second, treeJointPositions ).head(3);
}

Eigen::Vector3d AtlasCaptureNode::getCoP()
{
    return m_copPosition;
}

visualization_msgs::Marker AtlasCaptureNode::getCoPMarker()
{
    // Build and publish COM marker
    visualization_msgs::Marker comMarker;

    comMarker.header.stamp = ros::Time::now();
    comMarker.header.frame_id = m_baseName;
    comMarker.pose.position.x = m_copPosition(0);
    comMarker.pose.position.y = m_copPosition(1);
    comMarker.pose.position.z = -0.079342;
    comMarker.action = visualization_msgs::Marker::ADD;
    comMarker.type = visualization_msgs::Marker::SPHERE;

    comMarker.scale.x = 0.03;
    comMarker.scale.y = 0.03;
    comMarker.scale.z = 0.005;

    comMarker.color.a = 1.0;
    comMarker.color.b = 0.0;
    comMarker.color.g = 0.0;
    comMarker.color.r = 1.0;

    return comMarker;
}


visualization_msgs::Marker AtlasCaptureNode::getComProjectedMarker( geometry_msgs::Quaternion orientation )
{
    // Build and publish COM marker
    visualization_msgs::Marker comMarker;
    Eigen::Vector3d projectedCom = getComProjection( orientation );

    comMarker.header.stamp = ros::Time::now();
    comMarker.header.frame_id = m_baseName;
    comMarker.pose.position.x = projectedCom(0);
    comMarker.pose.position.y = projectedCom(1);
    comMarker.pose.position.z = -0.079342f;
    comMarker.action = visualization_msgs::Marker::ADD;
    comMarker.type = visualization_msgs::Marker::SPHERE;

    comMarker.scale.x = 0.03;
    comMarker.scale.y = 0.03;
    comMarker.scale.z = 0.005;

    comMarker.color.a = 1.0;
    comMarker.color.b = 1.0;
    comMarker.color.g = 0.0;
    comMarker.color.r = 0.0;

    return comMarker;
}

visualization_msgs::Marker AtlasCaptureNode::getSupProjectedMarker( geometry_msgs::Quaternion orientation )
{
    // Build and publish COM marker
    visualization_msgs::Marker projSupMarker;
    Eigen::Vector3d projectedSup = getCorrectedSupportPolygonCenterInFootFrame( orientation );

    projSupMarker.header.stamp = ros::Time::now();
    projSupMarker.header.frame_id = m_baseName;
    projSupMarker.pose.position.x = projectedSup(0);
    projSupMarker.pose.position.y = projectedSup(1);
    projSupMarker.pose.position.z = -0.079342f;
    projSupMarker.action = visualization_msgs::Marker::ADD;
    projSupMarker.type = visualization_msgs::Marker::SPHERE;

    projSupMarker.scale.x = 0.03;
    projSupMarker.scale.y = 0.03;
    projSupMarker.scale.z = 0.005;

    projSupMarker.color.a = 1.0;
    projSupMarker.color.b = 0.0;
    projSupMarker.color.g = 1.0;
    projSupMarker.color.r = 0.5;

    return projSupMarker;
}

geometry_msgs::PolygonStamped AtlasCaptureNode::getSupportPolygonMarker()
{
    return m_supportPolygon->getSupportPolygon();
}

std::string AtlasCaptureNode::getBaseName()
{
  return m_baseName;
}

std::string AtlasCaptureNode::getSwingName()
{
  return m_swingName;
}

Eigen::Vector3d AtlasCaptureNode::getSupportPolygonCenter() const
{
    return m_supportPolygon->getSupportPolygonCenter();
}

visualization_msgs::Marker AtlasCaptureNode::getSupportPolygonCenterMarker() const
{
    return m_supportPolygon->getSupportPolygonCenterMarker();
}

}
