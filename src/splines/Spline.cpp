/*
 * Spline.cpp
 *
 *  Created on: May 30, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/splines/Spline.hpp>
#include "splines.hpp"

namespace re2uta
{

Spline::
Spline( const Eigen::VectorXd & keyPoints,
        const Eigen::VectorXd & keyTimes,
        const EndPointCondition & startCondition,
        const EndPointCondition & endCondition )
{
    m_startCondition       = startCondition;
    m_endCondition         = endCondition;
    m_keySpaceValues       = keyPoints;
    m_keyTimeValues        = keyTimes;
    m_keySpaceValuesDerivT = calcKeypointDerivT( m_keyTimeValues, m_keySpaceValues, m_startCondition, m_endCondition );

}

Eigen::Vector3d
Spline::
eval( double time )
{
    Eigen::Vector3d posVelAccel;
    posVelAccel(0) = spline_cubic_val( m_keyTimeValues.size(),
                                       m_keyTimeValues.data(),
                                       m_keySpaceValues.data(),
                                       m_keySpaceValuesDerivT.data(),
                                       time,
                                       &posVelAccel(1),
                                       &posVelAccel(2) );

    return posVelAccel;
}



Eigen::VectorXd
Spline::
calcKeypointDerivT( const Eigen::VectorXd &   keyTimeValues,
                    const Eigen::VectorXd &   keySpaceValues,
                    const EndPointCondition & beginCondition,
                    const EndPointCondition & endCondition )
{
    double * uglyDoubles = NULL; // AJS: I'm absolutely horrified that they would return dynamically allocated data
    uglyDoubles = spline_cubic_set( keyTimeValues.size(),
                                    keyTimeValues.data(),
                                    keySpaceValues.data(),
                                    (int)beginCondition.first, beginCondition.second,
                                    (int)endCondition.first,   endCondition.second   );

    Eigen::VectorXd result = Eigen::Map<Eigen::VectorXd>( uglyDoubles, keyTimeValues.size() );

    if( uglyDoubles != NULL )
        delete [] uglyDoubles;

    return result;
}


}
