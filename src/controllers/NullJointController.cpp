/*
 * NullJointController.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/NullJointController.hpp>
#include <ros/ros.h>



namespace re2uta
{
namespace atlascommander
{


NullJointController::
NullJointController( boost::condition_variable & syncVar )
    : JointController( syncVar )
{
    ROS_INFO( "Starting NULL joint Controller" );
}

NullJointController::
~NullJointController()
{}

void
NullJointController::
setKEfforts( const std::vector<uint8_t> & kefforts )
{
}


void
NullJointController::
setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime )
{
    ROS_WARN_STREAM_THROTTLE( 20, "Warning: You're using the a joint controller that doesn't actually publish commands" );
}



}
}



