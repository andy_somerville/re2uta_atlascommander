/*
 * SimpleWalkCommander.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#include "../utils.hpp"
#include <re2uta/AtlasCommander/SimpleWalkCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <string>





namespace re2uta
{
namespace atlascommander
{



StepPose::Ptr generateContinuation( const StepPose::Ptr & stepPose, ros::Duration dt )
{
    StepPose::Ptr newStepPose( new StepPose(*stepPose) );

    double velocity = 0.3;
    newStepPose->swingFootPose() = Eigen::Translation3d( 0, 0, -velocity * dt.toSec() ) * stepPose->swingFootPose();
    newStepPose->comPose         = Eigen::Translation3d( 0, 0, -velocity * dt.toSec() ) * stepPose->comPose;

    return newStepPose;
}


SimpleWalkCommander::
SimpleWalkCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                      const urdf::Model & model,
                      const ros::Duration & dt )
{
    m_fullBodyController = fullBodyController;
    m_atlasLookup.reset( new AtlasLookup(        model ) );
    m_solver.reset( new FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                   m_atlasLookup->lookupString( "r_foot" ),
                                                   m_atlasLookup->lookupString( "utorso" ) ) );
    m_halfStepSize    = 0.2;
    m_dt              = dt;
    m_shutdown        = false;

    // Fg = 9.81 * 90 = 883N
    // Fg*distToEdge = Fd*stepLength
    // 883N * 0.13 = Fd*0.5
    // Fd = 182N
    m_forceThresh  = 50; // to be conservative // FIXME we should reduce this even more if possible

    m_controlLoopThread = boost::thread( &SimpleWalkCommander::commandLoop, this );
}

SimpleWalkCommander::
~SimpleWalkCommander()
{
    m_shutdown = true;
//    m_controlLoopThread.join();
}

void
SimpleWalkCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
//    ROS_INFO_STREAM_THROTTLE( 5, "Setting local state" );
    boost::atomic_store( &m_lastStateMsg, latestStateMsg );
}



void
SimpleWalkCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
    //FIXME we're not handling the frame id!
    setDest( walkGoal->goalPose );
}

void
SimpleWalkCommander::
setStepPlannerType( StepPlannerType stepPlannerType )
{
    ROS_ERROR( "SimpleWalkCommander Currently ignoring setStepPlannerType" );
    m_stepPlannerType = stepPlannerType;
}


void
SimpleWalkCommander::
shutdown()
{
    m_shutdown = true;
}





void
SimpleWalkCommander::
setDest( const Eigen::Affine3d & dest )
{
    Eigen::Affine3dPtr destPtr( new Eigen::Affine3d );
    *destPtr = dest;

    boost::atomic_store( &m_walkDestCmd, destPtr );
}




void
SimpleWalkCommander::
publishOdomToRoot( const Eigen::Affine3d & baseFootInOdom,
                  const std::string &      baseFootFrameId,
                  const Eigen::VectorXd &  jointPositions )
{
//    Eigen::Affine3d baseFootToTreeRoot;
//    baseFootToTreeRoot = m_solver->getTransformFromTo( baseFootFrameId, "root", jointPositions );
//
//    Eigen::Affine3d odomInBaseFoot;
//    odomInBaseFoot = baseFootInOdom.inverse();
//
//    Eigen::Affine3d odomInTreeRootXform;
//    odomInTreeRootXform = baseFootToTreeRoot * odomInBaseFoot;

    tf::Transform odomToTreeRoot;
    tf::transformEigenToTF( baseFootInOdom.inverse(), odomToTreeRoot );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( odomToTreeRoot,
                                                         ros::Time::now(),
                                                         baseFootFrameId,
                                                         "odom"  ));
}


StepPose::Ptr
SimpleWalkCommander::
moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose )
{
    StepPose::Ptr newStepPose( new StepPose( *currentStepPose ) );

    Eigen::Affine3d odomInBaseFoot;
    odomInBaseFoot.translation() = (currentStepPose->baseFootPose().translation() + currentStepPose->swingFootPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentStepPose->baseFootPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentStepPose->swingFootPose().rotation() );

    odomInBaseFoot.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    newStepPose->baseFootPose()  = odomInBaseFoot.inverse() * currentStepPose->baseFootPose() ;
    newStepPose->swingFootPose() = odomInBaseFoot.inverse() * currentStepPose->swingFootPose();
    newStepPose->comPose         = odomInBaseFoot.inverse() * currentStepPose->comPose        ;

    return newStepPose;
}



StepPose::Ptr
SimpleWalkCommander::
getCurrentStepPoseInBaseFoot( Foot baseFoot )
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    StepPose::Ptr currentStepPose;

    lastStateMsg = boost::atomic_load( &m_lastStateMsg );
    if( !lastStateMsg )
        return currentStepPose;

    currentStepPose.reset( new StepPose( baseFoot ) );

    std::string baseFootFrameId  = footToFrameId(  baseFoot, m_atlasLookup );
    std::string swingFootFrameId = footToFrameId( otherFoot(baseFoot), m_atlasLookup );

    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

    //FIXME need IMU rotation correction
    currentStepPose->baseFootPose()  = Eigen::Affine3d::Identity();
    currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
    currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );
    currentStepPose = moveStepPoseToNewOdom( currentStepPose );
}




class StepTrajPublisher
{
    public:
        ros::NodeHandle m_node;
        ros::Publisher lFootTrajPub;
        ros::Publisher rFootTrajPub;
        ros::Publisher comTrajPub  ;

        nav_msgs::Path lFootTraj;
        nav_msgs::Path rFootTraj;
        nav_msgs::Path comTraj;

        StepTrajPublisher()
        {
            lFootTrajPub = m_node.advertise<nav_msgs::Path>( "/walkTrajectory/l_foot", 5, true );
            rFootTrajPub = m_node.advertise<nav_msgs::Path>( "/walkTrajectory/r_foot", 5, true );
            comTrajPub   = m_node.advertise<nav_msgs::Path>( "/walkTrajectory/com",    5, true );

            lFootTraj.header.frame_id = "/odom";
            rFootTraj.header.frame_id = "/odom";
            comTraj.header.frame_id   = "/odom";
        }
};




void
SimpleWalkCommander::
commandLoop()
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Foot            baseFoot = LEFT_FOOT;

    StepTrajPublisher         stepTrajPublisher;
    re2::VisualDebugPublisher vizDebug( "full_body_controler_viz_debug", "odom" );

    while( !(lastStateMsg = boost::atomic_load( &m_lastStateMsg ))  && ros::ok() && !m_shutdown )
    {
        ROS_WARN_STREAM_THROTTLE( 2, "No atlas state available, waiting to start SimpleWalkCommander command loop until seen" );
        m_dt.sleep();
    }

    StepPose::Ptr currentStepPose;
    currentStepPose = getCurrentStepPoseInBaseFoot( baseFoot );

    while( ros::ok() && !m_shutdown )
    {
        Eigen::Affine3d walkSource;
        walkSource = Eigen::Affine3d::Identity(); // start at 0,0,0 for now

        Eigen::Affine3dPtr walkDest;
        walkDest      = boost::atomic_load( &m_walkDestCmd ); //, walkDest ); // represents transform between start and dest between feet
        lastStateMsg  = boost::atomic_load( &m_lastStateMsg );

//        swingFootFrameId = footToFrameId( swingFoot );
//        baseFootFrameId  = footToFrameId( otherFoot(swingFoot) );
//        jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
//
//        publishOdomToRoot( currentStepPose->baseFootPose(), baseFootFrameId, jointPositions );

        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }
        ROS_INFO( "SimpleWalkCommander got new destination" );

        currentStepPose = getCurrentStepPoseInBaseFoot( baseFoot );
        currentStepPose = moveStepPoseToNewOdom( currentStepPose );

//        publishOdomToRoot( currentStepPose->baseFootPose(), baseFootFrameId, jointPositions );

        StepTrajectoryGenerator generator( currentStepPose, *walkDest, m_halfStepSize, currentStepPose->baseFootPose().translation().norm() );
        StepPlan::Ptr           stepPlan;

        Eigen::Affine3dPtr latestDest = walkDest;
        while( latestDest == walkDest && !m_shutdown && ros::ok() ) // loop for full plan
        {
            //fixme this function should take the current base foot as an argument
            stepPlan = generator.generateNextStep( currentStepPose );// generator.generateNextStep( currentState )
            ros::Time     startTime = ros::Time::now();
            ros::Duration relativeTime( 0 );

            if( !stepPlan )
            {
                // we're done
                // if the desired walk dest hasn't changed then wipe it out so it doesn't run again when we break
                boost::atomic_compare_exchange( &m_walkDestCmd, &walkDest, Eigen::Affine3dPtr() );
                break;
            }

            Eigen::Vector3d     goalNormal = Eigen::ZAxis3d;
            Eigen::Hyperplane3d goalPlane( goalNormal, stepPlan->last()->swingFootPose().translation() );


            lFootTraj.poses.clear();
            rFootTraj.poses.clear();
            comTraj.  poses.clear();

            lFootTrajPub.publish( lFootTraj );
            rFootTrajPub.publish( rFootTraj );
            comTrajPub.  publish(   comTraj );

            swingFootFrameId = footToFrameId( stepPlan->swingFoot(), m_atlasLookup );
            baseFootFrameId  = footToFrameId( stepPlan->baseFoot(),  m_atlasLookup );

            m_fullBodyController->switchBaseFoot( baseFootFrameId );


            ROS_INFO_STREAM( "High force:   " << highFootForce( lastStateMsg, stepPlan->swingFoot(), m_dt )  );
            ROS_INFO_STREAM( "completion:   " << stepPlan->completion( relativeTime ) );
            ROS_INFO_STREAM( "relativeTime: " << relativeTime );
            ROS_INFO_STREAM( "SIGNED DIST:  " << goalPlane.signedDistance( currentStepPose->swingFootPose().translation() ) );

            bool   underactuated   = false;
            int    timeStep        = 0;
            double defaultVelocity = 0.2;

            while(    // !highFootForce( lastStateMsg, stepPlan->swingFoot(), m_dt ) // FIXME not using actual dt here, but desired dt
//                    (   stepPlan->completion( relativeTime ) < 0.5
//                     || currentStepPose->swingFootPose().translation().z() > (m_dt.toSec() * defaultVelocity) )
//                     &&
                     stepPlan->completion( relativeTime ) < 1.5 )
//            while( stepPlan->completion( relativeTime ) < 1 )
                   //&& !falling  )
            {
                relativeTime = ros::Time::now() - startTime;
                ROS_INFO_STREAM( "Inner Loop     " << stepPlan->completion( relativeTime ) );
                ROS_INFO_STREAM( "Goalplane dist " << goalPlane.signedDistance( currentStepPose->swingFootPose().translation() ) );
                ROS_INFO_STREAM( "zval           " << currentStepPose->swingFootPose().translation().z() );
                m_dt.sleep();
                lastStateMsg   = boost::atomic_load( &m_lastStateMsg );
                jointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );


                if(    !underactuated
                    && stepPlan->completion( relativeTime ) > 0.5 )
                {
                    // Underactuate foot goal in roll and pitch
//                    m_fullBodyController->setFrameGoalGains( buildActuationGoal( stepPlan->swingFoot(), 0.5 ) ); // Move gainst into Frame goal
                    underactuated = true;
                }


//                //FIXME this does not respect the fact that the base foot z axis does not represent true z axis (aka -g direction)
//                StepPose::Ptr currentStepPose( new StepPose );
                *currentStepPose = *stepPlan->last();

                currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
                currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );



                StepPose::Ptr nextStepPoseCommand = stepPlan->last();

//                if( goalPlane.signedDistance( currentStepPose->swingFootPose().translation() ) < (m_dt.toSec() * defaultVelocity) )
//                {
//                    nextStepPoseCommand = generateContinuation( currentStepPose, m_dt );
//                }
//                else
                {
                    nextStepPoseCommand = stepPlan->getAtTime( relativeTime );
                }

                publishOdomToRoot( nextStepPoseCommand->baseFootPose(), baseFootFrameId, jointPositions );

                Eigen::Affine3d footGoal    = nextStepPoseCommand->baseFootPose().inverse() * nextStepPoseCommand->swingFootPose();
                Eigen::Vector6d footWeights = Eigen::Vector6d::Ones();
                m_fullBodyController->setOppositeFootGoal( footGoal, footWeights );
                ROS_INFO_STREAM( "FootGoal : " << footGoal.translation().transpose() );
                ROS_INFO_STREAM( "Twist    : " << affineToTwist( footGoal ) );
                ROS_INFO_STREAM( "weights  : " << footWeights.transpose() );

                Eigen::Vector3d comWeights( 1, 1, 0 );
                m_fullBodyController->setCenterOfMassGoal( nextStepPoseCommand->baseFootPose().inverse() * nextStepPoseCommand->comPose, comWeights  ); //FIXME pick better comz weight

                {
                    geometry_msgs::PoseStamped poseStamped;
//                    poseStamped.header.frame_id = "/odom";

                    tf::poseEigenToMsg( nextStepPoseCommand->lFootPose, poseStamped.pose );
                    lFootTraj.poses.push_back( poseStamped );

                    tf::poseEigenToMsg( nextStepPoseCommand->rFootPose, poseStamped.pose );
                    rFootTraj.poses.push_back( poseStamped );

                    poseStamped.pose.position.x = nextStepPoseCommand->comPose.x();
                    poseStamped.pose.position.y = nextStepPoseCommand->comPose.y();
                    poseStamped.pose.position.z = nextStepPoseCommand->comPose.z();
                    comTraj.poses.push_back( poseStamped );
                }

                double desiredPeriod = 0.1;
                if( timeStep % (int)(desiredPeriod/m_dt.toSec()) == 0 )
                {
                    lFootTrajPub.publish( lFootTraj );
                    rFootTrajPub.publish( rFootTraj );
                    comTrajPub.publish(   comTraj );

                    Eigen::Vector4d color( 1, 1, 0, 1 );
                    int id;
                    id = boost::hash<std::string>()( baseFootFrameId + "preview" );
                    vizDebug.publishPoint3( nextStepPoseCommand->baseFootPose().translation(), color, id + timeStep, "odom", "preview" );

                    id = boost::hash<std::string>()( swingFootFrameId + "preview" );
                    vizDebug.publishPoint3( nextStepPoseCommand->swingFootPose().translation(), color, id + timeStep, "odom", "preview" );

                    id = boost::hash<std::string>()( std::string( "com" ) + "preview" );
                    vizDebug.publishPoint3( nextStepPoseCommand->swingFootPose().translation(), color, id + timeStep, "odom", "preview" );
                }

                ++timeStep;
            }

            // Fully actuate foot goal in roll and pitch
            m_fullBodyController->setFrameGoalGains( );

            swingFoot = otherFoot( swingFoot );

            latestDest = boost::atomic_load( &m_walkDestCmd );

            lFootTrajPub.publish( lFootTraj );
            rFootTrajPub.publish( rFootTraj );
            comTrajPub.publish(   comTraj );

            ROS_INFO_STREAM( "Step done!" );
        }

    }

    ROS_WARN( "Shutting down SimpleWalkCommander" );
}



bool
SimpleWalkCommander::
highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt )
{
    if( !atlasStateMsg )
        return false;

    Eigen::Vector3d force;
    if( foot == LEFT_FOOT )
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->l_foot.force.x );
    else
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->r_foot.force.x );

    //FIXME Filter with more samples
    m_filteredForce = lowPassFilter( m_filteredForce, force, dt.toSec()/m_dt.toSec() );

    if( m_filteredForce.norm() > m_forceThresh )
        return true;

    return false;
}



}
}
