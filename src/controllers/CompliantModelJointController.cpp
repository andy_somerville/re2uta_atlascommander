/*
 * CompliantModelJointController.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */

#include <re2uta/AtlasCommander/CompliantModelJointController.hpp>
#include <ros/ros.h>


namespace re2uta
{
namespace atlascommander
{


CompliantModelJointController::
CompliantModelJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar )
    : JointController( syncVar )
{
    m_complianceController.reset( new ModelBasedComplianceController() );

    std::string urdfString;
    m_node.getParam( "/robot_description", urdfString );

    urdf::Model urdfModel;
    urdfModel.initString( urdfString );

    m_atlasLookup.reset( new re2uta::AtlasLookup( urdfModel ) );

    m_shutdown          = false;
    m_iteration         = 0;
    m_commandLoopPeriod = commandLoopPeriod;
    m_commandLoopThread = boost::thread( &CompliantModelJointController::commandLoop, this );
    m_jointCommandPub   = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
}

CompliantModelJointController::
~CompliantModelJointController()
{
    m_shutdown = true;
//    m_commandLoopThread.join();
}


void
CompliantModelJointController::
setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime )
{
    boost::atomic_store( &m_currentCommand, msg );
    ROS_ERROR_STREAM_THROTTLE( 20, "CompliantModelJointController::setJointCommand currently ignoring commandTime" );
}


void
CompliantModelJointController::
commandLoop()
{
    ROS_INFO( "Starting command loop, period is: %2.3f", m_commandLoopPeriod.toSec() );

//    m_setDampingSrv     = m_node.serviceClient<atlas_msgs::SetJointDamping>( "/atlas/set_joint_damping" );
//    m_setDampingSrv.waitForExistence();
//
//    atlas_msgs::SetJointDamping transaction;
//    transaction.request.damping_coefficients.fill( 80 );
//    m_setDampingSrv.call( transaction );


    using boost::format;
    atlas_msgs::AtlasCommand::ConstPtr currentCommand;
    atlas_msgs::AtlasState::ConstPtr   currentState;

    ros::Duration margin( 0.0001 );
    m_nextCommandTime = ros::Time::now() + m_commandLoopPeriod;

    boost::mutex                     mutex;
    boost::unique_lock<boost::mutex> lock( mutex );

    while( ros::ok() && !m_shutdown )
    {
        m_syncVar.wait( lock ); //FIXME this should be timed wait, but how to do it with ros time?
        // ros::Duration( m_commandLoopPeriod.toSec() - std::fmod( ros::Time::now().toSec(), m_commandLoopPeriod.toSec() ) ).sleep();

        currentState   = boost::atomic_load( &m_latestStateMsg );
        currentCommand = boost::atomic_load( &m_currentCommand );
        if( !currentState || !currentCommand )
        {
            ROS_WARN_STREAM_THROTTLE( 5, "No current state continuting" );
            continue;
        }

        Eigen::VectorXd torques;

        m_complianceController->setStateMsg(   currentState   );
        m_complianceController->setCommandMsg( currentCommand );
        torques = m_complianceController->calcTorques( m_commandLoopPeriod.toSec() );

        if( torques.size() == 0 )
        {
            ROS_ERROR( "Compliant Model Controller returned torques of size 0" );
            continue;
        }

        atlas_msgs::AtlasCommand::Ptr atlasCommandMsg;

        atlasCommandMsg = m_atlasLookup->createEmptyJointCmdMsg();
        atlasCommandMsg->k_effort = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );
        atlasCommandMsg->effort   = re2::toStd( torques );
        atlasCommandMsg->desired_controller_period_ms = 5;

        m_jointCommandPub.publish( atlasCommandMsg );

        ++m_iteration;
    }
}



}
}
