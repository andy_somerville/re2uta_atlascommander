/**
 *
 * CalculateSwingTrajectory.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Jun 2, 2013
 * @modified Jun 3, 2013
 *
 */

#include <re2uta/CalculateSwingTraj.hpp>


namespace re2uta
{

AtlasSwingNode::
AtlasSwingNode()
{
    m_defaultStepLength    = 0.3;
    m_ankleHeight          = 0.07934;
    m_foreFootLen          = 0.184;
    m_aftFootLen           = 0.076;
    m_apexX                = m_defaultStepLength/2;
    m_liftoffAnklePitch    = 3.1416/30;
    m_touchdownAnklePitch  = 3.1416/30;
    m_startAnklePitch      = 0;
    m_endAnklePitch        = 0;
    m_maxZ                 = 0.07;
    m_stepPeriod           = 1;
    m_dt                   = 0.005;
    m_leftFirst            = true;
}


Spline
AtlasSwingNode::
createXSpline( double stepStart, double stepEnd, double stepPeriod )
{
    Eigen::VectorXd keySpaceValues(5);
    double startLoc = stepStart;
    double endLoc   = stepEnd;

    keySpaceValues[0] = startLoc;
    keySpaceValues[1] = startLoc + (m_ankleHeight * sin(m_liftoffAnklePitch))   + (m_foreFootLen * ( 1 - cos(m_liftoffAnklePitch)   ));
    keySpaceValues[2] = startLoc + stepEnd/2;
    keySpaceValues[3] = endLoc   - (m_ankleHeight * sin(m_touchdownAnklePitch)) - (m_aftFootLen  * ( 1 - cos(m_touchdownAnklePitch) ));
    keySpaceValues[4] = endLoc;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline xSpline( keySpaceValues, keyTimes, startCondition, endCondition );

//    std::cout << "X Spline key values " << keySpaceValues.transpose() << std::endl;
//    std::cout << "X Spline key times " <<  keyTimes.transpose() << std::endl;
//    std::cout << "X Spline key deriv " << m_keySpaceValuesDerivT.transpose() << std::endl;

    return xSpline;
}

Spline
AtlasSwingNode::
createYSpline( double stepStart, double stepEnd, double stepPeriod )
{
    Eigen::VectorXd keySpaceValues(5);
    double startLoc = stepStart;
    double endLoc   = stepEnd;

    keySpaceValues[0] = startLoc;
    keySpaceValues[1] = startLoc + (m_ankleHeight * sin(m_liftoffAnklePitch))   + (m_foreFootLen * ( 1 - cos(m_liftoffAnklePitch)   ));
    keySpaceValues[2] = startLoc + stepEnd/2;
    keySpaceValues[3] = endLoc   - (m_ankleHeight * sin(m_touchdownAnklePitch)) - (m_aftFootLen  * ( 1 - cos(m_touchdownAnklePitch) ));
    keySpaceValues[4] = endLoc;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::FIRSTDERIV,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline ySpline( keySpaceValues, keyTimes, startCondition, endCondition );
    return ySpline;
}

Spline
AtlasSwingNode::
createZSpline( double stepStart, double stepEnd, double stepPeriod )
{
    double zStart = stepStart;
    double zEnd   = stepEnd;

    Eigen::VectorXd keySpaceValues(5);

    keySpaceValues(0) = zStart;
    keySpaceValues(1) = zStart + (m_foreFootLen * sin(m_liftoffAnklePitch))   + (m_ankleHeight * cos(m_liftoffAnklePitch) ) - m_ankleHeight;
    keySpaceValues(2) = m_maxZ;// - m_ankleHeight; 0.2*(stepDist/m_defaultStepLength);//
    keySpaceValues(3) = zEnd   + (m_aftFootLen * sin(m_touchdownAnklePitch)) + (m_ankleHeight * cos(m_touchdownAnklePitch) ) - m_ankleHeight;
    keySpaceValues(4) = zEnd;

    Eigen::VectorXd keyTimes = calcTranslationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );


    Spline zSpline( keySpaceValues, keyTimes, startCondition, endCondition );


    return zSpline;
}

Spline
AtlasSwingNode::
createAnkleRotSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  m_startAnklePitch;
    keyAngleValues(1) =  m_liftoffAnklePitch;
    keyAngleValues(2) = -m_touchdownAnklePitch;
    keyAngleValues(3) = -m_endAnklePitch;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline anklePitchSpline( keyAngleValues, keyTimes, startCondition, endCondition );

    return anklePitchSpline;
}

Spline
AtlasSwingNode::
createGroundContactSpline( double stepPeriod )
{
    Eigen::VectorXd keyAngleValues(4);

    keyAngleValues(0) =  0;
    keyAngleValues(1) =  0;
    keyAngleValues(2) =  1;
    keyAngleValues(3) =  1;

    Eigen::VectorXd keyTimes = calcRotationTimeKeypoints( stepPeriod );

    typedef Spline::EndPointCondition EndPointCondition;

    EndPointCondition startCondition(  Spline::QUADRATIC,  0 );
    EndPointCondition endCondition(    Spline::FIRSTDERIV, 0 );

    Spline spline( keyAngleValues, keyTimes, startCondition, endCondition );

    return spline;
}

Eigen::VectorXd
AtlasSwingNode::
calcTranslationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes( 5 );

    double startTime           = 0;
    double endTime             = stepPeriod;
    double doubleSupportPeriod = 0.1 * stepPeriod;
    double midpointTime        = stepPeriod/2;

    transTimes[0] = startTime;
    transTimes[1] = startTime + doubleSupportPeriod;
    transTimes[2] = startTime + midpointTime;
    transTimes[3] = endTime   - doubleSupportPeriod;
    transTimes[4] = endTime;

    return transTimes;
}

Eigen::VectorXd
AtlasSwingNode::
calcRotationTimeKeypoints( double stepPeriod )
{
    Eigen::VectorXd transTimes = calcTranslationTimeKeypoints( stepPeriod );
    Eigen::VectorXd rotationTimes( transTimes.size() - 1 );

    assert( rotationTimes.size() == 4 );

    rotationTimes(0) = transTimes(0) ;
    rotationTimes(1) = transTimes(1) ;
    rotationTimes(2) = transTimes(3) ;
    rotationTimes(3) = transTimes(4) ;

    return rotationTimes;
}

void
AtlasSwingNode::
calcSwingTrajectory( Eigen::Vector3d & stepStart, Eigen::Vector3d & stepEnd, double stepPeriod )
{
  m_xSpline.reset( new Spline( createXSpline( (double) stepStart(0), (double) stepEnd(0), stepPeriod )) );
  m_ySpline.reset( new Spline( createYSpline( (double) stepStart(1), (double) stepEnd(1), stepPeriod )) );
  m_zSpline.reset( new Spline( createZSpline( (double) stepStart(2), (double) stepEnd(2), stepPeriod )) );
}

Eigen::Vector3d
AtlasSwingNode::
getSwingPosPoint( double timeVal )
{
  Eigen::Vector3d trajPos;

  trajPos(0) = m_xSpline->eval( timeVal )(0);
  trajPos(1) = m_ySpline->eval( timeVal )(0);
  trajPos(2) = m_zSpline->eval( timeVal )(0);

  return trajPos;

}

Eigen::Vector3d
AtlasSwingNode::
getSwingVelPoint( double timeVal )
{
  Eigen::Vector3d trajVel;

  trajVel(0) = m_xSpline->eval( timeVal )(1);
  trajVel(1) = m_ySpline->eval( timeVal )(1);
  trajVel(2) = m_zSpline->eval( timeVal )(1);

  return trajVel;

}

Eigen::Vector3d
AtlasSwingNode::
getSwingAccPoint( double timeVal )
{
  Eigen::Vector3d trajAcc;

  trajAcc(0) = m_xSpline->eval( timeVal )(2);
  trajAcc(1) = m_ySpline->eval( timeVal )(2);
  trajAcc(2) = m_zSpline->eval( timeVal )(2);

  return trajAcc;

}

nav_msgs::Path
AtlasSwingNode::
getSwingPosTrajMsg( double stepPeriod, std::string supportName )
{
  int numTimeSteps = stepPeriod / m_dt;

  nav_msgs::Path             swingTraj;
  geometry_msgs::PoseStamped pose;
  swingTraj.header.frame_id = supportName;

  for( int timeStep = 1; timeStep <= numTimeSteps && ros::ok(); ++timeStep )
  {
      double timeVal          = timeStep * m_dt;
      double completionFactor = (float)timeStep/numTimeSteps;

      pose.header.seq = timeStep;

      pose.pose.position.x = m_xSpline->eval( timeVal )(0);
      pose.pose.position.y = m_ySpline->eval( timeVal )(0);
      pose.pose.position.z = m_zSpline->eval( timeVal )(0);

      swingTraj.poses.push_back( pose );

//      Eigen::Affine3d trajectoryPose = Eigen::Affine3d::Identity();

//      trajectoryPose.translation().x() = xSpline.eval( timeVal )(0);
////            trajectoryPose.translation().y() = ySpline.eval( timeVal )(0);
//      trajectoryPose.translation().y() = stepDiff.translation().y() * completionFactor;
//      trajectoryPose.translation().z() = zSpline.eval( timeVal )(0);
//      trajectoryPose.linear()          = Eigen::AngleAxisd( anklePitchSpline.eval( timeVal )(0), Eigen::YAxis3d ).toRotationMatrix();
//      trajectoryPose.linear()          = trajectoryPose.rotation() *  Eigen::Quaterniond().slerp( completionFactor, diffQuat );
//      trajectoryPose                   = currentPose * trajectoryPose;

//            vizdebug.publishPoint3( trajectoryPose.translation() );

  }

  return swingTraj;

}

}
