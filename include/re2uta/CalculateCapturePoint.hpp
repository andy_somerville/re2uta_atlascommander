/**
 *
 * CalculateCapturePoint.hpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Apr 24, 2013
 * @modified Apr 24, 2013
 *
 */

#pragma once

#include <ros/ros.h>
#include <ros/subscribe_options.h>

#include <sensor_msgs/JointState.h>
#include <osrf_msgs/JointCommands.h>
#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <std_msgs/Int32.h>
#include <geometry_msgs/PolygonStamped.h>

#include <std_msgs/Empty.h>
#include <tf/transform_listener.h>

#include <re2uta/debug.hpp>

#include <re2uta/AtlasLookup.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/capture_point.hpp>

#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <Eigen/Core>

#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>

#include <re2uta/SupportPolygon.hpp>

// KDL stuff TODO maybe already included??
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

#include <eigen_conversions/eigen_kdl.h>

#include <re2uta/FullBodyPoseSolver.hpp>


// TODO maybe move these enums to a global place

enum PreemptFlag
{
    KeepPublishing    = 0, // 0 - keep publishing (default)
    StopPublishing    = 1, // 1 - stop trajectory pub
    RestartPublishing = 2, // 2 - start/restart trajectory pub
    ResetPublishing   = 3  // 3 - Reset trajectory
};

namespace re2uta
{

class AtlasCaptureNode
{
    public:
        FootSupport           m_supportState;
    private:

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;

        Eigen::VectorXd  m_treeJointPositions;
        Eigen::VectorXd  m_atlasJointPositions;

        Eigen::VectorXd  m_atlasR2LjointPos;
        Eigen::VectorXd  m_atlasL2RjointPos;

        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;

        Eigen::Vector3d  m_comPosition;
        Eigen::Vector3d  m_lastCOMPosition;
        Eigen::Vector3d  m_comVelocity;

        Eigen::Vector3d  m_instantaneousCapturePoint;
        Eigen::Vector3d  m_propagatedCapturePoint;

        Eigen::Vector3d  m_copPosition;

        AtlasLookup::Ptr m_atlasLookup;
        FullBodyPoseSolver::Ptr m_poseSolver;

        re2uta::AtlasSupportPolygon::Ptr m_supportPolygon;

        ros::Subscriber       m_atlasStateSubscriber;

        ros::Publisher        m_capturePointPublisher;
        ros::Publisher        m_stopTrajectoryPublisher;
        ros::Publisher        m_supportPolygonPublisher;

        tf::TransformListener m_tfListener;

        ros::Time             m_lastTime;;
        ros::Time             m_sampleTime;

        // Maintains the sate of Atlas support

        // Maintains base frame
        std::string           m_baseName;
        std::string           m_swingName;

        Eigen::Affine3d       m_R2Ltransform;

        /*
         * This will perform the forward kinematics from root to tip of a chain using KDL solvers
         * TODO change this to Tree FK
         */
        KDL::Frame ChainFK( KDL::Tree & in_tree, std::string root, std::string tip, Eigen::VectorXd & atlasJointPos );

        /*
         * This function sets the internal class state that maintains the support state of Atlas
         * TODO add more sophisticated way to calculate this, maybe include torques etc
         */
        void setSupportState( double lForceZ, double rForceZ );

        void setSupportStateNoDouble( double lForceZ, double rForceZ );

    public:

        typedef boost::shared_ptr<AtlasCaptureNode> Ptr;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        AtlasCaptureNode( const urdf::Model & urdfModel );

        Eigen::Vector3d calculateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        void updateMemberDataInternal( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        void updateMemberDataNoDouble( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        void updateMemberData( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );


        Eigen::Vector3d calculatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT );

        /*
         * This function will update the Instantaneous Capture Point member data
         */
        void updateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        /*
         * This function will update the propagated Capture Point (CP) member data
         */
        void updatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT );

        /*
         * Here we set the KDL tree joint states using the Atlas state message
         * This is where the main processing is done                 *
         */
        bool isStateCaptured( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, Eigen::Vector3d & capturePoint );

        Eigen::Vector3d getInstantaneousCapturePoint();

        /*
         * This will get the time propagated Capture Point (CP) from Instantaneous Capture Point (ICP)
         * This will also perform a calculation to get the Instantaneous Capture Point (ICP)
         */
        Eigen::Vector3d getPropagatedCapturePoint();

        double calculateFutureCapturePointTime( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, Eigen::Vector3d futureCP );

        void updateCoP( geometry_msgs::Wrench wrenchL, geometry_msgs::Wrench wrenchR, Eigen::Vector3d posL, Eigen::Vector3d posR );

        visualization_msgs::Marker getCoPMarker();

        void updateCOM( std::string baseName,  Eigen::VectorXd & treeJointPositions );

        Eigen::Vector3d getCoP();

        Eigen::Vector3d getCom();

        /*
         * In this gravityVector is the direction vector of gravity in the base frame
         * This function will give the vector of gravity in the frame of the given link name
         */
        Eigen::Vector3d getGravityVectorInLink( geometry_msgs::Quaternion orient, std::string linkName );

        /*
         * In this gravityNormal is the normal away from the plane that the gravity vector goes into
         * This function will give the vector of gravity in the frame of the given link name
         */
        Eigen::Vector3d getGravityNormalInLink( geometry_msgs::Quaternion orient, std::string linkName );

        Eigen::Vector3d getComProjection( geometry_msgs::Quaternion orient, Eigen::Vector3d  & comInFootFrame );

        Eigen::Vector3d getComProjection( geometry_msgs::Quaternion orient );

        Eigen::Vector3d getLinkNormalInInertial( geometry_msgs::Quaternion orient, std::string linkName );

        Eigen::Vector3d getCorrectedSupportPolygonCenterInFootFrame( geometry_msgs::Quaternion orient, Eigen::Vector3d  & supPolyCenter );

        Eigen::Vector3d getCorrectedSupportPolygonCenterInFootFrame( geometry_msgs::Quaternion orient );

        /*
         * This function will calculate the CoP aka ZMP for each foot
         */
        Eigen::Vector3d calcIndividualCoP( geometry_msgs::Wrench wrench );

        /*
         * This function will calculate the CoP aka ZMP for each foot
         */
        Eigen::Vector3d calcCombinedCoP( geometry_msgs::Wrench wrenchL, geometry_msgs::Wrench wrenchR, Eigen::Vector3d posL, Eigen::Vector3d posR);

        visualization_msgs::Marker getComMarker();

        visualization_msgs::Marker getComProjectedMarker( geometry_msgs::Quaternion orientation );

        visualization_msgs::Marker getSupProjectedMarker( geometry_msgs::Quaternion orientation );

        visualization_msgs::Marker getInstantaneousCapturePointMarker();

        visualization_msgs::Marker getPropagatedCapturePointMarker();

        geometry_msgs::PolygonStamped getSupportPolygonMarker();

        Eigen::Vector3d getSupportPolygonCenter() const;

        visualization_msgs::Marker getSupportPolygonCenterMarker() const;

        std::string getBaseName();

        std::string getSwingName();

        Eigen::Vector3d getSwingFootPose();

};

}
