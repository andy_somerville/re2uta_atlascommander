/*
 * WalkCommanderInterface.hpp
 *
 *  Created on: Jun 7, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/walking/StepGenerator.hpp>
#include <atlas_msgs/AtlasState.h>
#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>


namespace re2uta
{
namespace atlascommander
{


class WalkGoal
{
    public:
        typedef boost::shared_ptr<WalkGoal> Ptr;

    public:
        std::string     goalFrameId;
        Eigen::Affine3d goalPose;
        Foot            swingLegSuggestion;


        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

typedef std::vector<WalkGoal::Ptr>        WalkGoalVector;
typedef boost::shared_ptr<WalkGoalVector> WalkGoalVectorPtr;


class WalkCommanderInterface
{
    public:
        enum StepPlannerType
        {
            NAO_PLANNER    = 0,
            CIRCLE_PLANNER = 1,
            MANUAL_PLANNER = 2
        };

    public:
        typedef boost::shared_ptr<WalkCommanderInterface> Ptr;

    public:
        virtual ~WalkCommanderInterface() {}
        virtual void walkTo( const WalkGoal::Ptr & walkGoal ) = 0;
        virtual void walkTo( const atlascommander::WalkGoalVectorPtr& walkGoals ); //FIXME violating concept of interface
        virtual void setStepPlannerType( StepPlannerType stepPlanner ) = 0;
        virtual void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg ) = 0;
        virtual void shutdown() = 0;

//        virtual void setDest( const Eigen::Affine3d & dest ) = 0;
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}
