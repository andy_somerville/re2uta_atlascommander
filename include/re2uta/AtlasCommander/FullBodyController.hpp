/*
 * FullBodyController.hpp
 *
 *  Created on: May 13, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasCommander/FrameGoal.hpp>
#include <re2uta/AtlasCommander/JointGoal.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <atlas_msgs/AtlasState.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <boost/thread/condition_variable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>


namespace re2uta
{
namespace atlascommander
{

class FullBodyController : public FullBodyControllerInterface
{
    public:
        typedef boost::shared_ptr<FullBodyController> Ptr;

        enum CommandType { VELOCITY_CMD = 0,
                           POSITION_CMD = 1  };

        enum ControlType { VELOCITY_CONTROL = 0,
                           POSITION_CONTROL = 1  };

    public:
        FullBodyController( const urdf::Model & model,
                            AtlasLookup::Ptr const & atlasLookup,
                            boost::condition_variable & syncVar,
                            const JointController::Ptr & jointController,
                            ros::Duration dt );

        virtual ~FullBodyController();

        void setLatestStateMsg(   const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );
        void setCenterOfMassGoal( const Eigen::Vector3d & comLocation, const Eigen::Vector3d & comWeight = Eigen::Vector3d::Ones() );
        void setCenterOfMassGoalGains( const Eigen::Vector3d & kp, const Eigen::Vector3d & kd );
//        void setOppositeFootGoal( const Eigen::Vector6d & frame,       const Eigen::Vector6d & weights );
        void setOppositeFootGoal( const Eigen::Affine3d & frame,       const Eigen::Vector6d & weights );
        void setLeftHandGoal(     const Eigen::Vector6d & frame,       const Eigen::Vector6d & weights );
        void setRightHandGoal(    const Eigen::Vector6d & frame,       const Eigen::Vector6d & weights );
        void setFrameGoal(        const FrameGoal::Ptr & frameGoal );
        void setFrameGoalGains(   const FrameGoalGains::Ptr & frameGoalGains );
        void setJointGoal(        const Eigen::VectorXd & jointGoal );

        void switchBaseFoot(      const std::string & baseFootFrameId );
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts );

        virtual std::string getBaseFrameId();



        visualization_msgs::Marker publishOppFootInBase();


        FrameGoalGains::Ptr  defaultFrameGains() { return m_defaultFrameGains; }

//        Eigen::Affine3d getConstraintState( const std::string & constraintName ); // FIXME we need a better way to address constraints
//        void            setFrameConstraint( const Eigen::Vector6d & frame, const Eigen::Vector6d & comWeight );
//        void            createFrameConstraint( const std::string & baseFrame, const std::string & constrainedFrame );
//        void            removeFrameConstraint( );

    protected:

//        Eigen::VectorXd projectJointVelocities( const ros::Time & time );
        void                 passVelocities(     const Eigen::VectorXd & velocities );
        ros::Time            getNextCommandTime( const ros::Time sampleTime );
        bool                 updateParameters(   const ros::Time & nextCommandTime );
        void                 updateConstraints();
        void                 controlLoop();
        FrameConstraint::Ptr findFrameConstraint( const std::string & name );
        std::string          oppositeFootFrameId();
        void                 publishDebug();

        Eigen::Affine3d      lookupTransformFromTo( const std::string & fromFrame, const std::string & toFrame );

        void                 handleJointConstraintTimerEvent( const ros::TimerEvent & timerEvent );
        void                 handleKEffortsTimerEvent( const ros::TimerEvent & timerEvent, std::vector<uint8_t> kefforts );


    protected:
        bool                        m_shutdown;
        bool                        m_constraintsDirty;
        CommandType                 m_commandType;
        ControlType                 m_jointControlType;
        boost::thread               m_controlLoopThread;
        boost::condition_variable & m_syncVar;
        int                         m_iteration;
        ros::Duration               m_dt;


        FullBodyPoseSolver::Ptr     m_poseSolver;
        JointController::Ptr        m_jointController;
        AtlasLookup::Ptr            m_atlasLookup;
        re2::VisualDebugPublisher::Ptr m_vizDebugPublisher;

        Eigen::VectorXd             m_posKp;
        Eigen::VectorXd             m_posKd;
        Eigen::VectorXd             m_velKp;
        Eigen::VectorXd             m_maxVelocity;
        Eigen::VectorXd             m_treeJointPositions;
        Eigen::VectorXd             m_treeJointVelocities;
        Eigen::VectorXd             m_defaultTreeJointPositions;

        Eigen::MatrixXd             m_outputWeights;
        Eigen::Affine3d             m_defaultTransform;
        Eigen::MatrixXd             m_inputWeights; // intentionally empty so gradient descent solver will ignore

//        ros::Time                   m_sampleTime;
        ros::Time                   m_nextCommandTime;
        boost::shared_ptr<std::string>  m_baseFootFrameId;
//        std::string                 m_oppFoot;
//        boost::mutex                m_goalParamMutex;
        FrameGoalGains::Ptr            m_defaultFrameGains;


        //updated via thread handoff using boost::atomic
//        Eigen::Vector3d                  m_desiredPostureOrientation;
//        Eigen::Vector3d             m_desiredComPose;
//        Eigen::Vector6d             m_desiredOppFootPose;
        FrameGoal::Ptr                   m_oppFootGoal;
        JointGoal::Ptr                   m_jointGoal;
        atlas_msgs::AtlasState::ConstPtr m_lastStateMsg;
        FrameGoalListPtr                 m_frameGoalUpdateList;
        FrameGoalGainsListPtr            m_frameGoalGainsUpdateList;
        Eigen::Vector6dPtr               m_comGoal;
        ComGains::Ptr                    m_comGains;

        boost::shared_ptr<std::string>   m_newBaseFoot;

        ros::NodeHandle                  m_node;

        tf::TransformBroadcaster         m_tfBroadcaster;
        tf::TransformListener            m_tfListener;
        ros::Timer                       m_kEffortsTimer;
        ros::Timer                       m_jointConstraintTimer;
        Eigen::VectorXd                  m_bdiTreeDefaults;
        Eigen::VectorXd                  m_defaultOptimalAngles;


        ros::CallbackQueue               m_privateCallbackQueue;




    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

}
}
