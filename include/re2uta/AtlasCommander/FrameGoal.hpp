/*
 * FrameGoal.hpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2/eigen/eigen_util.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <list>

namespace re2uta
{
namespace atlascommander
{

class FrameGoalGains
{
    public:
        typedef boost::shared_ptr<FrameGoalGains> Ptr;

    public:
        Eigen::Vector6d & posKp()      { return m_posKp;      }
        Eigen::Vector6d & posKd()      { return m_posKd;      }
        Eigen::Vector6d & velKp()      { return m_velKp;      }
        Eigen::Vector6d & velMax()     { return m_velMax;     }
        std::string &     tipFrameId() { return m_tipFrameId; } //FIXME need better constraint id

    protected:
        Eigen::Vector6d m_posKp;
        Eigen::Vector6d m_posKd;
        Eigen::Vector6d m_velKp;
        Eigen::Vector6d m_velMax;
        std::string     m_tipFrameId;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


typedef std::list<FrameGoalGains::Ptr>                     FrameGoalGainsList;
typedef boost::shared_ptr<std::list<FrameGoalGains::Ptr> > FrameGoalGainsListPtr;

class FrameGoal
{
    public:
        typedef boost::shared_ptr<FrameGoal> Ptr;

    public:
        FrameGoal() { weightMatrix = Eigen::MatrixXd::Identity(6,6); }
        std::string     baseFrameId;
        std::string     tipFrameId;
        std::string     goalFrameId;
        Eigen::Affine3d goalPose;
        Eigen::MatrixXd weightMatrix;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


typedef std::list<FrameGoal::Ptr>                     FrameGoalList;
typedef boost::shared_ptr<std::list<FrameGoal::Ptr> > FrameGoalListPtr;



class ComGains
{
    public:
        typedef boost::shared_ptr<ComGains> Ptr;

    public:
        Eigen::Vector3d & posKp()      { return m_posKp;      }
        Eigen::Vector3d & posKd()      { return m_posKd;      }
        Eigen::Vector3d & velKp()      { return m_velKp;      }
        Eigen::Vector3d & velMax()     { return m_velMax;     }
        std::string &     tipFrameId() { return m_tipFrameId; } //FIXME need better constraint id

    protected:
        Eigen::Vector3d m_posKp;
        Eigen::Vector3d m_posKd;
        Eigen::Vector3d m_velKp;
        Eigen::Vector3d m_velMax;
        std::string     m_tipFrameId;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};



}
}
