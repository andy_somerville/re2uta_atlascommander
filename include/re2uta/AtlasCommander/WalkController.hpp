/*
 * WalkController.hpp
 *
 *  Created on: May 20, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>
#include <string>

namespace re2uta
{
namespace atlascommander
{


class WalkGoal
{
    public:
        typedef boost::shared_ptr<WalkGoal> Ptr;

    public:
        std::string     goalFrameId;
        Eigen::Affine3d goalPose;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


class WalkController
{
    public:
        typedef boost::shared_ptr<WalkController> Ptr;

    public:
        WalkController();
        virtual ~WalkController();
        virtual void walkTo( const WalkGoal::Ptr & walkGoal );

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
}
