/*
 * FullBodyControllerInterface.hpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/FrameGoal.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <cstddef>
#include <stdint.h>


namespace re2uta
{
namespace atlascommander
{

class FullBodyControllerInterface
{
    public:
        typedef boost::shared_ptr<FullBodyControllerInterface> Ptr;


    public:
//        void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );
        virtual void setCenterOfMassGoal( const Eigen::Vector3d & comLocation, const Eigen::Vector3d & comWeight = Eigen::Vector3d::Ones() ) = 0;
        virtual void setCenterOfMassGoalGains( const Eigen::Vector3d & kp, const Eigen::Vector3d & kd ) = 0;

//        void setOppositeFootGoal( const Eigen::Vector6d & frame,  const Eigen::Vector6d & weights ) = 0;
        virtual void setOppositeFootGoal( const Eigen::Affine3d & frame,  const Eigen::Vector6d & weights ) = 0;
//        void setLeftHandGoal(     const Eigen::Vector6d & frame,       const Eigen::Vector6d & weights );
//        void setRightHandGoal(    const Eigen::Vector6d & frame,       const Eigen::Vector6d & weights );
        virtual void setFrameGoal(        const FrameGoal::Ptr &       frameGoal       ) = 0;
        virtual void setFrameGoalGains(   const FrameGoalGains::Ptr &  frameGoalGains  ) = 0;
        virtual void switchBaseFoot(      const std::string &          baseFootFrameId ) = 0;
        virtual void setJointGoal(        const Eigen::VectorXd &      jointGoal       ) = 0;
        virtual void setKEfforts(         const std::vector<uint8_t> & kefforts        ) = 0;
        virtual std::string getBaseFrameId() = 0;

        virtual FrameGoalGains::Ptr  defaultFrameGains() = 0;


//        virtual Eigen::Affine3d getConstraintState( const std::string & constraintName ) = 0; // FIXME we need a better way to address constraints


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}
