/*
 * DefaultJointController.hpp
 *
 *  Created on: May 13, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <boost/thread/condition_variable.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <boost/thread.hpp>
#include <ros/ros.h>

namespace re2uta
{
namespace atlascommander
{

class DefaultJointController : public JointController
{
    public:
        DefaultJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar, AtlasLookup::Ptr const & atlasLookup );
        virtual ~DefaultJointController();
        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime );
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts );

    protected:
        void commandLoop();

    protected:
        ros::NodeHandle                    m_node;
        ros::Duration                      m_commandLoopPeriod;
        atlas_msgs::AtlasCommand::ConstPtr m_currentCommand;
        long unsigned int                  m_iteration;
        ros::Time                          m_nextCommandTime;
        bool                               m_shutdown;
        boost::thread                      m_commandLoopThread;
        ros::Publisher                     m_jointCommandPub;
        re2uta::AtlasLookup::Ptr           m_atlasLookup;
        ros::ServiceClient                 m_setDampingSrv;
        std::vector<uint8_t>               m_kEfforts;

};

}
}
