/*
 * CommandLoop.hpp
 *
 *  Created on: May 3, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <osrf_msgs/JointCommands.h>
#include <ros/ros.h>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

namespace re2uta
{
namespace atlascommander
{

class CommandLoop
{
    public:
        typedef boost::shared_ptr<CommandLoop> Ptr;

    public:
        ros::NodeHandle               m_node;
        ros::Duration                 m_commandLoopPeriod;
        osrf_msgs::JointCommands::Ptr m_currentCommand;
        int                           m_iteration;
        ros::Time                     m_nextStart;
        bool                          m_shutdown;
        boost::thread                 m_commandLoopThread;
        ros::Publisher                m_jointCommandPub;


    public:
        CommandLoop( const ros::Duration & period );

        osrf_msgs::JointCommands::Ptr getLatestCommand();
        ros::Duration commandLoopPeriod() { return m_commandLoopPeriod; } // copying ros::Duration thread safe?
        ros::Time     nextCommandTime()   { return m_nextStart; }         // copying ros::Time thread safe?
        int           iteration()         { return m_iteration; }
        void          shutdown()          { m_shutdown = false; }

        void commandLoop();
        void commandLoop2();
};


}
} /* namespace re2uta */

