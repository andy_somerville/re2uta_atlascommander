/*
 * CompliantModelJointController.hpp
 *
 *  Created on: Jun 14, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */

#pragma once


#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/ModelBasedComplianceController.hpp>
#include <atlas_msgs/SetJointDamping.h>
#include <boost/thread.hpp>


namespace re2uta
{
namespace atlascommander
{


class CompliantModelJointController : public JointController
{
    public:
        CompliantModelJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar );
        virtual ~CompliantModelJointController();
        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime );
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts );

    protected:
        void commandLoop();

    protected:
        ros::NodeHandle                     m_node;
        ros::Duration                       m_commandLoopPeriod;
        atlas_msgs::AtlasCommand::ConstPtr  m_currentCommand;
        long unsigned int                   m_iteration;
        ros::Time                           m_nextCommandTime;
        bool                                m_shutdown;
        boost::thread                       m_commandLoopThread;
        ros::Publisher                      m_jointCommandPub;
        ModelBasedComplianceController::Ptr m_complianceController;
        re2uta::AtlasLookup::Ptr            m_atlasLookup;
        ros::ServiceClient                  m_setDampingSrv;
};



}
}
