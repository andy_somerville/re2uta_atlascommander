/*
 * StepGenerator.hpp
 *
 *  Created on: May 31, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2/eigen/eigen_util.h>
#include <re2uta/AtlasLookup.hpp>
#include <halfsteps_pattern_generator/Footprint.h>
#include <walk_msgs/Footprint2d.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/foreach.hpp>
#include <ros/ros.h>


namespace re2uta
{


enum Foot
{
    LEFT_FOOT = 0,
    RIGHT_FOOT,
    UNKNOWN_FOOT
};

enum Limb
{
    LEFT_FOOT_LIMB = 0,
    RIGHT_FOOT_LIMB,
    LEFT_HAND_LIMB,
    RIGHT_HAND_LIMB
};

Foot otherFoot( Foot foot );

Limb otherLimb( Limb limb );


class StepPose
{
    public:
        typedef boost::shared_ptr<StepPose>       Ptr;
        typedef boost::shared_ptr<StepPose const> ConstPtr;

    public:
        StepPose( Foot baseFoot ) { swingFoot = otherFoot( baseFoot ); }

        Eigen::Affine3d & swingFootPose();
        Eigen::Affine3d & baseFootPose();
        const Eigen::Affine3d & swingFootPose() const;
        const Eigen::Affine3d & baseFootPose()  const;

        Foot baseFoot() const { return otherFoot( swingFoot ); }

        Eigen::Affine3d lFootPose;
        Eigen::Affine3d rFootPose;
        Eigen::Vector3d comPose;
        Foot            swingFoot;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

typedef std::vector<StepPose::Ptr>           StepPosePtrVector;
typedef boost::shared_ptr<StepPosePtrVector> StepPosePtrVectorPtr;


class CrawlPose
{
    public:
        typedef boost::shared_ptr<CrawlPose>       Ptr;
        typedef boost::shared_ptr<CrawlPose const> ConstPtr;

    public:
        CrawlPose( Limb baseLimb ) { swingLimb = otherLimb( baseLimb ); }

        Eigen::Affine3d & swingLimbPoseOne();
        Eigen::Affine3d & swingLimbPoseTwo();
        Eigen::Affine3d & swingLimbPoseThree();
        Eigen::Affine3d & baseLimbPose();

        const Eigen::Affine3d & swingLimbPoseOne()   const;
        const Eigen::Affine3d & swingLimbPoseTwo()   const;
        const Eigen::Affine3d & swingLimbPoseThree() const;
        const Eigen::Affine3d & baseLimbPose()       const;

        Limb baseLimb() const { return otherLimb( swingLimb ); }

        Eigen::Affine3d lFootPose;
        Eigen::Affine3d rFootPose;
        Eigen::Affine3d lHandPose;
        Eigen::Affine3d rHandPose;
        Eigen::Vector3d comPose;
        Limb            swingLimb;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

typedef std::vector<CrawlPose::Ptr> CrawlPosePtrVector;
typedef boost::shared_ptr<CrawlPosePtrVector> CrawlPosePtrVectorPtr;


std::string footToFrameId( Foot foot, AtlasLookup::Ptr const & atlasLookup );

std::string limbToFrameId( Limb limb, AtlasLookup::Ptr const & atlasLookup );

class StepPlan
{
    public:
        typedef boost::shared_ptr<StepPlan> Ptr;

    public:
        StepPlan( Foot baseFoot, ros::Duration dt ) { m_baseFoot = baseFoot; m_dt = dt; }

        Foot   swingFoot();
        Foot & baseFoot();

        StepPosePtrVector & stepPoses() { return m_stepPoses; }
        double        completion( const ros::Duration & relativeTime );
        StepPose::Ptr getAtTime( const ros::Duration & relativeTime );
        StepPose::Ptr last();

    protected:
//        Foot              m_swingFoot;
        Foot              m_baseFoot;
        StepPosePtrVector m_stepPoses;
        ros::Duration     m_dt;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


class WalkPlan
{
    public:
        typedef boost::shared_ptr<WalkPlan> Ptr;

    public:
        WalkPlan()
        {
            leftFootPoses.reset(  new Eigen::Affine3dList );
            rightFootPoses.reset( new Eigen::Affine3dList );
            comPoints.reset(      new Eigen::Vector3dList );
        }

        Eigen::Affine3dListPtr leftFootPoses;
        Eigen::Affine3dListPtr rightFootPoses;
        Eigen::Vector3dListPtr comPoints;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


class StepGenerator
{
    public:
        typedef boost::shared_ptr<StepGenerator>    Ptr;


    public:
        virtual ~StepGenerator();
        StepGenerator( ros::Duration dt ) : m_dt( dt ), m_swingFoot( LEFT_FOOT ) {};
        virtual bool hasArrived( const Eigen::Affine3d & current ) const = 0;
        virtual bool hasArrived( const WalkPlan & walkPlan )       const;

        const Eigen::Affine3d & source() const { return m_source; }
        const Eigen::Affine3d & dest()   const { return m_dest;   }

        virtual Eigen::Affine3d    generateNextStep( const Eigen::Affine3d & currentBase,
                                                     const Eigen::Affine3d & currentSwing,
                                                     int stepIndex ) = 0;
//        virtual Eigen::Affine3d    generateNextStep( const Eigen::Vector3d & currentTranslation, double zAngle, int stepIndex );
        virtual StepPlan::Ptr      generateAllSteps();
//        virtual FootprintVectorPtr generateAllStepsFootprints();

    protected:
        virtual StepPose::Ptr getBeginStepPose() = 0;
//        StepPose::Ptr getEndStepPose();

    protected:
        Eigen::Affine3d           m_dest;
        Eigen::Affine3d           m_source;
        ros::Duration             m_dt;
        Foot                      m_swingFoot;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};



class StraightLineStepGenerator : public StepGenerator
{
    public:
        StraightLineStepGenerator( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength );

        virtual bool            hasArrived(   const Eigen::Affine3d & current ) const;
        virtual Eigen::Affine3d generateNextStep( const Eigen::Affine3d & last,
                                                  const Eigen::Affine3d & current,
                                                  int stepIndex );

    protected:
        bool withinDistance( const Eigen::Affine3d & current, double distance ) const;
        StepPose::Ptr getBeginStepPose();


        double          m_halfStepLength;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


Eigen::Vector3d interpolate( const Eigen::Vector3d & vec0, const Eigen::Vector3d & vec1, double factor );
Eigen::Affine3d interpolate( const Eigen::Affine3d & pose0, const Eigen::Affine3d & pose1, double factor );

}
