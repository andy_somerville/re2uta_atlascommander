/*
 * CirclePathStepGenerator.hpp
 *
 *  Created on: Jun 17, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/walking/StepGenerator.hpp>


namespace re2uta
{


class Circle
{
    public:
        typedef boost::shared_ptr<StepGenerator> Ptr;

    public:
        Eigen::Vector3d &       origin()       { return m_origin; }
        const Eigen::Vector3d & origin() const { return m_origin; }
        double &                radius()       { return m_radius; }
        const double &          radius() const { return m_radius; }

    protected:
        Eigen::Vector3d m_origin;
        double          m_radius;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

typedef std::vector<Circle> CircleVector;


class Ray
{
    public:
        Ray( const Eigen::Vector3d & originArg, const Eigen::Vector3d & directionArg ) : m_origin( originArg ), m_direction( directionArg ) {}
        Eigen::Vector3d &       origin()          { return m_origin; }
        Eigen::Vector3d &       direction()       { return m_direction; }

        const Eigen::Vector3d & origin()    const { return m_origin; }
        const Eigen::Vector3d & direction() const { return m_direction; }

        Eigen::Vector3d normal( const Eigen::Vector3d & coPlanarVector ) const { return m_direction.cross( coPlanarVector ).normalized(); }

    protected:
        Eigen::Vector3d m_origin;
        Eigen::Vector3d m_direction;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


CircleVector findCircles( const Ray & ray0, const Ray & ray1 );



class CirclePathStepGenerator : public StepGenerator
{
    public:
        CirclePathStepGenerator( const StepPose::ConstPtr srcPose,
                                 const Eigen::Affine3d & dest,
                                 double halfStepLength,
                                 double footSpreadOffset,
                                 Foot   swingFoot );

        CirclePathStepGenerator( const Eigen::Affine3d & source,
                                 const Eigen::Affine3d & dest,
                                 double halfStepLength,
                                 double footSpreadOffset,
                                 Foot   swingFoot );


        virtual bool            hasArrived(   const Eigen::Affine3d & current ) const;
        virtual Eigen::Affine3d generateNextStep( const Eigen::Affine3d & last,
                                                  const Eigen::Affine3d & current,
                                                  int  stepIndex );


    protected:
        CircleVector::iterator  determineCircle( const Eigen::Affine3d & current );
        bool withinDistance( const Eigen::Affine3d & current, double distance ) const;
        StepPose::Ptr getBeginStepPose();

    protected:
        CircleVector              m_circles;
        CircleVector::iterator    m_circleIterator;
        double                    m_halfStepLength;
        double                    m_footSpreadOffset;
        Foot                      m_swingFoot;
        StepPose::Ptr             m_srcPose;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
