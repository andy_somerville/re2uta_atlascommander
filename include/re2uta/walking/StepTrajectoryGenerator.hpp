/*
 * WalkPatternGenerator.hpp
 *
 *  Created on: May 29, 2013
 *      Author: Ghassan Atmeh, andrew.somerville
 */

#pragma once

#include <halfsteps_pattern_generator/GetPath.h>
#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/splines/Spline.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Core>



namespace re2uta
{



Eigen::Vector3dListPtr
comTraj( const double & Dt,
         const Eigen::Vector3dVector & zmpRef,
         Eigen::Vector3dListPtr & zmpRefPlan,
         Eigen::Vector3d * lastComPose,
         Eigen::Vector3d * lastComVel,
         Eigen::Vector3d * lastComAccel,
         Eigen::Vector3d * lastZmpPose,
         double m_doubleSupportTime);


Eigen::Affine3d
createOdomFrame( const StepPose::ConstPtr & currentStepPose );


class StepTrajectoryGenerator
{
    public:
        StepTrajectoryGenerator( const StepPose::ConstPtr & stepPose, const Eigen::Affine3d & dest, double halfStepSize, double footSpreadOffset );

        StepPlan::Ptr generateNextStep( const StepPose::ConstPtr & stepPose );
        StepPlan::Ptr generateNextStep0();

    public:
        Eigen::Vector3dListPtr m_zmpRefPlan;


    protected:
        Eigen::Affine3dListPtr
        generateTrajectory( const Eigen::Affine3d & currentPose,
                            const Eigen::Affine3d & nextPose,
                            const Eigen::Affine3dListPtr & trajectoryArg = Eigen::Affine3dListPtr() );

        Spline          createXSpline(           double stepLength, double stepPeriod );
        Spline          createYSpline(           double stepLength, double stepPeriod );
        Spline          createZSpline(             double stepDist,   double stepPeriod );
        Spline          createAnkleRotSpline(                         double stepPeriod );
        Spline          createGroundContactSpline(                    double stepPeriod );

        Eigen::VectorXd calcTranslationTimeKeypoints( double stepPeriod );
        Eigen::VectorXd calcRotationTimeKeypoints(    double stepPeriod );


    protected:
        Eigen::Affine3d & baseFootPose();
        Eigen::Affine3d & swingFootPose();


        WalkPatternGenerator m_stepPatternGenerator;
        StepGeneratorPtr m_stepGenerator;
        Eigen::Affine3d  m_lastLeftFootPose;
        Eigen::Affine3d  m_lastRightFootPose;
        Eigen::Vector3d  m_lastComPose;
        Eigen::Vector3d  m_lastZmpPose;
        Eigen::Vector3d  m_lastComVelocity;
        Eigen::Vector3d  m_lastComAccel;
        size_t           m_stepIndex;
        Foot             m_baseFoot;



        double m_defaultStepLength;
        double m_ankleHeight;
        double m_foreFootLen;
        double m_aftFootLen;
        double m_apexX;
        double m_liftoffAnklePitch;
        double m_touchdownAnklePitch;
        double m_startAnklePitch;
        double m_endAnklePitch;
        double m_maxZ;
        double m_defaultStepPeriod;
        double m_doubleSupportTime;
        double m_dt;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};






}
